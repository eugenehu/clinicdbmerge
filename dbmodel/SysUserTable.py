import datetime

'''
name, type, create_date, modify_date
'''
class SysUserTable:

    @property
    def name(self):
        return self._name

    @property
    def create_date(self):
        return self._create_date

    @property
    def modify_date(self):
        return self._modify_date

    def __init__(self):
        self._name = ''
        self._id = -1
        self._create_date = datetime.datetime.now()
        self._modify_date = datetime.datetime.now()

    def set_value(self, row):
        self._name = row[0]
        self._create_date = row[2]
        self._modify_date = row[3]


'''column information of data table'''
class SysColumn:
    def __init__(self):
        self.table_id = -1
        self.name = ''
        self.xtype = -1
        self.length = -1

    def set_value(self, row):
        self.name = ''

