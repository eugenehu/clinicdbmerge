from dbmodel.SysUserTable import *
from common.utils import *


class TestOdbc(object):

    def __init__(self):
        print('db compare is created')
        self.conn_str = "Driver={SQL Server Native Client 11.0}; Server=127.0.0.1; Database=ec01_122; uid=ecuser;pwd=1234"
        # conn_str = "Driver={SQL Server Native Client 11.0}; Server=192.168.168.101; Database=ec29; uid=ecuser;pwd=1234"

    def get_schema_info(self):
        dbutil = DbUtil(self.conn_str)

        sql = "SELECT top 12 name, type, create_date, modify_date ,object_id  FROM sys.all_objects where type='U'"
        cursor_tbl = dbutil.query_sql_to_list(sql)

        for row in cursor_tbl:
            sys_user_table = SysUserTable()
            sys_user_table.set_value(row)
            print('table_row = %r' % (row))

            table_name = sys_user_table.name
            print("table_name={0}".format(table_name))

            sql_col = ("SELECT  col.name, col.system_type_id, col.max_length ,col.is_identity FROM sys.all_objects obj  "
                               "inner join sys.all_columns col "
                               "ON  obj.object_id =col.object_id  WHERE obj.name = '{0}'".format(table_name))
            print(sql_col)
            cursor_col = dbutil.query_sql_to_list(sql_col)
            for col in cursor_col:
                print(col)

    def get_autoinc_column(self):
        dbutil = DbUtil(self.conn_str)
        sql = ("SELECT  obj.name table_name, col.name column_name, col.system_type_id, col.max_length ,col.is_identity FROM sys.all_objects obj  "
                    "inner join sys.all_columns col "
                    "ON  obj.object_id =col.object_id " 
                    "WHERE col.is_identity =1 and obj.type='U' order by 1")

        print(sql)

        cursor_col = dbutil.query_sql_to_list(sql)
        idx = 0
        max_db_id = -1
        for row in cursor_col:
            idx += 1
            table_name = row[0]
            column_name = row[1]
            sql_max_id = ('select max({0}) from {1}').format(column_name, table_name)
            max_id = dbutil.query_sql_to_int(sql_max_id)
            max_db_id = max(max_id, max_db_id)
            if(max_id > 123456078):
                print("{0},{1},{2},{3}".format(idx, table_name, column_name, max_id))

        print('max_db_id= {0}'.format(max_db_id))


    def pull_patient(self):
        dbid = 21
        dbutil = DbUtil(self.conn_str)
        patient_list = dbutil.query_sql_to_list('select top 3 * from patient')
        for pat in patient_list:
            print(pat)


def Test():
    # get_schema_info()
    # get_autoinc_column()
    tester = TestOdbc()
    tester.pull_patient()

def TestXls():
    print('xls')


if __name__ == "__main__":
    TestXls()