
from schemamg.dbmg import generate_central_mongo_view_schema

''' 
create table redshift to save the view of ec database
for example:

View_mongo_OtherAssessmentPlan  -----  ec_OtherAssessmentPlan
'''
if __name__ == "__main__":
    generate_central_mongo_view_schema()