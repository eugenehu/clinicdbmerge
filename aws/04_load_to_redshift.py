from common.utils import Global, DbUtil
import re


def get_db_util(clinic_id):
    # webid = Global.get_webid_by_clinic(clinic_id)
    conn_str = Global.get_connection_str(clinic_id)
    print(conn_str)
    return DbUtil(conn_str)


def get_copy_sql(clinic_id, view_name):
    """根据诊所id 表名 获取列copy语句"""
    db_source = get_db_util(clinic_id)
    col_list = db_source.get_columns(view_name)

    if len(col_list) == 0:
        print('{0} is not defined in center table'.format(view_name))
        return
    copy_sql = ''
    have_webid = 0
    for col in col_list:
        col_name = col.column_name
        if col_name.lower() == 'webid':
            have_webid = 1
        copy_sql += '"{0}",'.format(col_name)
    if view_name == 'ClinicMPI':
        copy_sql += 'clinicpatientid,'
        # MPatient不需要webid字段
    elif view_name == 'MPatient':
        have_webid = 1
    copy_sql = ' copy {0} ({1}{2}) from '.format(view_name, copy_sql[0:-1], ', "WebID"' if not have_webid else '')

    return copy_sql


def get_descri_by_table_name(clinic_id, file_name):
    """根据表名视图名 获取控制文件内容"""
    view_name = file_name.split('.')[0]
    copy_sql = get_copy_sql(clinic_id, view_name)
    # 替换前缀
    if 'redshift' in view_name.lower():
        copy_sql = re.sub(r'view_redshift', 'ec', copy_sql, flags=re.IGNORECASE)
    if 'view_mongo' in copy_sql.lower():
        copy_sql = re.sub(r'view_mongo', 'ec', copy_sql, flags=re.IGNORECASE)
    if 'clinicmpi' in copy_sql.lower() and view_name == 'ClinicMPI':
        copy_sql = re.sub(r'clinicmpi', 'com_clinicmpi', copy_sql, flags=re.IGNORECASE)
    if 'mpatient' in copy_sql.lower() and view_name == 'MPatient':
        copy_sql = re.sub(r'mpatient', 'com_mpatient', copy_sql, flags=re.IGNORECASE)

    str_copy = '''{0} 's3://dws3-eugene-sftp/export/{1}/{2}.gz'
       credentials 'aws_access_key_id=AKIAJRIFKFAO2ABSNV7A;aws_secret_access_key=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'
       CSV ACCEPTINVCHARS GZIP DELIMITER as '^' NULL AS 'null' REGION AS 'us-east-1';\n'''.format(copy_sql, clinic_id, file_name)
    return str_copy


def load_gz_file(file_name, clinic_id):
    '''根据文件生成去查询sql数据库，生成控制文件，直接载入'''
    gz_content = get_descri_by_table_name(clinic_id, file_name)
    print(gz_content)
    with open('descript.txt', 'a+', encoding='utf8') as des_file:
        des_file.write(gz_content + '\n')
    return gz_content


def exe_control_sql(control_file):
    control_sql_list = []
    cur_sql = ''
    # 读取文件内容
    with open(control_file, 'r', encoding='utf8') as des_file:
        total_lines = des_file.readlines()
        for cur_line in total_lines:
            if cur_line.strip() != '':
                cur_sql += cur_line
            else:
                control_sql_list.append(cur_sql)
                cur_sql = ''
    print('Total:{}'.format(len(control_sql_list)))
    for sql_item in control_sql_list:
        print(sql_item)



if __name__ == "__main__":
    # content = load_gz_file('View_mongo_patient.0001.csv', 1004)
    exe_control_sql('descript.txt')