from common.utils import *
import zipfile,os
import boto3
import psycopg2
import  gzip
from boto3.session import Session

session = Session(aws_access_key_id='AKIAJRIFKFAO2ABSNV7A',
                  aws_secret_access_key='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')

uploaded_directory = Global.base_dir + '/uploaded/'


def upload_file():
     s3 = session.resource('s3')
     s3BucketName = 'dws3-eugene-sftp'
     bucket = s3.Bucket(s3BucketName)
     dirpath = Global.redshift_dir
     dirs = os.listdir(dirpath)
     for clinic_dir in dirs:
         sql_file_list = os.listdir(dirpath + clinic_dir)
         print('Search: ' + dirpath + clinic_dir)
         i = 0
         total = len(sql_file_list)
         for file in sql_file_list:
             if (file.endswith('.csv')) and 'working' not in file:
                 i += 1
                 print('------------{}/{}   {}_{}------------------'.format(i,total,clinic_dir,file))
                 # Upload a new file
                 old_file_path = dirpath + clinic_dir + '/' + file
                 zip_path = gzip_file(old_file_path)
                 server_file = clinic_dir + '/' + file + '.gz'
                 data = open(zip_path, 'rb')
                 res= bucket.put_object(Key='export/' + server_file, Body=data)
                 data.close()
                 print(res)
                 if not file.endswith('.txt'):
                     remove_file(zip_path)
                     # 转移到完成目录
                 move_to_completed_directory(file, clinic_dir, old_file_path)


def gzip_file(old_file_pah):
    f_in = open(old_file_pah, "rb")  # 打开文件
    f_out = gzip.open(old_file_pah + ".gz", "wb")  # 创建压缩文件对象
    f_out.writelines(f_in)
    f_out.close()
    f_in.close()
    return old_file_pah + ".gz"


def move_to_completed_directory(filename, clinic_dir, old_file_path):
    # 移到 uploaded 文件夹中
    if not os.path.exists(uploaded_directory):
        os.mkdir(uploaded_directory)
    if not os.path.exists(uploaded_directory + clinic_dir):
        os.mkdir(uploaded_directory + clinic_dir)
    if os.path.exists(uploaded_directory + clinic_dir + '/' + filename):
        remove_file(uploaded_directory + clinic_dir + '/' + filename)
    os.rename(old_file_path, uploaded_directory + clinic_dir + '/' + filename)



def remove_file(path):
    os.remove(path)


if __name__ == "__main__":
    upload_file()

