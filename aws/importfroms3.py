from common.utils import *
import zipfile,os
import boto3
import psycopg2
import  gzip
from boto3.session import Session

session = Session(aws_access_key_id='AKIAJRIFKFAO2ABSNV7A',
                  aws_secret_access_key='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')


def un_zip(file_name):
    zip_file = zipfile.ZipFile(file_name)
    localdir = file_name[0: file_name.rfind('/')]
    for names in zip_file.namelist():
        zip_file.extract(names, localdir)
    zip_file.close()

def get_s3_dir():
    s3 = session.resource('s3')

    bucket = s3.Bucket('dws3-eugene-sftp')
    obj_list = bucket.objects.filter(Prefix='export/', Delimiter='/')
    for s3obj in obj_list:
        print(s3obj.key)

    client = session.client('s3')
    result = client.list_objects(Bucket='dws3-eugene-sftp',Prefix='export/', Delimiter='/')

    for o in result.get('CommonPrefixes'):
        print ('sub folder : {0}'.format(o.get('Prefix')))


def scan_asw_s3(clinic_id):
    #client = boto3.client('s3')
    #client = session.client('s3')
    s3 = session.resource('s3')
    s3BucketName = 'dws3-eugene-sftp'
    bucket = s3.Bucket(s3BucketName)
    s3_export_dir = 'export' #test or export
    newFolderKey = s3_export_dir +'_downloaded/' + str(clinic_id)

    for s3obj in bucket.objects.filter(Prefix="{1}/{0}/".format(clinic_id, s3_export_dir)):
        srcKey = s3obj.key
        if srcKey.endswith('gz'):
            #print(key)
            obj = bucket.Object(srcKey)
            localpath = '{0}{1}/'.format(Global.export_dir, clinic_id)

            if not os.path.exists(localpath):
                os.mkdir(localpath)
            #print(localpath)

            zip_filename = srcKey[srcKey.rfind('/') + 1:]
            #down load
            localfile = localpath + zip_filename
            obj.download_file(localfile)
            un_zip(localfile)
            os.remove(localfile)

            copySource = s3BucketName + '/' + srcKey
            destFileKey = newFolderKey + '/' + zip_filename
            s3.Object(s3BucketName, destFileKey).copy_from(CopySource=copySource)
            s3.Object(s3BucketName, srcKey).delete()
            break

def upload_file():
     s3 = session.resource('s3')
     s3BucketName = 'dws3-eugene-sftp'
     bucket = s3.Bucket(s3BucketName)
     dirpath = Global.redshift_dir
     dirs = os.listdir(dirpath)
     for clinic_dir in dirs:
         sql_file_list = os.listdir(dirpath + clinic_dir)
         print('Search: ' + dirpath + clinic_dir)
         i = 0
         total = len(sql_file_list)
         for file in sql_file_list:
             if (file.endswith('.csv')) and 'working' not in file:
                 i += 1
                 # Upload a new file
                 old_file_path = dirpath + clinic_dir + '/' + file
                 zip_path = gzip_file(old_file_path)
                 server_file = clinic_dir + '/' + file + '.gz'
                 data = open(zip_path, 'rb')
                 res= bucket.put_object(Key='test/' + server_file, Body=data)
                 print(res)


def gzip_file(old_file_pah):
    f_in = open(old_file_pah, "rb")  # 打开文件
    f_out = gzip.open(old_file_pah + ".gz", "wb")  # 创建压缩文件对象
    f_out.writelines(f_in)
    f_out.close()
    f_in.close()
    return old_file_pah + ".gz"

def execute_sql():
    conn = psycopg2.connect(dbname='ec_central', host='mso-dw-instance.co8icnzqzo5f.us-east-1.redshift.amazonaws.com', port='5439', user='awsuser', password='MDLand2018')
    cur = conn.cursor()
    cur.execute('insert into test values(2)')
    conn.commit()
    print('success ')


if __name__ == "__main__":
    #scan_asw_s3(1001)
    #get_s3_dir()
    upload_file()