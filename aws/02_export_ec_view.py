import pyodbc, os, csv, re, time
from common.utils import *
from common.settings import *
from bson import ObjectId
from datamg.redshiftexport import export_tables_has_clinicid

'''parse data/ec_view_mongo_sql.sql'''
def get_view_list():
    view_list = []
    return list

def export_ec_view_data(clinic_id):
    view_list = []
    webid = Global.get_webid_by_clinic(clinic_id)
    conn_str = Global.get_connection_str(clinic_id)
    print(conn_str)

    dbview_list = get_view_list()
    for dbview in dbview_list:
        sql = ''
        #export(sql)

def export_super_group_1003():
    clinic_list = [1001, 1196, 2013, 3171, 3198, 3229, 3281, 3332, 1004, 1061]
    for cli in clinic_list:
        # print(Global.get_connection_str(cli))
        export_ec_view_data(cli)

def export_view_by_clinic():
    cli = 1001
    export_tables_has_clinicid(cli, is_view=True)

if __name__ == "__main__":
    # export_super_group_1003()
    export_view_by_clinic()


