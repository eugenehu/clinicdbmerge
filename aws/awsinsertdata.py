import os, time
from common.settings import *
from common.utils import DbUtilRedshift
from datamg.importsql import ImportSqlFile

def scan_and_execute_loop():
    while True:
        scan_and_execute()
        print('Thread Sleep 10s')
        time.sleep(10)

def scan_and_execute():
    db = DbUtilRedshift()
    imf = ImportSqlFile(db)
    imf.process(Global.export_dir)

def test_import_one_redshift():
    db = DbUtilRedshift()
    imf = ImportSqlFile(db)
    imf.import_one_file_batch_commit('1001','DM_ActionGuideline.sql')

if __name__ == "__main__":
    #scan_and_execute()
    test_import_one_redshift()
