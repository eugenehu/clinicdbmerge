import os, os.path, pprint, string

class OutFile(object):
    """ To write in the outfile
    """
    def __init__(self, file_name, write_mode='w'):
        self.outFile = file_name
        df=open(self.outFile, write_mode)
        df.close()

    def write(self, *msg):
        df=open(self.outFile,'a')
        for m in msg:
            if type(m) is dict or type(m) is list:
                df.write("%s\n" % pprint.pformat(m))
            else:
                df.write("%s\n" % str(m))
        df.close()