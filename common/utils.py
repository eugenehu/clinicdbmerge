import pyodbc
import logging, datetime
#用字典保存日志级别
import os, platform
from common.settings import *
import psycopg2


class DbUtil:

    def __init__(self, conn_str, db_name=''):
        self._conn_str = conn_str
        self._db_name = db_name
        self._conn = pyodbc.connect(self._conn_str)
        self._cursor = None

    @property
    def db_name(self):
        return self._db_name

    '''return cursor'''
    def get_cursor(self):
        cursor = self._conn.cursor()
        self._cursor = cursor
        return cursor

    def commit(self):
        if self._cursor is not None:
            self._cursor.commit()

    def get_error(self):
        return pyodbc.Error

    '''execute sql , return array'''
    def query_sql_to_list(self, sql):
        cursor = self._conn.cursor()
        cursor.execute(sql)
        res_array = []
        for row in cursor:
            res_array.append(row)

        #for row in cursor.description:
        #   print (row[0])
        return res_array

    '''return a int number'''
    def query_sql_to_int(self, sql):
        res_array = self.query_sql_to_list(sql)
        if len(res_array) > 0:
            res = res_array[0][0]
            if res is None:
                return 0
            else:
                return res
        return -1

    def get_tables(self):
        table_cursor = self.get_cursor().tables(schema='dbo', tableType='TABLE')
        table_list = []
        for tbl in table_cursor:
            table_list.append(tbl)
        return table_list

    def get_views(self):
        view_cursor = self.get_cursor().tables(schema='dbo', tableType='VIEW')
        view_list = []
        for tbl in view_cursor:
            view_name = tbl.table_name.lower()
            if view_name.startswith('view_mongo'):
                view_list.append(tbl)
        return view_list

    def get_columns(self, table_name):
        cursor = self.get_cursor().columns(table=table_name)
        col_list = []
        for col in cursor:
            col_list.append(col)
        return col_list

    def get_column(self, table_name, col_name):
        col_list = self.get_columns(table_name)
        col_name_lower = col_name.lower()
        for col in col_list:
            if col.column_name.lower() == col_name_lower:
                return col
        return None

    def get_primary_keys(self, table_name):
        cursor = self.get_cursor().primaryKeys(table=table_name)
        row_list = []
        for row in cursor:
            row_list.append(row)
        return row_list

    ''':return true when Clinic Is in the list'''
    def has_column_clinic_id(self, col_list):
        for col in col_list:
            if col.column_name.lower() == 'clinicid':
                return True
        return False

    ''':return true when PatientID in the list'''

    def has_column_patient_id(self, col_list):
        for col in col_list:
            if col.column_name.lower() == 'patientid':
                return True
        return False

    def get_tables_has_visitid(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_patient = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'visitid':
                    table_has_patient = True
                    break

            if not table_has_patient:
                continue

            result_list.append(table)
        return result_list

    ''':返回带有类似date字段的名称列表 '''

    def get_col_list_has_date(self, col_list):
        date_col_list = []
        for col in col_list:
            if 'date' in col.column_name.lower():
                date_col_list.append(col.column_name)
        return date_col_list

    ''':return true when VisitID in the list'''

    def has_column_visit_id(self, col_list):
        for col in col_list:
            if col.column_name.lower() == 'visitid':
                return True
        return False

    def get_tables_has_billingid(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_patient = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'billingid':
                    table_has_patient = True
                    break

            if not table_has_patient:
                continue

            result_list.append(table)
        return result_list

    ''':return true when BillingID in the list'''

    def has_column_billing_id(self, col_list):
        for col in col_list:
            if col.column_name.lower() == 'billingid':
                return True
        return False

    def get_tables_has_labid(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_patient = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'labid':
                    table_has_patient = True
                    break

            if not table_has_patient:
                continue

            result_list.append(table)
        return result_list

    ''':return true when LabID in the list'''

    def has_column_lab_id(self, col_list):
        for col in col_list:
            if col.column_name.lower() == 'labid':
                return True
        return False

    def get_tables_has_inboxid(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_patient = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'inboxid':
                    table_has_patient = True
                    break
            if not table_has_patient:
                continue

            result_list.append(table)
        return result_list

    ''':return true when InboxID in the list'''

    def has_column_inbox_id(self, col_list):
        for col in col_list:
            if col.column_name.lower() == 'inbox':
                return True
        return False

    def get_tables_has_patientid(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_patient = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'patientid':
                    table_has_patient = True
                    break

            if not table_has_patient:
                continue

            result_list.append(table)
        return result_list

    ''' 增量更新时 获取 不含有visit_id lab_id billing_id 的table'''

    def get_tables_has_patientid_without_other_id(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_patient = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'patientid':
                    table_has_patient = True
                    continue
                if col.column_name.lower() in ['visitid', 'labid', 'billingid']:
                    table_has_patient = False
                    break

            if not table_has_patient:
                continue

            result_list.append(table)
        return result_list

    def get_identity_column(self):
        sql = ("SELECT  obj.name table_name, col.name column_name, col.system_type_id, col.max_length ,col.is_identity FROM sys.all_objects obj  "
                    "inner join sys.all_columns col "
                    "ON  obj.object_id =col.object_id " 
                    "WHERE col.is_identity =1 and obj.type='U' order by 1")
        col_list = self.query_sql_to_list(sql)
        res_list = []
        for col in col_list:
            table_name = col.table_name
            if Global.is_ignore_table(table_name):
                continue
            res_list.append(col)

        return col_list

    '''return all tables which contain ClinicID
       require_int_pk = True , only the pk is int will return
    '''
    def get_tables_has_clinicid(self, require_int_pk = False ):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            table_has_clinic_id = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'clinicid':
                    table_has_clinic_id = True
                    break

            if not table_has_clinic_id:
                continue

            if require_int_pk:
                is_int_pk = False
                '''check if the table's pk is int'''
                pk_list = self.get_primary_keys(table.table_name)
                if len(pk_list) == 1:
                    col_name = pk_list[0].column_name
                    col = self.get_column(table.table_name, col_name)
                    if col is not None and Utils.is_numeric_type(col.column_name):
                        is_int_pk = True
                ''''''
                if is_int_pk:
                    result_list.append(table)
            else:
                result_list.append(table)
            '''end of for table'''

        return result_list

    '''return all views which contain ClinicID
              require_int_pk = True , only the pk is int will return
       '''

    def get_view_has_clinicid(self, require_int_pk=False):
        result_list = []
        view_list = self.get_views()

        for table in view_list:
            table_has_clinic_id = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'clinicid':
                    table_has_clinic_id = True
                    break

            if not table_has_clinic_id:
                continue

            if require_int_pk:
                is_int_pk = False
                '''check if the table's pk is int'''
                pk_list = self.get_primary_keys(table.table_name)
                if len(pk_list) == 1:
                    col_name = pk_list[0].column_name
                    col = self.get_column(table.table_name, col_name)
                    if col is not None and Utils.is_numeric_type(col.column_name):
                        is_int_pk = True
                ''''''
                if is_int_pk:
                    result_list.append(table)
            else:
                result_list.append(table)
            '''end of for table'''

        return result_list

    '''table has clinic_id ,and the pk is not int'''
    def get_tables_has_clinicid_no_intpk(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            if Global.is_ignore_table(table.table_name):
                continue

            table_has_clinic_id = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'clinicid':
                    table_has_clinic_id = True
                    break

            if not table_has_clinic_id:
                continue

            pk_list = self.get_primary_keys(table.table_name)
            if len(pk_list) != 1:
                result_list.append(table)

            else:
                col_name = pk_list[0].column_name
                col = self.get_column(table.table_name, col_name)
                if not Utils.is_numeric_type(col.column_name):
                    result_list.append(table)

        return result_list


    '''return all tables which don't contain ClinicID'''
    def get_tables_without_clinicid(self):
        result_list = []
        table_list = self.get_tables()

        for table in table_list:
            has_clinicid = False
            column_list = self.get_columns(table.table_name)
            for col in column_list:
                if col.column_name.lower() == 'clinicid':
                    has_clinicid = True
                    break
            if not has_clinicid:
                result_list.append(table)

        return result_list

    ''' '''
    def is_table_without_columns(self,cols):
        return True

    def generate_truncate_all_table(self):
        table_list = self.get_tables()
        print ('use ec_central')
        for tbl in table_list:
            str_line = 'if exists (select 1 from sys.all_objects where name =\'{0}\' and type =\'U\')   truncate table {0} ;'.format(tbl.table_name)
            print(str_line)

class DbUtilRedshift:
    def __init__(self):
        self._conn = None

    def get_conn(self):
        if self._conn is None:
            self._conn = psycopg2.connect(dbname='ec_central', host='mso-dw-instance.co8icnzqzo5f.us-east-1.redshift.amazonaws.com', port='5439', user='awsuser',
                                    password='MDLand2018')
        return self._conn

    def get_cursor(self):
        conn = self.get_conn()
        cur = conn.cursor()
        return cur

    def commit(self):
        self._conn.commit()

    def get_error(self):
        return 'psycopg2.error'

class IdentityStratify(object):
    columm_keep_orginal = ['MPatientID', 'ClinicID','ContactClinicID']
    columm_must_change = ['PatientID', 'DoctorID','BillingID', 'VisitID', 'OVID', 'PatientRefDoctorID','CreateBy', 'CDID', 'LastModifier', 'ContactEmployeeID', 'MyEmployeeID']
    columm_identity = []

    def __init__(self):
        IdentityStratify.columm_identity.clear()
        db = DbUtil(Global.db_conn_str1)
        id_list = db.get_identity_column()
        for id_col in id_list:
            id_name = id_col.column_name
            IdentityStratify.columm_identity.append(id_name)
        Global.make_lower_letter(IdentityStratify.columm_keep_orginal)
        Global.make_lower_letter(IdentityStratify.columm_must_change)
        Global.make_lower_letter(IdentityStratify.columm_identity)

    '''if identity file, then the id need to be change to db*10e7 
        since many table has 'ID' column ,need double checked, from db we know they are all is_identity
    '''
    def is_identity_column(self, col):
        col_name = col.column_name.lower()

        if col_name == 'id':
            if col.type_name in ['int']:
                return True

            if col.type_name.find('identity') != -1:
                #print('is_identity_column ' + col)
                return True
            return False

        if col_name in IdentityStratify.columm_keep_orginal:
            return False

        if col_name in IdentityStratify.columm_must_change:
            return True

        if col_name in IdentityStratify.columm_identity:
            return True

        return False


format_dict = {
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
}

logger_dict = {}

# 开发一个日志系统， 既要把日志输出到控制台， 还要写入日志文件
class Logger():

    def __init__(self, logger_name='fox', mode = 'a', encoding = 'utf-8', delay=False):
        nowdate = datetime.datetime.now().strftime('%Y-%m-%d')

        file_path = Global.log_dir +'runtime/'
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        logname = file_path + '{0}_{1}.log'.format(logger_name, nowdate)

        #logger = "fox"
        '''
           指定保存日志的文件路径，日志级别，以及调用文件
           将日志存入到指定的文件中
        '''

        # 创建一个logger
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)

        # 创建一个handler，用于写入日志文件
        fh = logging.FileHandler(logname)
        fh.setLevel(logging.DEBUG)

        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        #formatter = format_dict[int(loglevel)]
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        if not self.logger.handlers:
            # 给logger添加handler
            self.logger.addHandler(fh)
            self.logger.addHandler(ch)

    def get_logger(self):
        return self.logger

    @staticmethod
    def get_common_logger():
        cm_logger = Logger('common').get_logger()
        return cm_logger

    @staticmethod
    def get_error_logger():
        err_logger = Logger('error').get_logger()
        return err_logger


class Utils(object):
    @staticmethod
    def sql_encode(s):
        res = s
        res = s.replace("&lt;", "<")
        return res

    @staticmethod
    def is_numeric_type(type_name):
        if type_name in ['int', 'long', 'bigint','tinyint','money' , 'smallint', 'decimal','real' ,'float' , 'numeric']:
            return True
        return False

    @staticmethod
    def replace_sql_str(txt):
        if txt is None:
            return ''
        txt = txt.replace('\'', '\'\'')
        txt = txt.replace('\\', '\\\\') #redshift
        return txt

    @staticmethod
    def replace_cvs_str(txt):
        if txt is None:
            return ''
        txt = txt.replace('"', '""').replace('\'', '"').replace('\\', '\\\\')
        return txt

    @staticmethod
    def get_value_with_quot(col, col_value, db_id, id_increase, is_csv=False):
        if col_value is None :
            return "null"

        type_name = col.type_name
        if type_name in ['bit']:
            if col_value:
                return 1
            else:
                return 0

        if Utils.is_numeric_type(type_name):
            if col_value == id_increase:
                return 0
            return col_value

        if type_name in ['datetime', 'smalldatetime']:
            str_val = col_value.strftime("%m/%d/%Y %H:%M:%S")
            if str_val.endswith('00:00:00'):
                str_val = str_val[0:10]
            if is_csv:
                return col_value.strftime('%Y-%m-%d %H:%M:%S') + '+00'
            return "\'{0}\'".format(str_val)

        elif type_name in ['char', 'varchar', 'nchar', 'nvarchar']:
            if not str(col_value).strip():
                return "null"
            if type_name == 'nchar' and len(col_value) < 3:
                return "null"
            col_value = col_value.strip()
            col_value = col_value.replace('^','')
            if is_csv:
                col_value = Utils.replace_cvs_str(col_value)
                if len(col_value) >= 40000:
                    col_value = col_value[:40000]
                return col_value
            col_value = Utils.replace_sql_str(col_value)
            if len(col_value) >= 40000:
                col_value = col_value[:40000]
            return '\'{0}\''.format(col_value)

            col_value = col_value.replace('\r', '\n')
            if col_value.find('\n') == -1:
                return '\'{0}\''.format(col_value)
            else:
                rows = col_value.split('\n')
                res = ''
                for row in rows:
                    row = row.replace('\r', '')
                    res += '\'{0}\' + \'\n\' + '.format(row) #CHAR(13)
                res = res[0:len(res) - len('+ \'\n\' + ')]
                return res
        elif type_name in ['uniqueidentifier']:
            if len(col_value) != 36:
                return 'null'
            #return '\'{0}{1}\''.format(db_id, col_value[2:])
            if is_csv:
                return col_value
            return '\'{0}\''.format(col_value)
        elif type_name in ['varbinary']:
            #str_val = col_value.decode('utf-8')
            #self._logger.info('   get_value_with_quot({0}) = {1}'.format(type_name, col_value))
            return 'null'
            #return '\'{0}\''.format(str_val)

        print('{0} utils.get_value_with_quot({1}) = {2}'.format(col, type_name, col_value))
        if is_csv:
            return '{0}'.format(col_value)
        return '\'{0}\''.format(col_value)

def main():
    #logname='log.txt', loglevel=1, logger="fox"
    #logger = Logger(logger_name="fox").get_common_logger()
    #logger.info('test')
    #logger.error('error')
    db = DbUtil(Global.db_conn_str1)
    db.generate_truncate_all_table()
    '''
    table_list = db.get_tables_has_clinicid_no_intpk()
    print(len(table_list))
    for table in table_list:
        print(table)
    '''

if __name__ == '__main__':
    main()