import openpyxl,os
from common.replicationdb import SERVER_DB

class Global(object):
    """ Class used to store global parameters
    """
    db_conn_pro_21 = "Driver={SQL Server Native Client 11.0}; Server=192.168.168.65; Database=ec21; uid=ecuser;pwd=1234"

    db_name_1 = 'ec21'
    db_name_2 = 'ec59'

    db_conn_str1 = "Driver={SQL Server Native Client 11.0}; Server=192.168.168.85; Database=ec21; uid=ecuser;pwd=1234"
    db_conn_str2 = "Driver={SQL Server Native Client 11.0}; Server=127.0.0.1; Database=ec59; uid=ecuser;pwd=1234"

    db_conn_masterdb = "Driver={SQL Server Native Client 11.0}; Server=192.168.168.66; Database=MasterDB; uid=ecuser;pwd=1234"

    db_conn_central = "Driver={SQL Server Native Client 11.0}; Server=127.0.0.1; Database=ec_central; uid=ecuser;pwd=1234"

    base_dir = 'D:/Workspace/WS-iClinic/DbMerge'
    export_dir = base_dir + '/export/'
    redshift_dir = base_dir + '/redshift/'
    import_working = base_dir + '/import_working/'
    import_done = base_dir + '/import_done/'
    log_dir = base_dir + '/log/'

    web_clinic_config = {}  #{'Web': {21:[1004,1007,1098]}, 'Clinic': {1004:21 , 1007:21}}
    server_database = {}

    running_mode = 'pro'
    uat_db_conn_str = 'Driver={SQL Server Native Client 11.0}; Server=192.168.168.81; Database=ec01_122; uid=ecuser;pwd=1234'
    uat_web_id = 10

    tables_with_clinic = []
    tables_hasnt_clinic = []

    @staticmethod
    #OutBox has SendClnicID and Receieve Clinic, n
    def is_ignore_table(table_name):
        ignore_start = ['Log', '_', 'PT_3', 'Temp', 'del_','tmp','sys','MSs','MSr']
        ignore_inword = ['Delete', '_Log', 'History', 'Sheet1$', '_New','_Patch', '2009',' _2016','_2015', 'for_bug' ,'BackUp','Patch', 'Ref_Zipcode2010', '_2017', 'Billing_1',
                       'Clinic_GreetingSound','Pati_NameSound', 'Pati_Signature', 'Ref_CareTeam_Member', 'Ref_CareTeam',
                       'Ref_UpgradeTo122', 'Ref_Taxonomy20','Ref_Templet20',
                       'Ref_PayerID2009', 'Ref_NDC2RxNorm_2','ref_medicarefeeschedule2009', 'Ref_UpgradeTo122',
                       'Ref_MeasureValueSet', 'Audit_Forms_General_', 'Boston_Bug4601','Employee_Vacation2010' , 'Employee_UpgradeTo122Info', 'Medi_OV_PRES2004',
                        'work_to_do' , 'Patient_For', 'Ref_ICD2011', 'Ref_ICD2010' ,'Ref_PayerID2015', 'Ref_Options_20161123', 'Pati_InsuPlan_EligibilityResult'
                         ,'DM_ActionLog_Report','OutBox', 'Medi_OV_RiskAE','drug_RedayConvertedDrugList', 'Ref_CPT_2016_Update','ref_cptnewfee2015','sysdiagrams',
                         'dtproperties','Ref_MOD2007' ,'ref_cptnewfee2015', 'ref_cptnews2015', 'ref_cptnewl2015', 'ref_cptnewfix2015','iClinic_Config','ElgRequests',
                         'HealthFirst_Analytics','MedicaidPopulation2016' ,'Pati_Daily_Tracking', 'Acct_EOBModify_Record','supportHelpInfo' ,'SyncTableReocrd','cchit_ShaFiles',
                         'Medi_Billing_Rec','Form_PHQ9TeenScreening_20160225','Ref_CPT2015' ,'Ref_CPT2010', 'Medi_OV_Record',
                         'Pati_Check', 'Pati_InsuCard', 'Pati_DiagPic','Pati_Picture' ,'syssubscriptions','tblSysConfig' ,'MSpeer_lsns','MSreplication_subscriptions',
                         'MSpeer_','MSreplication']
        ignore_end = ['2', '_3', '_2016', 'Temp', '_bak','Log','2004']

        Global.make_lower_letter(ignore_start)
        Global.make_lower_letter(ignore_inword)
        Global.make_lower_letter(ignore_end)

        tbl_name = table_name.lower()

        table_withour_idchage = ['Ref_CPT_Modifier']

        for word in ignore_start:
            if tbl_name.startswith(word):
                return True

        for word in ignore_inword:
            if tbl_name == word or tbl_name.find(word) > -1:
                return True

        for word in ignore_end:
            if tbl_name.endswith(word):
                return True

        return False


    @staticmethod
    def clear_ignore_table(table_list):
        idx = 0

        while idx < len(table_list):
            tbl = table_list[idx]
            tbl_name = tbl.table_name
            if Global.is_ignore_table(tbl_name):
                table_list.remove(tbl)
            else:
                idx += 1

    @staticmethod
    def make_lower_letter(word_list):
        for idx in range(len(word_list)):
            word_list[idx] = word_list[idx].lower()

    @staticmethod
    def get_webid_by_clinic(clinic_id):
        '''test enviroment'''
        if Global.running_mode == 'uat':
            return Global.uat_web_id

        '''production'''
        keys = Global.web_clinic_config.keys()
        if len(keys) == 0:
            Global.int_webserver_clinic_from_excel()

        if clinic_id in Global.web_clinic_config['Clinic']:
            return Global.web_clinic_config['Clinic'][clinic_id]

        return -1

    @staticmethod
    def int_webserver_clinic_from_excel():
        dict_web = {}    #{21:[1004, 1063] }
        dict_clinic = {} #{1004:21, 1063:21}

        cur_path = os.path.dirname(os.path.abspath(__file__))
        #print(cur_path)
        wb = openpyxl.load_workbook(cur_path + '\Clinic-List.xlsx')
        sheet = wb.active
        for row in sheet.iter_rows():
            clinic_id = row[0].value
            web_server = row[9].value #Web52
            if not isinstance(clinic_id, int):
                continue
            if len(web_server) > 5:
                continue
            web_id = int(web_server.replace('Web', ''))
            #print('{0},{1},{2}'.format(clinic_id, web_server, db_id))
            dict_clinic[clinic_id] = web_id
            if web_id in dict_web:
                dict_web[web_id].append(clinic_id)
            else:
                dict_web[web_id] = [clinic_id]

        Global.web_clinic_config = {'Web': dict_web, 'Clinic': dict_clinic}


    @staticmethod
    def get_connection_str(clinic_id):
        if Global.running_mode == 'uat':
            return Global.uat_db_conn_str

        webid = Global.get_webid_by_clinic(clinic_id)
        ip = ''
        for item in SERVER_DB:
            if item['dbid'] == str(webid):
                ip = item['ip']
                break
        return ('Driver={{SQL Server Native Client 11.0}}; Server={0}; Database=ec{1}; uid=ecuser;pwd=1234').format(ip, webid)


if __name__ == "__main__":

    conn_str = Global.get_connection_str(1001)
    print(conn_str)

