import os


def split_big_file():
    working_file = 'D:/Workspace/WS-iClinic/DbMerge/runtime_log/error_2018-12-19.log'

    fo = open(working_file, "r", encoding='utf-8')
    line = fo.readline()
    file_idx = 0
    idx = 0
    new_file_fmt = 'D:/Workspace/WS-iClinic/DbMerge/runtime_log/split/error_2018-12-19_sml{0}.log'
    outf = open(new_file_fmt.format('{:03}'.format(file_idx)), 'w', encoding='utf-8')
    while line:
        idx += 1
        line = fo.readline()
        outf.write(line)
        if idx > 10e5:
            idx = 0
            outf.close()
            file_idx += 1
            outf = open(new_file_fmt.format('{:03}'.format(file_idx)), 'w', encoding='utf-8')

    outf.close()

if __name__ == "__main__":
    split_big_file()