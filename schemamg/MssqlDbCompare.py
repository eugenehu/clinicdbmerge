import string, pprint
from common.utils import *
from common.settings import *
from common.outfile import OutFile


class dbCompare(object):
    """ Core function to compare the DBs
    """
    def __init__(self):
        print('db compare is created')

    def process(self):
        db1 = DbUtil(Global.db_conn_str1, Global.db_name_1)
        db2 = DbUtil(Global.db_conn_str2, Global.db_name_2)

        table_list_1 = db1.get_tables()
        Global.clear_ignore_table(table_list_1)

        table_list_2 = db2.get_tables()
        Global.clear_ignore_table(table_list_2)

        #output the whole table list
        self.print_table_list(table_list_1)

        cp = self.compare_table_list(table_list_1, table_list_2)
        #out put the difference result
        self.print_table_difference(cp)

        idx = 0
        for tbl in table_list_1:
            table_name = tbl.table_name
            col_list_1 = db1.get_columns(table_name)
            col_list_2 = db2.get_columns(table_name)
            idx += 1
            print(idx)
            db1_lackcol_list = self.compare_column_list(col_list_1, col_list_2)
            self.print_column_list(db1_lackcol_list)

    def compare_table_list(self,table_list_1,table_list_2):
        result = {'l1notInl2': [],
                  'l2notInl1': []}

        table_name_list_1 = [x.table_name.lower() for x in table_list_1]
        table_name_list_2 = [x.table_name.lower() for x in table_list_2]
        d1 = dict(zip(table_name_list_1, table_list_1))
        d2 = dict(zip(table_name_list_2, table_list_2))

        for tbl in d1:
            if tbl not in d2:
                result['l1notInl2'].append(d1[tbl])

        for tbl in d2:
            if tbl not in d1:
                result['l2notInl1'].append(d2[tbl])

        return result

    def compare_column_list(self, center_col_list, source_col_list):
        center_col_name_list = [x.column_name.lower() for x in center_col_list]
        source_col_name_list = [x.column_name.lower() for x in source_col_list]

        dict_center = dict(zip(center_col_name_list, center_col_list))
        dict_source = dict(zip(source_col_name_list, source_col_list))

        notin_center_list = []

        for col_name in source_col_name_list:
            if col_name not in dict_center:
                notin_center_list.append(dict_source[col_name])

        return notin_center_list


    def print_table_difference(self, cp):
        of = OutFile(Global.log_dir + 'dbcompare_difference.txt')
        of.write("Different tables")

        if cp['l1notInl2'] != []:
            of.write("   --> Tables from %s missing in %s" % (db1.db_name, db2.db_name))
            for tbl in cp['l1notInl2']:
                of.write(tbl)
        of.write('')
        if cp['l2notInl1'] != []:
            of.write("   --> Tables from %s missing in %s" % (db2.db_name, db1.db_name))
            for tbl in cp['l2notInl1']:
                of.write(tbl)


    def print_table_list(self, table_list):
        of = OutFile(Global.log_dir + 'dbcompare_table_list.txt')
        for idx in range(len(table_list)):
            of.write('{0} {1}'.format(idx, table_list[idx]))

    def print_column_list(self, column_list):
        if len(column_list) == 0 :
            return
        of = OutFile(Global.log_dir + 'dbcompare_column_list.txt', 'a')
        for idx in range(len(column_list)):
            of.write('{0} {1}'.format(idx, column_list[idx]))


if __name__ == "__main__":
    cmp = dbCompare()
    cmp.process()

