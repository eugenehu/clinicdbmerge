import pyodbc
from common.utils import *
from common.outfile import *
from common.settings import *


class DbCreateTable:
    def __init__(self):
        self._conn = "Driver={SQL Server Native Client 11.0}; Server=192.168.168.85; Database=ec21; uid=ecuser;pwd=1234"
        self._db = DbUtil(self._conn)
        self._need_webid = True

    def set_need_web_id(self, value):
        self._need_webid = value

    def set_conn(self, conn):
        self._conn = conn
        self._db = DbUtil(conn)

    def get_table_create_sql(self, table_name):
        res = 'CREATE TABLE {0} (\n'.format(table_name)
        col_list = self._db.get_columns(table_name)
        idx = 0
        for col in col_list:
            data_type = self.data_type_redshift(col)
            res += ('    "{0}" {1},\n').format(col.column_name, data_type)
            idx += 1
        if self._need_webid:
            res += ' WebID int \n'
        else:
            res = res[0:len(res) - 2] + '\n'
        res += ');'
        return res


    def get_table_primary_keys(self, table_name):
        row_list = self._db.get_primary_keys(table_name)
        if len(row_list) == 0:
            return ''
        pk_name = row_list[0].pk_name
        pk_name = pk_name.replace(' ', '_')
        res = 'ALTER TABLE {0} ADD CONSTRAINT {1} PRIMARY KEY  ('.format(table_name, pk_name) #CLUSTERED
        idx = 0
        for row in row_list:
            res += '"{0}"'.format(row.column_name)  #Lock in billing_account need "" in postgress
            if idx != len(row_list)-1:
                res += ','
            idx += 1
        res += ');'
        return res

    def data_type_sql_server(self, col):
        res = ''
        type_name = col.type_name
        if type_name in ['nchar','nvarchar' , 'char','varchar']:
            col_size = col.column_size if col.column_size > 0 else 'max'
            res += ' {0}({1})'.format(type_name, col_size)
        elif type_name == 'int identity':
            res += ' int'
        else:
            res += ' ' + type_name

        if col.nullable == 0:
            res += ' NOT NULL'

        if col.column_def is not None:
            res += ' DEFAULT' + col.column_def

        return res

    def data_type_redshift(self, col):

        sqlserver_postgresql_diff = {
            'bit': 'SMALLINT'
            ,'money': 'DECIMAL(15,4)'
            , 'smalldatetime': 'TIMESTAMP'
            , 'tinyint': 'smallint'
            , 'uniqueidentifier': 'char(36)'
            #, 'varbinary': 'BYTEA' not support
        }

        res = ''
        type_name = col.type_name.lower()
        if type_name in ['nchar', 'nvarchar', 'char', 'varchar']:
            col_size = col.column_size if col.column_size > 0 else 'max'
            res += ' {0}({1})'.format(type_name, col_size)
        elif type_name == 'int identity':
            res += ' int'
        elif type_name in sqlserver_postgresql_diff:
            res += ' {0}'.format(sqlserver_postgresql_diff[type_name])
        else:
            res += ' ' + type_name

        if type_name in ['varbinary']:
            print(col)

        if col.nullable == 0:
            res += ' NOT NULL'

        if col.column_def is not None:
            column_def = col.column_def
            if column_def != '(newid())':
                res += ' DEFAULT' + col.column_def

        return res


def generate_central_database_schema():
    '''using web21 as standard database'''
    db = DbUtil(Global.db_conn_str1)
    table_list = db.get_tables()
    outfile = OutFile(Global.log_dir + 'central_db.sql')
    dbc = DbCreateTable()
    idx = 0
    for table in table_list:
        if Global.is_ignore_table(table.table_name):
            continue
        idx += 1
        table_name = table.table_name
        outfile.write('---{0} {1}---'.format(idx, table_name))
        outfile.write(dbc.get_table_create_sql(table_name))
        #outfile.write(dbc.get_table_primary_keys(table_name))
        outfile.write('\n')

def generate_central_mongo_view_schema():
    db = DbUtil(Global.db_conn_pro_21)
    table_list = db.get_views()
    outfile = OutFile(Global.log_dir + 'central_view.sql')
    dbc = DbCreateTable()
    idx = 0
    for table in table_list:
        if Global.is_ignore_table(table.table_name):
            continue
        idx += 1
        table_name = table.table_name
        outfile.write('---{0} {1}---'.format(idx, table_name))
        outfile.write(dbc.get_table_create_sql(table_name))
        # outfile.write(dbc.get_table_primary_keys(table_name))
        outfile.write('\n')

def generate_drop_table():
    #DROP TABLE IF EXISTS public.clinic; postgresql
    db = DbUtil(Global.db_conn_str1)
    table_list = db.get_tables()
    outfile = OutFile(Global.log_dir + 'central_db_drop.sql')
    for table in table_list:
        table_name = table.table_name
        if Global.is_ignore_table(table.table_name):
            continue
        outfile.write('DROP TABLE IF EXISTS public.{0};'.format(table_name))


def test_generate_sql():
    dbc = DbCreateTable()
    sql_create = dbc.get_table_create_sql('Clinic_GreetingSound')
    sql_pk = dbc.get_table_primary_keys('medi_officevisit')
    print(sql_create)
    print(sql_pk)

def test_analysis_db():
    db = DbUtil(Global.db_conn_str1)
    #table_list = db.get_tables_has_clinicid()
    table_list = db.get_tables_without_clinicid()
    idx = 0
    for table in table_list:
        idx += 1
        print('{0}.{1}'.format(idx, table))

if __name__ == "__main__":
    #test_generate_sql()
    #test_analysis_db()
    #generate_central_database_schema()
    #generate_drop_table()
    generate_central_mongo_view_schema()