import pyodbc, os
from common.utils import *
from common.settings import *

class ExportTable:

    def __init__(self, clinic_id):
        self._db_central = DbUtil(Global.db_conn_central)
        self._db_source = None
        self._db_id = -1
        self._id_increase = 0
        self._insert_sql = ''
        self._select_sql = ''
        self._col_list = []
        self._clinic_id = clinic_id

        self._table_name = ''
        self._table_has_column_clinicid = False

        self._logger = Logger(logger_name="ExportTable").get_logger()
        self._is_mode_test = False

    def set_source_db(self, db_id, conn_str):
        self._db_source = DbUtil(conn_str)
        self._db_id = db_id
        self._id_increase = int(db_id * 10e6)

    def export(self, table_name):
        self._table_name = table_name
        """"""
        self._logger.info('\nClinicID={0}, Table={1}'.format(self._clinic_id, self._table_name))
        self.init_export_sql()
        self._logger.info(self._insert_sql)
        self._logger.info(self._select_sql)
        sql_list = self.split_small_query()
        self._logger.info('The Table is Splited to {2} Batches to Export '.format(self._clinic_id, self._table_name, len(sql_list)))
        file_idx = 0
        for sql in sql_list:
            file_idx += 1
            self._logger.info( '  NO. {0} '.format(file_idx) + sql)
            self.export_insertsql_to_file(sql, file_idx)
            if self._is_mode_test:
                break#only export one file for test mode

    def init_export_sql(self):
        self._col_list = self._db_central.get_columns(self._table_name)
        if len(self._col_list) == 0:
            self._logger.info('{0} is not defined in center table'.format(self._table_name))
            return
        insert_sql = ''
        select_sql = ''

        id_stratify = IdentityStratify()

        for col in self._col_list:
            col_name = col.column_name
            insert_sql += '"{0}",'.format(col_name)

            select_sql += '{0},'.format(self.change_id_column(col, id_stratify))

        insert_sql = insert_sql[0: len(insert_sql) - 1]
        insert_sql = 'INSERT INTO {0} ({1}, WebID) VALUES '.format(self._table_name, insert_sql)
        self._insert_sql = insert_sql

        select_sql = select_sql[0: len(select_sql) - 1]
        select_sql = 'SELECT {2} {0} FROM {1} '.format(select_sql, self._table_name, 'top 10' if self._is_mode_test else '')
        self._table_has_column_clinicid = self._db_central.has_column_clinic_id(self._col_list)
        if self._table_has_column_clinicid:
            select_sql += ' WHERE ClinicID={0} '.format(self._clinic_id)
        self._select_sql = select_sql


    def change_id_column(self, col, id_stratify):
        col_name = col.column_name
        if True:
            return '[{0}]'.format(col_name)
        #
        #if col.type_name in ['uniqueidentifier']:
        #    return "concat('{1}',SUBSTRING(CONVERT(nvarchar(36), [{0}]),3,34 ))".format(col_name, self._db_id)

        #must be int
        if col.type_name not in ['int', 'bigint']:
            return '[{0}]'.format(col_name)

        is_id_field = id_stratify.is_identity_column(col)

        if is_id_field:
            sel_filed = ('[{0}]+{1}'.format(col_name, self._id_increase))
        else:
            sel_filed = '[{0}]'.format(col_name)
        return sel_filed


    def split_small_query(self):
        sql_list = []
        pk_row = self._db_source.get_primary_keys(self._table_name)
        if len(pk_row) != 1:
            sql_list.append(self._select_sql)
            return sql_list

        #print(pk_row)
        pk_column_name = pk_row[0].column_name
        #prepare to export data at most 100k records every time
        total_sql = 'SELECT COUNT(1), MAX({1}), MIN({1}) FROM {0}  '.format(self._table_name, pk_column_name)
        if self._table_has_column_clinicid:
            total_sql += ' WHERE ClinicID={0} '.format(self._clinic_id)

        total_sql = self._db_source.query_sql_to_list(total_sql)
        total_record = total_sql[0][0]
        max_id = total_sql[0][1]
        min_id = total_sql[0][2]
        self._logger.info('{0} , total = {1} , min({4}) = {2} , max({4}) ={3}'.format(self._table_name, total_record, min_id, max_id, pk_column_name))

        if not isinstance(max_id, int):
            sql_list.append(self._select_sql)
            return sql_list

        record_per_page = 5000
        if total_record <= record_per_page:
            sql_list.append(self._select_sql)
            return sql_list


        #total_pages = int((total_record + (record_per_page -1)) / record_per_page)
        #page_step = int((max_id - min_id + 1) / total_record * record_per_page)
        total_pages = int((max_id - min_id + record_per_page) / record_per_page)

        for idx in range(total_pages):
            from_id = min_id + record_per_page * idx
            to_id = min_id + record_per_page * (idx+1)

            sql_sel = self._select_sql
            if self._table_has_column_clinicid :
                sql_sel += ' and '
            else:
                sql_sel += ' WHERE '

            if to_id < max_id:
                sql_list.append('{0} {1}>={2} and {1}<{3} '.format(sql_sel, pk_column_name, from_id, to_id))
            else:
                sql_list.append('{0} {1}>={2} and {1}<={3} '.format(sql_sel, pk_column_name, from_id, max_id))
        return sql_list


    def export_insertsql_to_file(self,sql, file_idx):
        list = self._db_source.query_sql_to_list(sql)
        if len(list) == 0:
            self._logger.info(' {0} no data'.format(file_idx))
            return
        if file_idx > 1 and len(list) == 0:
            return
        file_idx_4num = '{:04}'.format(file_idx)
        '''create directory for each clinic'''
        if not os.path.exists(Global.export_dir):
            os.mkdir(Global.export_dir)
        clinic_dir = '{0}{1}/'.format(Global.export_dir, self._clinic_id)
        if not os.path.exists(clinic_dir):
            os.mkdir(clinic_dir)

        filepath = (Global.export_dir + '{0}/{1}.{2}').format(self._clinic_id, self._table_name, file_idx_4num)
        df = open(filepath, 'w+', encoding='utf-8')
        #if file_idx == 1: df.write('truncate table {0};\n'.format(self._table_name))
        row_idx = 0
        for row in list:
            row_value = ''
            row_idx += 1
            col_idx = 0
            for col in self._col_list:
                row_value += '{0},'.format(self.get_value_with_quot(col, row[col_idx]))
                col_idx += 1
            row_value = row_value[0:len(row_value)-1]
            #print(row_idx)
            #if row_idx > 1000:break
            df.write('{0} ( {1} ,{2});\n'.format(self._insert_sql, row_value, self._db_id))

            if row_idx > 0 and row_idx == int(row_idx / 100000)*100000:
                df.close()
                file_done = filepath + '.sql'
                os.rename(filepath, file_done)

                file_idx_4num = '{:04}'.format(int(row_idx / 100000))
                filepath = (Global.export_dir + '{0}/{1}.a{2}').format(self._clinic_id, self._table_name, file_idx_4num)
                df = open(filepath, 'w+', encoding='utf-8')

        df.close()

        file_done = filepath + '.sql'
        if os.path.exists(file_done):
            os.remove(file_done)
        os.rename(filepath, file_done)
        self._logger.info('    Batch {0} is DONE'.format(file_idx))

    def get_value_with_quot(self, col, col_value):
        return Utils.get_value_with_quot(col, col_value, self._db_id, self._id_increase)

    def log_info(self, txt):
        self._logger.info(txt)


class UpdateTable:
    def __init__(self, clinic_id):
        print('')

def test_export():
    clinic_id = 1004
    exp = ExportTable(clinic_id)
    #print(exp.replace_sql_str('\''))
    #webid = Global.get_webid_by_clinic(clinic_id)
    webid = 21
    #conn_str = Global.get_connection_str(clinic_id)
    conn_str = Global.db_conn_str1
    exp.set_source_db(webid, conn_str)
    #exp.export('DM_ActionGuideline')
    #exp.export('Clinic')
    exp.export('Medi_VitalSign')

def export_table_without_clinicid():
    exp = ExportTable(1004)
    exp.set_source_db(21, Global.db_conn_str1)

    db_central = DbUtil(Global.db_conn_central)
    table_list = db_central.get_tables_without_clinicid()
    for table in table_list:
        if Global.is_ignore_table(table.table_name):
            continue
        exp.export(table.table_name)


def export_tables_has_clinicid(clinic_id):

    exp = ExportTable(clinic_id)
    webid = Global.get_webid_by_clinic(clinic_id)
    conn_str = Global.get_connection_str(clinic_id)

    exp.set_source_db(webid, conn_str)

    db_central = DbUtil(Global.db_conn_central)
    table_list = db_central.get_tables_has_clinicid()

    work_idx = 0
    for table in table_list:
        work_idx += 1
        if Global.is_ignore_table(table.table_name):
            continue

        exp.log_info('Exporting Table ={0}/{1} ClinicID={2}, tableName={3}'.format(work_idx, len(table_list), clinic_id, table.table_name))
        try:
            exp.export(table.table_name)
        except Exception as e:
            Logger.get_error_logger().info('ClinicID={0}, Table={1}'.format(clinic_id, table.table_name))
            Logger.get_error_logger().info(str(e))

'''
patient,visitid, labid, billingid
1. delete
2. export
3. insert: start with delete commit immediately
'''
def export_tables_has_clinicid_without_other_key_columns(clinic_id):
    print('delete where by clinic id ,')

def export_super_group_1003():
    clinic_list = [1001,1196,2013,3171,3198,3229,3281,3332,1004,1061]
    for cli in clinic_list:
        print(Global.get_connection_str(cli))
        export_tables_has_clinicid(cli)

def export_truncate_table():
    db_central = DbUtil(Global.db_conn_central)
    table_list = db_central.get_tables()

    for tbl in table_list:
        print('TRUNCATE TABLE {0}'.format(tbl.table_name))

if __name__ == "__main__":
    test_export()
    #export_truncate_table()
    #export_table_without_clinicid()
    #export_tables_has_clinicid(1004)
    #export_super_group_1003()

#select ReceiverClinicID, SenderClinicID   from Outbox
