import psycopg2,re
from  common.settings import *
from common.utils import *

is_test = True

def get_identity_data():
    sql_str = " select t.table_name,t.column_name from com_clinic_identity t;"
    rows = excute_redshift(sql_str)
    return rows

def update_identity_data():
    update_sql_list = []
    rows = get_identity_data()
    top_5 = ""
    if is_test:
        top_5 = "top 5 "
    for item in rows:
        my_table = item[0]
        my_column = item[1]
        if my_column.lower() in ['pcpid' , 'visitid', 'patientid', 'coverbyid']:
            column_str = "{} {},".format(top_5, my_column)
            if check_table_has_column('webid', 'biz_hedispatient'):
                column_str += "webid,"
            if check_table_has_column('clinicid', 'biz_hedispatient'):
                column_str += "clinicid,"
            # 如果不存在 webid 或者 clinic id 则返回 无法获得webid
            if not ("webid," in column_str or "clinicid," in column_str):
                continue
            sql_str = " select {}  from {}".format(column_str[0:-1], my_table)
            temp_rows = excute_redshift(sql_str)
            for temp_data in temp_rows:

                cur_col_data = temp_data[0]
                cur_webid = temp_data[1] if "webid," in column_str else ''
                # 如果webid 为空 则通过clinicid 获取
                if not cur_webid:
                    cur_webid = Global.get_webid_by_clinic(temp_data[1])
                if str(cur_col_data) and str(cur_col_data)[1:3] != str(cur_webid):
                    # 构建 update_sql
                    update_sql = get_update_sql(my_table,cur_webid,my_column,cur_col_data)
                    update_sql_list.append(update_sql)
    print(update_sql_list)


def check_table_has_column(column_n, table_n):
    check_sql = "select count(1) from information_schema.columns c where c.table_name='{}' and c.column_name='{}'".format(table_n,column_n)
    res = excute_redshift(check_sql)
    if res[0][0] > 0:
        return True
    else:
        return False


def get_update_sql(table_name, webid, column_n,col_v):
    _id_increase = int(webid * 10e6)
    update_v =  col_v + _id_increase
    update_sql = "update {0} set {1} = {2} where {1}={3} ;\n".format(table_name,column_n, update_v,col_v )
    return update_sql

def excute_redshift(sql):
    conn = psycopg2.connect(database="ec_central", port=5439, user="awsuser",password="MDLand2018",
                            host="ec-dw-instance.crey7elebiyp.us-east-1.redshift.amazonaws.com")
    cur = conn.cursor()
    cur.execute(sql)
    rows = cur.fetchall()
    conn.commit()
    return rows


if __name__ == "__main__":
    update_identity_data()


