import pyodbc, os, datetime
from common.utils import *
from common.settings import *


class DailyExport:
    '''
    Pati_Daily_Tracking
    Billing,Inbox,OV,Patient
    '''

    def __init__(self):
        self._patient_id = 0
        self._clinic_id = ''
        self._db_central = DbUtil(Global.db_conn_central)
        self._db_source = DbUtil(Global.db_conn_str2)
        self._db_id = 81
        self._id_increase = 0
        self._insert_sql = ''
        self._select_sql = ''
        self._delete_sql = ''
        self._table_has_column_visitid = ''
        self._col_list = []
        self._table_name = ''
        self._patient_view = ''
        self._patient_view_list = []
        self._table_has_column_patientid = False
        self._table_has_column_clinicid = False

        self.table_columns_dict = {}
        self.today = (datetime.datetime.now() + datetime.timedelta(days=0)).strftime("%Y-%m-%d")
        self._logger = Logger(logger_name="ExportTable").get_logger()

    # general 正常类型 daily_patient: 特殊类型 需要特殊处理一下
    def export(self, table_name, type='general'):
        self._table_name = table_name
        """"""
        self._logger.info('\nClinicID={0}, Table={1}, Patient={2}'.format(self._clinic_id, self._table_name, self._patient_id))
        self.init_export_sql(type=type)
        self._logger.info(self._insert_sql)
        self._logger.info(self._select_sql)
        sql_list = [self._select_sql]
        # self._logger.info('The Table is Splited to {2} Batches to Export '.format(self._clinic_id, self._table_name, len(sql_list)))
        file_idx = 0
        for sql in sql_list:
            file_idx += 1
            self._logger.info('  NO. {0} '.format(file_idx) + sql)
            self.export_insertsql_to_file(sql, file_idx)

    def set_col_list(self):
        # 从 字典中获取 colo_list 不用每次都去查询
        if self._table_name not in self.table_columns_dict:
            self._col_list = self._db_central.get_columns(self._table_name)
            self.table_columns_dict[self._table_name] = self._col_list
        else:
            self._col_list = self.table_columns_dict[self._table_name]

    def init_export_sql(self, type):
        # 从 字典中获取 colo_list 不用每次都去查询
        self.set_col_list()
        if len(self._col_list) == 0:
            self._logger.info('{0} is not defined in center table'.format(self._table_name))
            return
        insert_sql = ''
        select_sql = ''

        id_stratify = IdentityStratify()

        for col in self._col_list:
            col_name = col.column_name
            insert_sql += '[{0}],'.format(col_name)

            select_sql += '{0},'.format(self.change_id_column(col, id_stratify))

        insert_sql = insert_sql[0: len(insert_sql) - 1]
        insert_sql = 'INSERT INTO {0} ({1}) VALUES '.format(self._table_name, insert_sql)
        delete_sql = 'delete from {0}'.format(self._table_name)
        self._insert_sql = insert_sql

        select_sql = select_sql[0: len(select_sql) - 1]
        select_sql = 'SELECT {0} FROM {1} '.format(select_sql, self._table_name)
        self._table_has_column_patientid = self._db_central.has_column_patient_id(self._col_list)
        if self._table_has_column_patientid:
            select_sql += ' WHERE PatientID={0} '.format(self._patient_id)
            delete_sql += ' WHERE PatientID={0} '.format(self._patient_id)
        # 增量更新的时候:判断是或否有visit_id 有就加入
        if self._clinic_id and self._db_central.has_column_clinic_id(self._col_list):
            select_sql = self.build_where_condition('ClinicID', self._clinic_id, select_sql)
            delete_sql = self.build_where_condition('ClinicID', self._clinic_id, delete_sql)
        # 增量更新的时候:判断是或否有visit_id 有就加入
        if self._visit_id and self._db_central.has_column_visit_id(self._col_list):
            select_sql = self.build_where_condition('VisitID', self._visit_id, select_sql)
            delete_sql = self.build_where_condition('VisitID', self._visit_id, delete_sql)
        # 增量更新的时候:判断是或否有billing_id 有就加入
        if self._billing_id and self._db_central.has_column_billing_id(self._col_list):
            select_sql = self.build_where_condition('BillingID', self._billing_id, select_sql)
            delete_sql = self.build_where_condition('BillingID', self._billing_id, delete_sql)
        # 增量更新的时候:判断是或否有lab_id 有就加入
        if self._lab_id and self._db_central.has_column_lab_id(self._col_list):
            select_sql = self.build_where_condition('LabID', self._lab_id, select_sql)
            delete_sql = self.build_where_condition('LabID', self._lab_id, delete_sql)

        # 增量更新的时候 时间需要加限制条件
        if type == 'daily_patient':
            select_sql = self.build_or_condition_by_collist(self._col_list, select_sql)
            delete_sql = self.build_or_condition_by_collist(self._col_list, delete_sql)

        self._select_sql = select_sql
        # 删除语句 必须有限制条件才能删除 防止误删
        if '=' in delete_sql:
            self._delete_sql = delete_sql

    def export_insertsql_to_file(self, sql, file_idx):
        list = self._db_source.query_sql_to_list(sql)
        if len(list) == 0:
            self._logger.info(' {0} no data'.format(file_idx))
            return
        if file_idx > 1 and len(list) == 0:
            return
        # file_idx_4num = '{:04}'.format(file_idx)
        '''create directory for each clinic'''
        if not os.path.exists(Global.export_dir):
            os.mkdir(Global.export_dir)
        clinic_dir = '{0}{1}/'.format(Global.export_dir, self._clinic_id)
        if not os.path.exists(clinic_dir):
            os.mkdir(clinic_dir)
        # filepath = (Global.export_dir + '{0}/{1}.{2}').format(self._clinic_id, self._table_name, file_idx_4num) + '.sql'
        df = self.get_df_by_idx(file_idx)
        df.write('{0};\n'.format(self._delete_sql))
        # if file_idx == 1: df.write('truncate table {0};\n'.format(self._table_name))
        row_idx = 0
        for row in list:
            row_value = ''
            row_idx += 1
            col_idx = 0
            for col in self._col_list:
                row_value += '{0},'.format(self.get_value_with_quot(col, row[col_idx]))
                col_idx += 1
            row_value = row_value[0:len(row_value) - 1]
            # print(row_idx)
            # if row_idx > 1000:break
            df.write('{0} ( {1} );\n'.format(self._insert_sql, row_value))
        df.close()

        # file_done = filepath + '.sql'
        # if os.path.exists(file_done):
        #     os.remove(file_done)
        # os.rename(filepath, file_done)
        self._logger.info('    Batch {0} is DONE'.format(file_idx))

    # 普通语句 拼接where 条件
    def build_where_condition(self, name, value, orgin_sql):
        # 如果查询语句中存在 = 或者 in ( 说明已经存在 where 查询条件了 此时连接符用 and
        if '=' in orgin_sql.lower() or ' in (' in orgin_sql.lower():
            key_con = 'and'
        else:
            key_con = 'where'
        if isinstance(value, int):
            orgin_sql += ' {0} {2}={1} '.format(key_con, value, name)
        elif isinstance(value, list):
            if len(value) > 1:
                orgin_sql += ' {0} {2} in {1} '.format(key_con, tuple(value), name)
            else:
                orgin_sql += ' {0} {2}={1} '.format(key_con, value[0], name)
        else:
            orgin_sql += ' {0} {2}={1} '.format(key_con, value, name)
        return orgin_sql

    # 批量操作 构建条件
    def build_where_conditon_batch(self, name, value, orgin_sql, is_sql_value=False):
        key_con = 'and'
        if orgin_sql[-2:] == 'on':
            key_con = ''
        if not is_sql_value:
            orgin_sql += ' {0} {2}={1} '.format(key_con, value, name)
        else:
            if ' where ' not in orgin_sql.lower():
                key_con = ' where '
            orgin_sql += ' {0} {2} in ({1}) '.format(key_con, value, name)
        return orgin_sql

    def build_or_condition_by_collist(self, col_list, orgin_sql):
        # 获取带有时间的字段
        date_col_list = self._db_central.get_col_list_has_date(col_list)
        if len(date_col_list) < 1:
            return
        # 拼接成 or 的SQL查询条件
        date_sql = ''
        for date_col in date_col_list:
            date_sql += ' {0} >= dateadd(day,-1,\'{1}\') or'.format(date_col, self.today)
        # 删除多余的or
        if len(date_sql) > 2:
            date_sql = date_col[0:-2]
        # 如果 = 在sql 中 说明已经有了where
        if '=' in orgin_sql and len(date_sql) > 2:
            orgin_sql += 'and ({0})'.format(date_sql)
        elif len(date_sql) > 5:
            orgin_sql += 'where ({0})'.format(date_sql)
        return orgin_sql

    def get_value_with_quot(self, col, col_value):
        return Utils.get_value_with_quot(col, col_value, self._db_id, self._id_increase)

    def log_info(self, txt):
        self._logger.info(txt)

    def change_id_column(self, col, id_stratify):
        col_name = col.column_name
        #
        # if col.type_name in ['uniqueidentifier']:
        #    return "concat('{1}',SUBSTRING(CONVERT(nvarchar(36), [{0}]),3,34 ))".format(col_name, self._db_id)

        # must be int
        if col.type_name not in ['int', 'bigint']:
            return '[{0}]'.format(col_name)

        is_id_field = id_stratify.is_identity_column(col)

        if is_id_field:
            sel_filed = ('[{0}]+{1}'.format(col_name, self._id_increase))
        else:
            sel_filed = '[{0}]'.format(col_name)
        return sel_filed

    # 总数大于5000 则分批运行
    def split_small_query(self):
        sql_list = []
        pk_row = self._db_source.get_primary_keys(self._table_name)
        if len(pk_row) != 1:
            sql_list.append(self._select_sql)
            return sql_list

        # print(pk_row)
        pk_column_name = pk_row[0].column_name
        # prepare to export data at most 100k records every time
        total_sql = 'SELECT COUNT(1), MAX({1}), MIN({1}) FROM {0}  '.format(self._table_name, pk_column_name)
        if self._table_has_column_patientid:
            total_sql += ' WHERE PatientID={0} '.format(self._patient_id)

        total_sql = self._db_source.query_sql_to_list(total_sql)
        total_record = total_sql[0][0]
        max_id = total_sql[0][1]
        min_id = total_sql[0][2]
        self._logger.info('{0} , total = {1} , min({4}) = {2} , max({4}) ={3}'.format(self._table_name, total_record, min_id, max_id, pk_column_name))

        if not isinstance(max_id, int):
            sql_list.append(self._select_sql)
            return sql_list

        record_per_page = 5000
        if total_record <= record_per_page:
            sql_list.append(self._select_sql)
            return sql_list

        # total_pages = int((total_record + (record_per_page -1)) / record_per_page)
        # page_step = int((max_id - min_id + 1) / total_record * record_per_page)
        total_pages = int((max_id - min_id + record_per_page) / record_per_page)

        for idx in range(total_pages):
            from_id = min_id + record_per_page * idx
            to_id = min_id + record_per_page * (idx + 1)

            sql_sel = self._select_sql
            if self._table_has_column_patientid:
                sql_sel += ' and '
            else:
                sql_sel += ' WHERE '

            if to_id < max_id:
                sql_list.append('{0} {1}>={2} and {1}<{3} '.format(sql_sel, pk_column_name, from_id, to_id))
            else:
                sql_list.append('{0} {1}>={2} and {1}<={3} '.format(sql_sel, pk_column_name, from_id, max_id))
        return sql_list

    # 增量更新时 根据类型获取 病人列表
    def get_patient_list(self, daily_type):
        db = DbUtil(Global.db_conn_str2)
        print('----------------Get Patient List----------------------------')
        sql = "select PatientID,ClinicID from Pati_Daily_Tracking where TrackingType='{1}' and TrackDate>=dateadd(day,-1,'{0}')".format(self.today, daily_type)
        print('SQL: ' + sql)
        cursor_list = db.query_sql_to_list(sql)
        print('Total:{0}'.format(len(cursor_list)))
        return cursor_list

    # 批量更新 1.创建临时视图 存储全部病人信息
    def create_daily_temp_view(self, daily_type):
        sql_create_view = ''
        if daily_type == 'OV':
            # 判断视图是否存在，如果存在则删除
            self._patient_view = 'View_Daily_Visit_Patient'
            self.sql_remove_temp_view(self._patient_view)
            sql_create_view = '''
            CREATE VIEW {1} as
            select  dt.PatientID, ov.VisitID, dt.ClinicID, ov.VisitDate, ov.CheckOutDate, cpt.ModifyDate,dt.TrackDate from Pati_Daily_Tracking dt 
            left join Medi_OfficeVisit ov on ov.PatientID = dt.PatientID and ov.ClinicID = dt.ClinicID 
            left join Medi_OV_CPTList cpt on ov.VisitID = cpt.VisitID
            where dt.TrackDate>=dateadd(day,-1,'{0}') 
            and (ov.VisitDate>=dateadd(day,-1,'{0}') or cpt.ModifyDate>=dateadd(day,-1,'{0}') or ov.CheckOutDate>=dateadd(day,-1,'{0}')) 
            '''.format(self.today, self._patient_view)
        self._db_source.get_cursor().execute(sql_create_view)
        self._db_source.get_cursor().commit()

    '''export table record has patient_id'''

    def sql_remove_temp_view(self, view_name):
        view_exists = '''if exists(select * from sys.views where name='{0}')
        drop view {0}
        '''.format(view_name)
        cursor = self._db_source.get_cursor()
        cursor.execute(view_exists)
        cursor.commit()

    def export_patient(self):
        print('export patient')
        cursor_list = self.get_patient_list(daily_type='Patient')
        db_central = DbUtil(Global.db_conn_central)
        # 获取 不含有 visitid labid billingid 的table
        table_list = db_central.get_tables_has_patientid_without_other_id()
        ignore_table_list = []
        i = 1
        for cursor in cursor_list:
            self.log_info('Patient:{} is working'.format(i))
            i += 1
            # 每次都重置
            self.clear_daily_temp_info()
            self._patient_id = cursor[0]
            self._clinic_id = cursor[1]
            if not self._patient_id:
                print('Clinic:{0}, Patient:{1} not Found'.format(self._clinic_id, self._patient_id))
                continue

            for table in table_list:
                if Global.is_ignore_table(table.table_name):
                    continue
                if table.table_name in ignore_table_list:
                    continue
                self.export(table.table_name, type='patient')

    '''export table has visit_id'''

    def export_visit(self):
        print('visit')
        cursor_list = self.get_patient_list(daily_type='OV')
        db_central = DbUtil(Global.db_conn_central)
        table_list = db_central.get_tables_has_visitid()
        i = 1
        for cursor in cursor_list:
            self.log_info('Patient:{} is working'.format(i))
            i += 1
            # 每次都重置
            self.clear_daily_temp_info()
            self._patient_id = cursor[0]
            self._clinic_id = cursor[1]
            self._visit_id = self.get_cantact_id(daily_type='OV')
            if not self._visit_id:
                self.log_info('Clinic:{0}, Patient:{1} not Found'.format(self._clinic_id, self._patient_id))
                continue
            for table in table_list:
                if Global.is_ignore_table(table.table_name):
                    continue
                self.export(table.table_name)

    '''批量操作  OV 批量操作'''

    def export_visit_batch(self):
        # 1 创建视图 存储全部增量病人信息
        self.create_daily_temp_view('OV')
        # 2.获取 带有 visitid 的表格 准备获取这些表对应的数据
        db_central = DbUtil(Global.db_conn_central)
        table_list = db_central.get_tables_has_visitid()
        for table in table_list:
            if Global.is_ignore_table(table.table_name):
                continue
            self.export_batch(table.table_name)

    # 批量操作 1.构造sql 语句
    def export_batch(self, table_name):
        self._table_name = table_name
        self.set_col_list()
        id_stratify = IdentityStratify()
        insert_sql = ''
        select_sql = ''
        for col in self._col_list:
            col_name = col.column_name
            insert_sql += '[{0}],'.format(col_name)
            select_sql += 'v.{0},'.format(self.change_id_column(col, id_stratify))

        insert_sql = insert_sql[0:-1]
        select_sql = select_sql[0:-1]
        insert_sql = 'INSERT INTO {0} ({1}) VALUES '.format(self._table_name, insert_sql)
        self._insert_sql = insert_sql
        self._delete_sql = 'delete from {0}'.format(self._table_name)
        # 拼接查询语句
        self._select_sql = 'SELECT {2} FROM {1} t inner join {0} v on'.format(table_name, self._patient_view, select_sql)
        # 构建 查询语句
        self.build_sql_str_batch(is_delete=False)
        # 构建 删除语句
        self.build_sql_str_batch(is_delete=True)
        if '=' not in self._delete_sql or ' where ' not in self._delete_sql.lower():
            self._delete_sql = ''
        self.insert_file_by_sql_batch(self._select_sql)

    # 批量操作内建函数：构建 sql 语句
    def build_sql_str_batch(self, is_delete=False):
        self._table_has_column_patientid = self._db_central.has_column_patient_id(self._col_list)
        self._table_has_column_visitid = self._db_central.has_column_visit_id(self._col_list)
        self._table_has_column_clinicid = self._db_central.has_column_clinic_id(self._col_list)

        if is_delete:
            if len(self._patient_view_list) < 1:
                select_str = ' select * from {}'.format(self._patient_view)
                self._patient_view_list = self._db_source.query_sql_to_list(select_str)
            if len(self._patient_view_list) == 0:
                self._logger.info(' no data')
                return
            view_col_list = self._db_source.get_columns(self._patient_view)
            my_sql = ''
            origin_sql = ' ' + self._delete_sql
            need_delete = 0
            for row in self._patient_view_list:
                col_idx = 0
                have_where = 0
                temp_sql = origin_sql + ' where '
                for col in view_col_list:
                    if (self._table_has_column_patientid and col.column_name.lower() == 'patientid') or (
                            self._table_has_column_visitid and col.column_name.lower() == 'visitid') \
                            or (self._table_has_column_clinicid and col.column_name.lower() == 'clinicid'):
                        temp_sql += '{1} = {0} and '.format(self.get_value_with_quot(col, row[col_idx]), col.column_name)
                        need_delete = 1
                        have_where = 1
                    col_idx += 1
                if ' and ' in temp_sql:
                    temp_sql = temp_sql[0:-5]
                temp_sql += ';\n'
                if temp_sql not in my_sql and have_where:
                    my_sql += temp_sql
            if ' where ' in my_sql and is_delete and need_delete:
                self._delete_sql = my_sql
            return
        if self._table_has_column_patientid:
            self._select_sql = self.build_where_conditon_batch('t.PatientID', 'v.PatientID', self._select_sql)
        if self._table_has_column_visitid:
            self._select_sql = self.build_where_conditon_batch('t.VisitID', 'v.VisitID', self._select_sql)
        if self._table_has_column_clinicid:
            self._select_sql = self.build_where_conditon_batch('t.ClinicID', 'v.ClinicID', self._select_sql)


    # 批量操作 2.查询数据 并插入数据
    def insert_file_by_sql_batch(self, sql):
        list = self._db_source.query_sql_to_list(sql)
        if len(list) == 0:
            self._logger.info(' no data')
            return
        if not os.path.exists(Global.export_dir):
            os.mkdir(Global.export_dir)
        table_dir = '{0}{1}/'.format(Global.export_dir, self._table_name)
        if not os.path.exists(table_dir):
            os.mkdir(table_dir)
        file_index = 1
        df = self.get_df_by_idx(file_index, is_batch=True)
        df.write('{0};\n'.format(self._delete_sql))
        # if file_idx == 1: df.write('truncate table {0};\n'.format(self._table_name))
        row_idx = 0
        for row in list:
            row_value = ''
            row_idx += 1
            col_idx = 0
            for col in self._col_list:
                row_value += '{0},'.format(self.get_value_with_quot(col, row[col_idx]))
                col_idx += 1
            row_value = row_value[0:len(row_value) - 1]
            # print(row_idx)
            # if row_idx > 1000:break
            df.write('{0} ( {1} );\n'.format(self._insert_sql, row_value))
            # 每两千条一批次
            if row_idx > 2000:
                row_idx = 0
                df.close()
                file_index += 1
                df = self.get_df_by_idx(file_index, is_batch=True)
        df.close()

    def get_df_by_idx(self, idx, is_batch=False):
        file_idx_4num = '000{}'.format(idx)
        if self._clinic_id:
            filepath = (Global.export_dir + '{0}/{1}.{2}').format(self._clinic_id, self._table_name, file_idx_4num) + '.sql'
        else:
            filepath = (Global.export_dir + '{0}/{0}.{1}').format(self._table_name, file_idx_4num) + '.sql'
        mode = 'a+' if not is_batch else 'w+'
        df = open(filepath, mode, encoding='utf-8')
        return df

    # 根据 daily_type 去对应的表获取对应的   daily_type_id 如： OV 就去 ov表 获取该病人的 visitid 并赋值
    def get_cantact_id(self, daily_type):
        contact_id = 0
        cursor_list = []
        if daily_type == 'OV':
            db = DbUtil(Global.db_conn_str2)

            sql = '''select top 100 ov.VisitID, ov.VisitDate, ov.CheckOutDate, cpt.ModifyDate from Medi_OfficeVisit ov left join Medi_OV_CPTList cpt on ov.VisitID = cpt.VisitID
                  where ov.PatientID={1}
                  and (ov.VisitDate>=dateadd(day,-1,'{0}') or cpt.ModifyDate>=dateadd(day,-1,'{0}') or ov.CheckOutDate>=dateadd(day,-1,'{0}')) 
                  order by ov.VisitDate desc'''.format(self.today, self._patient_id)
            cursor_list = db.query_sql_to_list(sql)
        elif daily_type == 'Billing':
            db = DbUtil(Global.db_conn_str2)
            sql = '''select  BillingID, ServiceDate, ClaimDate, ModifyDate from Billing_Account  
                 where PatientID={1}
                 and (ServiceDate>=dateadd(day,-1,'{0}') or ModifyDate>=dateadd(day,-1,'{0}') or ClaimDate>=dateadd(day,-1,
                 '{0}') or AcctCreateDate>=dateadd(day,-1,'{0}')) 
                 order by ServiceDate desc'''.format(self.today, self._patient_id)
            cursor_list = db.query_sql_to_list(sql)
        elif daily_type == 'Inbox':
            db = DbUtil(Global.db_conn_str2)
            sql = '''select  t_order.LabID,  t_order.LastModifyDate from Lab_Order t_order left join Lab_Result t_res on t_order.OrderID = t_res.OrderID  
                            where t_order.PatientID={1}
                            and (t_order.OrderDate>=dateadd(day,-1,'{0}') or t_order.ReportDate>=dateadd(day,-1,'{0}') 
                            or t_order.CheckInDate>=dateadd(day,-1, '{0}') or t_order.LastModifyDate>=dateadd(day,-1,'{0}')  
                            or t_res.ModifyDate>=dateadd(day,-1,'{0}') or  t_res.ResultDate>=dateadd(day,-1,'{0}') ) 
                            order by t_order.LastModifyDate desc'''.format(self.today, self._patient_id)

            cursor_list = db.query_sql_to_list(sql)
        if len(cursor_list) > 0:
            # 如果有多个 contact_id 添加到数组中
            print(cursor_list)
            if len(cursor_list) > 1:
                contact_id = []
                for cursor in cursor_list:
                    if cursor[0] not in contact_id:
                        contact_id.append(cursor[0])
            else:
                contact_id = cursor_list[0][0]

        return contact_id

    def export_lab(self):
        print('labid')
        cursor_list = self.get_patient_list(daily_type='Inbox')
        db_central = DbUtil(Global.db_conn_central)
        table_list = db_central.get_tables_has_labid()
        i = 1
        for cursor in cursor_list:
            self.log_info('Patient:{} is working'.format(i))
            i += 1
            # 每次都重置
            self.clear_daily_temp_info()
            self._patient_id = cursor[0]
            self._clinic_id = cursor[1]
            self._lab_id = self.get_cantact_id(daily_type='Inbox')
            if not self._lab_id:
                self.log_info('Clinic:{0}, Patient:{1} not Found'.format(self._clinic_id, self._patient_id))
                continue
            for table in table_list:
                if Global.is_ignore_table(table.table_name):
                    continue
                self.export(table.table_name)

    def export_billing(self):
        print('billing id')
        cursor_list = self.get_patient_list(daily_type='Billing')
        db_central = DbUtil(Global.db_conn_central)
        table_list = db_central.get_tables_has_billingid()
        i = 1
        for cursor in cursor_list:
            self.log_info('Patient:{} is working'.format(i))
            i += 1
            # 每次都重置
            self.clear_daily_temp_info()
            self._patient_id = cursor[0]
            self._clinic_id = cursor[1]
            self._billing_id = self.get_cantact_id(daily_type='Billing')
            if not self._billing_id:
                self.log_info('Clinic:{0}, Patient:{1} not Found'.format(self._clinic_id, self._patient_id))
                continue
            for table in table_list:
                if Global.is_ignore_table(table.table_name):
                    continue
                self.export(table.table_name)

    # 清除所有的临时信息
    def clear_daily_temp_info(self):
        self._delete_sql = ''
        self._insert_sql = ''
        self._delete_sql = ''
        self._visit_id = ''
        self._billing_id = ''
        self._lab_id = ''

    def test_sql(self):
        sql = " SELECT [ItemCheckID],[ClinicID],[PatientID]+0,[ItemCode],[ItemName],[ItemCodeSystem],[SubCode],[SubCodeSystem],[SubCodeName],[ItemStatus],[ItemDate],[ItemOther],[CalcYear],[CalcMonth],[ModifyBy],[ModifyDate] FROM Medi_QMCheckItemList  WHERE PatientID=1001486008  and ClinicID=1007 "
        list = self._db_source.query_sql_to_list(sql)
        print('Find Count:{}'.format(len(list)))
        self._col_list = DbUtil(Global.db_conn_central).get_columns('Medi_QMCheckItemList')
        filepath = (r'D:/Workspace/WS-iClinic/DbMerge/test.sql').format()
        df = open(filepath, 'w+', encoding='utf-8')
        row_idx = 0
        for row in list:
            row_value = ''
            row_idx += 1
            col_idx = 0
            for col in self._col_list:
                row_value += '{0},'.format(self.get_value_with_quot(col, row[col_idx]))
                col_idx += 1
            row_value = row_value[0:len(row_value) - 1]
            df.write('{0} ( {1} );\n'.format(self._insert_sql, row_value))
        df.close()


if __name__ == "__main__":
    DailyExport().export_visit_batch()
    # DailyExport().test_sql()
