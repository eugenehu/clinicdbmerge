import psycopg2,re
from  common.settings import *
from common.utils import *

def get_main_data_by_clinic(clinicId):
    webid = Global.get_webid_by_clinic(clinicId)
    conn_str = Global.get_connection_str(clinicId)
    db_central = DbUtil(conn_str)
    main_table_list = ['View_mongo_cptlist','View_mongo_icdlist','View_mongo_ov','View_mongo_patient_redshift','View_mongo_VitalSign']
    new_total = []
    for table_name in main_table_list:
        sqlserver_total = get_count_table_sql(table_name,db_central, clinicId)
        redshidt_total = get_count_table_refshift(table_name,clinicId)
        print('--------------{}/{}'.format(table_name,clinicId))

        if sqlserver_total != redshidt_total:
            print('-------SQL:{}, RedShift:{}'.format(sqlserver_total, redshidt_total))
        else:
            print('-------same')
        new_total.append([clinicId,table_name,sqlserver_total,redshidt_total])
    return new_total

def get_count_table_sql(table_name,db_central, clinicId):
    sql = "select count(1) from {0} where clinicid={1}".format(table_name,clinicId)
    return db_central.query_sql_to_int(sql)

def get_count_table_refshift(table_name,clinicId):
    if 'view_mongo' in table_name.lower():
        table_name = re.sub(r'view_mongo', 'ec', table_name, flags=re.IGNORECASE)
    sql = "select count(1) from {0} where clinicid={1}".format(table_name, clinicId)
    res = excute_redshift(sql)
    return res[0]

def excute_redshift(sql):
    conn = psycopg2.connect(database="ec_central", port=5439, user="awsuser",password="MDLand2018",
                            host="ec-dw-instance.crey7elebiyp.us-east-1.redshift.amazonaws.com")
    cur = conn.cursor()
    cur.execute(sql)
    rows = cur.fetchone()
    conn.commit()
    return rows


if __name__ == '__main__':
    clinic_ids = [1001, 1196, 2013, 3171, 3198, 3229, 3281, 3332, 1004, 1061]
    new_total = []
    for cli in clinic_ids:
        new_total += get_main_data_by_clinic(cli)
    total_item = {}
    for total in new_total:
        if total[1] not in total_item:
            total_item[total[1]] = {'sql': total[2], 'redshift': total[3]}
        else:
            total_item[total[1]]['sql'] += total[2]
            total_item[total[1]]['redshift'] += total[3]
    print(total_item)