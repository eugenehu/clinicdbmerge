import pyodbc, os, csv, re, time
from common.utils import *
from common.settings import *
from multiprocessing.pool import Pool
from bson import ObjectId

special_views = ['view_redshift_patient', 'view_redshift_ov', 'view_redshift_appointment', 'view_redshift_billing1500form', 'view_redshift_lab',
                 'view_redshift_labinbox', 'view_redshift_labinbox_single', 'view_redshift_taginbox']
orgin_views = [x.replace('redshift', 'mongo') for x in special_views]


class RedshiftExportTable:

    def __init__(self):
        self._db_central = ''
        self._db_source = None
        self._db_id = -1
        self._id_increase = 0
        self._insert_sql = ''
        self._select_sql = ''
        self._copy_sql = ''
        self._col_list = []
        self._conn_str = ''
        self.record_per_page = 10000
        self._table_name = ''
        self._is_mode_test = False
        self._table_has_column_clinicid = False

    def init_pool_arg(self, clinic_id, table_name, webid, conn_str):
        self._clinic_id = clinic_id
        # 多进程 无法解析 logger 模块
        # self._logger = Logger(logger_name="ExportTable").get_logger()

        self.set_source_db(webid, conn_str)
        descript_txt = self.export(table_name)
        return descript_txt

    def set_source_db(self, db_id, conn_str):
        self._db_source = DbUtil(conn_str)
        self._db_id = db_id
        self._conn_str = conn_str
        self._id_increase = int(db_id * 10e6)

    def check_view(self):
        path = '../data/{}'.format(self._table_name)
        sql_str = read_sql_file(path)
        self._db_source.get_cursor().execute(sql_str)
        self._db_source.get_cursor().commit()

    def export(self, table_name):
        self._table_name = table_name
        # 检查是否存在
        if self._table_name.lower() in special_views:
            self.check_view()
        """"""
        self.log_info('\nClinicID={0}, Table={1}'.format(self._clinic_id, self._table_name))
        self.init_export_sql()
        self.log_info(self._insert_sql)
        self.log_info(self._select_sql)
        sql_list = self.split_small_query_row_number()
        # sql_list = [self._select_sql]
        self.log_info('The Table is Splited to {2} Batches to Export '.format(self._clinic_id, self._table_name, len(sql_list)))
        descritpt_txt = []
        file_index = 0
        for sql in sql_list:
            file_index += 1
            temp_descript = self.export_insertsql_to_file(sql, file_index)
            descritpt_txt.append(temp_descript)

        # update_table = UpdateTable(sql_list, self._clinic_id, self._table_name, self._col_list, self._db_id, self._conn_str, self._copy_sql, self._db_source)
        # descript_txt = update_table.do_work()

        return descritpt_txt

    def export_insertsql_to_file(self, sql, file_idx):
        # 线程安全等级为1 只能共享模块 不能 共享连接 所以在线程内部只能单独获取连接
        # db_source = DbUtil(self._conn_str)
        list = self._db_source.query_sql_to_list(sql)
        if len(list) == 0:
            self.log_info(' {0} no data'.format(file_idx))
            return
        if file_idx > 1 and len(list) == 0:
            return
        file_idx_4num = '{:04}'.format(file_idx)
        '''create directory for each clinic'''
        if not os.path.exists(Global.redshift_dir):
            os.mkdir(Global.redshift_dir)
        clinic_dir = '{0}{1}/'.format(Global.redshift_dir, self._clinic_id)
        if not os.path.exists(clinic_dir):
            os.mkdir(clinic_dir)
        # create description file
        # 要上传的文件名 用来写入descri_file
        upload_file_name = '{2}/{0}.{1}.csv.gz'.format(self._table_name, file_idx_4num, self._clinic_id)
        filepath = (Global.redshift_dir + '{0}/{1}.{2}.working.csv').format(self._clinic_id, self._table_name, file_idx_4num)
        df = open(filepath, 'w+', encoding='utf-8', newline='')
        writer = csv.writer(df, dialect='excel', delimiter='^')
        descri_txt = self.get_descri_by_table_name(upload_file_name)
        # if file_idx == 1: df.write('truncate table {0};\n'.format(self._table_name))
        row_idx = 0
        have_webid = 0
        for row in list:
            row_idx += 1
            col_idx = 0
            patientid_index = 0
            value_list = []
            for col in self._col_list:
                if col.column_name.lower() == 'webid':
                    have_webid = 1
                if col.column_name.lower() == 'patientid':  # 有的数据 需要保留原始的patientid 记录下位置方便取
                    patientid_index = col_idx
                try:
                    value_list.append(self.get_value_with_quot(col, row[col_idx]))
                except Exception as e:
                    print(row)
                    print('detail--------{}---{}__{}'.format(col.column_name, str(e), self._table_name))
                col_idx += 1

            if len(value_list) > 0:
                if self._table_name == "ClinicMPI":
                    orgin_patientid = row[patientid_index] - self._id_increase
                    value_list.append(orgin_patientid)
                if not have_webid:
                    value_list.append(self._db_id)
                writer.writerow(value_list)
            if row_idx > 0 and row_idx == int(row_idx / self.record_per_page) * self.record_per_page:
                df.close()
                print('running...{}............................'.format(file_idx))
                file_done = filepath[0:-12] + '.csv'
                os.rename(filepath, file_done)
                file_idx_4num = '{:04}_{}'.format(file_idx, int(row_idx / self.record_per_page) + 1)
                upload_file_name = '{0}.{1}.csv.gz'.format(self._table_name, file_idx_4num)
                # descri_stream.write(self.get_descri_by_table_name(upload_file_name))
                descri_txt += self.get_descri_by_table_name('{}/'.format(self._clinic_id) + upload_file_name)
                filepath = (Global.redshift_dir + '{0}/{1}.{2}.working.csv').format(self._clinic_id, self._table_name, file_idx_4num)
                df = open(filepath, 'w+', encoding='utf-8', newline='')
                writer = csv.writer(df, dialect='excel', delimiter='^')

        df.close()
        # descri_stream.close()
        file_done = filepath[0:-12] + '.csv'
        if os.path.exists(file_done):
            os.remove(file_done)
        os.rename(filepath, file_done)
        self.log_info('    Batch {0} is DONE'.format(self._table_name))
        return (clinic_dir, descri_txt)

    def init_export_sql(self):
        # self._col_list = self._db_central.get_columns(self._table_name)
        self._col_list = self._db_source.get_columns(self._table_name)

        if len(self._col_list) == 0:
            self.log_info('{0} is not defined in center table'.format(self._table_name))
            return
        insert_sql = ''
        select_sql = ''
        copy_sql = ''
        id_stratify = IdentityStratify()
        have_webid = 0
        for col in self._col_list:
            col_name = col.column_name
            insert_sql += '"{0}",'.format(col_name)
            if col_name.lower() == 'webid':
                have_webid = 1
            copy_sql += '"{0}",'.format(col_name)
            # select_sql += '{0},'.format(col_name)
            select_sql += '{0},'.format(self.change_id_column(col, id_stratify))

        insert_sql = insert_sql[0: len(insert_sql) - 1]
        insert_sql = 'INSERT INTO {0} ({1}{2}) VALUES '.format(self._table_name, insert_sql, ', "WebID"' if not have_webid else '')
        self._insert_sql = insert_sql
        # ClinicMPI 需要加入一些特殊字段
        if self._table_name == 'ClinicMPI':
            copy_sql += 'clinicpatientid,'
        # MPatient不需要webid字段
        if self._table_name == 'MPatient':
            have_webid = 1
        copy_sql = ' copy {0} ({1}{2}) from '.format(self._table_name, copy_sql[0:-1], ', "WebID"' if not have_webid else '')
        self._copy_sql = copy_sql
        select_sql = select_sql[0: len(select_sql) - 1]
        select_sql = 'SELECT {2} {0} FROM {1} '.format(select_sql, self._table_name, 'top 10' if self._is_mode_test else '')

        self._table_has_column_clinicid = self._db_source.has_column_clinic_id(self._col_list)
        # ClinicMPI 不需要加入这个条件 因为是聚合数据
        if self._table_has_column_clinicid and self._table_name != "ClinicMPI":
            select_sql += ' WHERE ClinicID={0} '.format(self._clinic_id)
        if self._table_name == "MPatient":
            select_sql += ' WHERE mpatientid={0} '.format('2015463864')

        self._select_sql = select_sql

    def change_id_column(self, col, id_stratify):
        col_name = col.column_name
        # if True:
        #     return '[{0}]'.format(col_name)

        # must be int
        if col.type_name not in ['int', 'bigint']:
            return '[{0}]'.format(col_name)

        is_id_field = id_stratify.is_identity_column(col)
        if col_name.lower() in ['pcpid', 'visitid', 'coverbyid']:
            is_id_field = True
        if is_id_field:
            sel_filed = ('[{0}]+{1}'.format(col_name, self._id_increase))
        else:
            sel_filed = '[{0}]'.format(col_name)
        return sel_filed

    def split_small_query(self):
        sql_list = []
        pk_row = self._db_source.get_primary_keys(self._table_name)
        if len(pk_row) != 1:
            sql_list.append(self._select_sql)
            return sql_list

        # print(pk_row)
        pk_column_name = pk_row[0].column_name
        # prepare to export data at most 100k records every time
        total_sql = 'SELECT COUNT(1), MAX({1}), MIN({1}) FROM {0}  '.format(self._table_name, pk_column_name)
        if self._table_has_column_clinicid:
            total_sql += ' WHERE ClinicID={0} '.format(self._clinic_id)

        total_sql = self._db_source.query_sql_to_list(total_sql)
        total_record = total_sql[0][0]
        max_id = total_sql[0][1]
        min_id = total_sql[0][2]
        self.log_info('{0} , total = {1} , min({4}) = {2} , max({4}) ={3}'.format(self._table_name, total_record, min_id, max_id, pk_column_name))

        if not isinstance(max_id, int):
            sql_list.append(self._select_sql)
            return sql_list

        record_per_page = 5000
        if total_record <= record_per_page:
            sql_list.append(self._select_sql)
            return sql_list

        # total_pages = int((total_record + (record_per_page -1)) / record_per_page)
        # page_step = int((max_id - min_id + 1) / total_record * record_per_page)
        total_pages = int((max_id - min_id + record_per_page) / record_per_page)

        for idx in range(total_pages):
            from_id = min_id + record_per_page * idx
            to_id = min_id + record_per_page * (idx + 1)

            sql_sel = self._select_sql
            if self._table_has_column_clinicid:
                sql_sel += ' and '
            else:
                sql_sel += ' WHERE '

            if to_id < max_id:
                sql_list.append('{0} {1}>={2} and {1}<{3} '.format(sql_sel, pk_column_name, from_id, to_id))
            else:
                sql_list.append('{0} {1}>={2} and {1}<={3} '.format(sql_sel, pk_column_name, from_id, max_id))
        return sql_list

    def split_small_query_row_number(self):
        sql_list = []
        pk_row = self._db_source.get_primary_keys(self._table_name)
        if len(pk_row) < 1:
            sql_list.append(self._select_sql)
            return sql_list

        # print(pk_row)
        pk_column_name = pk_row[0].column_name
        # prepare to export data at most 100k records every time
        total_sql = 'SELECT COUNT(1), MAX({1}), MIN({1}) FROM {0}  '.format(self._table_name, pk_column_name)
        if self._table_has_column_clinicid:
            total_sql += ' WHERE ClinicID={0} '.format(self._clinic_id)

        total_sql = self._db_source.query_sql_to_list(total_sql)
        total_record = total_sql[0][0]
        max_id = total_sql[0][1]
        min_id = total_sql[0][2]
        self.log_info('{0} , total = {1} , min({4}) = {2} , max({4}) ={3}'.format(self._table_name, total_record, min_id, max_id, pk_column_name))

        # if not isinstance(max_id, int):
        #     sql_list.append(self._select_sql)
        #     return sql_list

        if total_record <= self.record_per_page:
            sql_list.append(self._select_sql)
            return sql_list

        # total_pages = int((total_record + (record_per_page -1)) / record_per_page)
        # page_step = int((max_id - min_id + 1) / total_record * record_per_page)
        (count, mod) = divmod(total_record, self.record_per_page)
        total_pages = count + 1 if mod else count
        # 获取列 因为下面要用分页函数 需要指定列名
        select_column_sql = self._select_sql.split(' FROM ')[0]
        where_str = ''
        if self._table_has_column_clinicid:
            where_str += ' WHERE ClinicID={0} '.format(self._clinic_id)
        row_number_sql = ''' From (SELECT  * , ROW_NUMBER() OVER (ORDER BY {0}) AS RowNumber FROM {1} {2} ) temp_table '''.format(pk_column_name,
                                                                                                                                  self._table_name,
                                                                                                                                  where_str)
        select_page_sql = select_column_sql + ' ' + row_number_sql
        for idx in range(total_pages):
            sql_sel = select_page_sql
            sql_list.append('{0} Where RowNumber>={1} and RowNumber<={2} '.format(sql_sel, idx * self.record_per_page, (idx + 1) * self.record_per_page))

        return sql_list

    def get_descri_by_table_name(self, file_name):
        # 替换前缀
        if 'redshift' in self._table_name.lower():
            self._copy_sql = re.sub(r'view_redshift', 'ec', self._copy_sql, flags=re.IGNORECASE)
        if 'view_mongo' in self._copy_sql.lower():
            self._copy_sql = re.sub(r'view_mongo', 'ec', self._copy_sql, flags=re.IGNORECASE)
        if 'clinicmpi' in self._copy_sql.lower() and self._table_name == 'ClinicMPI':
            self._copy_sql = re.sub(r'clinicmpi', 'com_clinicmpi', self._copy_sql, flags=re.IGNORECASE)
        if 'mpatient' in self._copy_sql.lower() and self._table_name == 'MPatient':
            self._copy_sql = re.sub(r'mpatient', 'com_mpatient', self._copy_sql, flags=re.IGNORECASE)

        str_copy = '''{0} 's3://dws3-eugene-sftp/export/{1}'
        credentials 'aws_access_key_id=AKIAJRIFKFAO2ABSNV7A;aws_secret_access_key=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'
        CSV ACCEPTINVCHARS GZIP DELIMITER as '^' NULL AS 'null' REGION AS 'us-east-1';\n'''.format(self._copy_sql, file_name)
        return str_copy

    def get_value_with_quot(self, col, col_value):
        temo_val = Utils.get_value_with_quot(col, col_value, self._db_id, self._id_increase, is_csv=True)
        return temo_val

    def log_info(self, txt):
        Logger(logger_name="ExportTable").get_logger().info(txt)


def read_sql_file(path):
    main_content = []
    sql_str = ''
    with open(path, 'r', encoding='utf8') as file:
        main_content = file.readlines()
    if len(main_content) > 0:
        sql_str = ' '.join(main_content)
    print(sql_str)
    return sql_str


def export_master_db():
    table_list = ['MPatient']
    export_tables_has_clinicid(clinic_id='MasterDB', is_view=False, table_list=table_list)


def get_zw():
    from pymongo import MongoClient
    client = MongoClient('mongodb://192.168.168.173:27017/')
    return client.zw


def export_mongo_patient_by_clinicid(clinicid):
    db_id = Global.get_webid_by_clinic(clinicid)
    _id_increase = int(db_id * 10e6)
    zw = get_zw()
    pat_rows = zw.phm_HedisPatient.aggregate(
        [{'$match': {'HedisScoreId': ObjectId('5adfe9b79d10da4bfc3e3582'), 'PcpClinicID': clinicid}},
         {'$project': {'_id': 0, 'HedisScoreId': 0, 'Appointment': 0,
                       'Race': 0}},
         {'$unwind': '$Measures'}, {'$project': {
            "mso_pid": 1, "MPID": 1, "PatientUID": 1, "FirstName": 1, "LastName": 1, "DOB": 1, "Gender": 1, "PcpID": 1, "ClinicID": '$PcpClinicID',
            "PcpName": 1, "PlanType": 1, "Period": 1, "Measure": "$Measures.Measure", "Denominator": "$Measures.Denominator",
            "Numerator": "$Measures.Numerator", "Exclusion": "$Measures.Exclusion", "UpdateTime": 1, "DOS": "$LastEncounter.DOS",
            "OfficeLocationID": "$LastEncounter.OfficeLocationID", "OfficeLocationName": "$LastEncounter.OfficeLocationName"
        }}])
    total_row = zw.phm_HedisPatient.aggregate(
        [{'$match': {'HedisScoreId': ObjectId('5adfe9b79d10da4bfc3e3582'), 'PcpClinicID': clinicid}},
         {'$project': {'_id': 0, 'HedisScoreId': 0, 'Appointment': 0,
                       'Race': 0}},
         {'$unwind': '$Measures'}, {'$project': {"_id": 1}}, {'$group': {'_id': '', 'count': {'$sum': 1}}}])
    total = list(total_row)[0]['count']
    # insert_sql = ''
    i = 0
    # 实际的列名
    clolumn_list = ["mso_pid", "MPID", "PatientUID", "FirstName", "LastName", "DOB", "Gender", "PcpID", "ClinicID",
                    "PcpName", "PlanType", "Period", "Measure", "Denominator",
                    "Numerator", "Exclusion", "UpdateTime", "DOS",
                    "OfficeLocationID", "OfficeLocationName"]
    # 上传之后 要修改的列名 对应redshift 里面的列名
    copy_sql_column = ["msopid", "MPID", "PatientID", "FirstName", "LastName", "DOB", "Gender", "PcpID", "ClinicID",
                       "PcpName", "PlanType", "Period", "Measure", "Denominator",
                       "Numerator", "Exclusion", "UpdateTime", "DOS",
                       "OfficeLocationID", "OfficeLocationName"]
    # 要保存的路径 以及上传之后的路径（用来写入copy .txt）

    filepath = '{0}HedisPatient/hedis_patient_{1}.csv'.format(Global.redshift_dir, clinicid)
    if not os.path.exists(Global.redshift_dir + 'HedisPatient'):
        os.mkdir(Global.redshift_dir + 'HedisPatient')
    upload_file_name = 'HedisPatient/hedis_patient_{0}.csv.gz'.format(clinicid)
    str_copy = '''copy com_hedisPatient {0} 's3://dws3-eugene-sftp/export/{1}'
           credentials 'aws_access_key_id=AKIAJRIFKFAO2ABSNV7A;aws_secret_access_key=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'
           CSV ACCEPTINVCHARS GZIP DELIMITER as '^' NULL AS 'null' REGION AS 'us-east-1';\n'''.format(tuple(copy_sql_column), upload_file_name)

    df = open(filepath, 'w+', encoding='utf-8', newline='')
    writer = csv.writer(df, dialect='excel', delimiter='^')
    for row in pat_rows:
        i += 1
        if i % 100 == 0:
            print('------------{0}/{1}----------------'.format(i, total))
        # row_sql = ''
        # temp_key_sql = ' insert into com_hedisPatient ('
        # tem_value_sql = ''
        value_list = []
        for key in clolumn_list:
            value = row[key] if key in row else ''
            if key == "PatientUID":
                key = "PatientID"
                value = value.split('-')[0] if '-' in value else value
                if value != "0":
                    value = str(int(value) + _id_increase)
            if isinstance(value, list) and len(value) > 0 and key != "Measures":
                value = ','.join(value)
            elif isinstance(value, list) and len(value) == 0 and key != "Measures":
                value = ""
            if isinstance(value, int):
                value = int(value)
            elif isinstance(value, str):
                value = value.replace('^', '')
            value_list.append(value)
        writer.writerow(value_list)
    df.close()
    # 写入描述文件
    write_file([(Global.redshift_dir + 'HedisPatient', str_copy)])
    # for key in row.keys():
    #
    #     value = row[key]
    #     if key == "PatientUID":
    #         key = "PatientID"
    #         value = value.split('-')[0] if '-' in value else value
    #         if value != "0":
    #             value = str(int(value) + _id_increase)
    #     if key == "mso_pid":
    #         key = "msopid"
    #     temp_key_sql += "\"" + key + '",'
    #     if isinstance(value, list) and len(value) > 0 and key != "Measures":
    #         value = ','.join(value)
    #     elif isinstance(value, list) and len(value) == 0 and key != "Measures":
    #         value = ""
    #     if isinstance(value, int):
    #         value = int(value)
    #     tem_value_sql += "'{}',".format(value)
    # tem_value_sql = tem_value_sql[0:-1]
    # temp_key_sql = temp_key_sql[0:-1] + ' ) values('
    # row_sql += temp_key_sql + tem_value_sql + '); \n'
    # insert_sql += row_sql

    # with open(path,'w', encoding='utf8') as file:
    #     file.write(insert_sql)


def test_export():
    clinic_ids = [1001, 1196, 2013, 3171, 3198, 3229, 3281, 3332, 1004, 1061]
    clinic_ids = [1196, 2013, 1061]
    table_list = ['View_mongo_Form_Screen', 'View_mongo_PTNote', 'View_mongo_assessmentplan', 'View_mongo_PEDetail', 'View_mongo_OtherAssessmentPlan']
    table_list = ['View_mongo_icdlist']
    for clinic_id in clinic_ids:
        export_tables_has_clinicid(clinic_id=clinic_id, is_view=True, table_list=table_list)


def export_table_without_clinicid():
    exp = RedshiftExportTable(1004)
    exp.set_source_db(21, Global.db_conn_str1)

    db_central = DbUtil(Global.db_conn_central)
    table_list = db_central.get_tables_without_clinicid()
    for table in table_list:
        if Global.is_ignore_table(table.table_name):
            continue
        exp.export(table.table_name)


def export_tables_has_clinicid(clinic_id, is_view=False, table_list=[]):
    # exp = RedshiftExportTable(clinic_id)
    if clinic_id == "MasterDB":
        webid = 66
        conn_str = Global.db_conn_masterdb
    else:
        webid = Global.get_webid_by_clinic(clinic_id)
        conn_str = Global.get_connection_str(clinic_id)
    print(conn_str)
    # exp.set_source_db(webid, conn_str)

    db_central = DbUtil(Global.db_conn_central)
    db_source = DbUtil(conn_str)
    if len(table_list) < 1:
        db_table_list = db_central.get_tables_has_clinicid() if not is_view else db_source.get_view_has_clinicid()
        print(db_table_list)
    else:
        db_table_list = table_list
        print(db_table_list)
    work_idx = 0
    pool = Pool(30)
    check_table_list = []
    for table in db_table_list:
        work_idx += 1
        if len(table_list) < 1:
            table_name = table.table_name
        else:
            table_name = table
        if Global.is_ignore_table(table_name):
            continue
        if table_name.lower() in orgin_views:
            table_name = table_name.replace('_mongo_', '_redshift_')
        # 如果已经处理了 就不用在处理一次了
        if table_name not in check_table_list:
            check_table_list.append(table_name)
        else:
            continue

        exp = RedshiftExportTable()
        exp.log_info('Exporting Table ={0}/{1} ClinicID={2}, tableName={3}'.format(work_idx, len(table_list), clinic_id, table_name))
        try:
            res = pool.apply_async(func=exp.init_pool_arg, args=(clinic_id, table_name, webid, conn_str), callback=write_file)
        except Exception as e:
            Logger.get_error_logger().info('ClinicID={0}, Table={1}'.format(clinic_id, table_name))
            Logger.get_error_logger().info(str(e))
    pool.close()
    pool.join()


def write_file(txt_list):
    for txt_item in txt_list:
        if not txt_item:
            continue
        clinic_dr = txt_item[0]
        with open(clinic_dr + '\descript.txt', 'a+', encoding='utf-8') as file:
            file.write(txt_item[1])


def import_clinic_list(path="../common/Clinic-List.xlsx"):
    import xlrd
    workbook = xlrd.open_workbook(path)
    table = workbook.sheets()[0]
    column_str = ''
    sql_str_list = ''
    for i in range(table.nrows):
        sql_str = ""
        value = table.row_values(i)
        if i == 0:
            column_str += 'insert into com_Clinic ('
            for item in value:
                column_str += '"' + item + '",'
            column_str += 'WebID '
            column_str = column_str + ") values "
        else:
            new_value = []
            for j, v in enumerate(value):
                if j == 6 and not v:
                    v = 0
                new_value.append(v)
            webid = value[-1] if not value[-1] or 'Web' not in value[-1] else value[-1][3:]
            new_value.append(webid)
            sql_str = column_str + str(tuple(new_value))
            sql_str_list += sql_str + " ;\n"
    with open('../common/clinic_list.txt', 'w', encoding='utf-8') as file:
        file.write(sql_str_list)


'''
patient,visitid, labid, billingid
1. delete
2. export
3. insert: start with delete commit immediately
'''


def export_tables_has_clinicid_without_other_key_columns(clinic_id):
    print('delete where by clinic id ,')


def export_super_group_1003(is_view=False):
    clinic_list = [1001, 1196, 2013, 3171, 3198, 3229, 3281, 3332, 1004, 1061]
    for cli in clinic_list:
        # print(Global.get_connection_str(cli))
        export_tables_has_clinicid(cli, is_view=is_view)


def export_truncate_table():
    db_central = DbUtil(Global.db_conn_central)
    table_list = db_central.get_tables()

    for tbl in table_list:
        print('TRUNCATE TABLE {0}'.format(tbl.table_name))


def export_mongo_patient_data():
    clinic_list = [1001, 1196, 2013, 3171, 3198, 3229, 3281, 3332, 1004, 1061]
    for cli in clinic_list:
        export_mongo_patient_by_clinicid(cli)


if __name__ == "__main__":
    # export_master_db()
    # test_export()
    # export_truncate_table()
    # export_table_without_clinicid()
    # export_tables_has_clinicid(1004, is_view=True)
    export_super_group_1003(is_view=True)
    # import_clinic_list()
    # export_mongo_patient_data()
    pass
