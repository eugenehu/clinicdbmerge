import os
from common.settings import *
from common.utils import *
import time

class ImportSqlFile(object):
    logger = Logger(logger_name="ImportFile").get_logger()

    def __init__(self, db):
        self._db = db

    def process(self, dirpath):
        dirs = os.listdir(dirpath)
        for clinic_dir in dirs:
            if len(clinic_dir) != 4:
                continue

            sql_file_list = os.listdir(dirpath + clinic_dir)
            ImportSqlFile.logger.info('ImportSqlFile: ' + dirpath + clinic_dir)

            for file in sql_file_list:
                if file.endswith('.sql'):
                    old_file_name = dirpath + clinic_dir + '/' + file
                    new_file_name = file #+ '.working.sql'

                    if not os.path.exists(Global.import_working):
                        os.mkdir(Global.import_working)
                    if not os.path.exists(Global.import_working + clinic_dir):
                        os.mkdir(Global.import_working + clinic_dir)

                    os.rename(old_file_name, Global.import_working + clinic_dir + '/' + new_file_name)
                    self.import_one_file_batch_commit(clinic_dir, new_file_name)

    def import_one_file_batch_commit(self,clinic_dir, filename):
        working_file = Global.import_working + clinic_dir + '/' + filename
        ImportSqlFile.logger.info(working_file)
        fo = open(working_file, "r", encoding='utf-8')
        line = fo.readline()
        idx = 0
        #db = DbUtil(Global.db_conn_central)
        cursor = self._db.get_cursor()
        sql_line = ''
        while line:
            sql_line += line
            if sql_line.endswith(');\n'):
                try:
                    cursor.execute(sql_line)
                    if idx == int(idx/500)*500:
                        self._db.commit()
                except Exception as e:
                    print('py_odbc or.Error')
                    Logger.get_error_logger().info('--{0}---{1}----'.format(self._db.get_error(), working_file))
                    Logger.get_error_logger().info(sql_line)
                    Logger.get_error_logger().info(str(e))
                idx += 1
                sql_line = ''
            #end excute one sql line

            line = fo.readline()
        self._db.commit()
        fo.close()
        if not os.path.exists(Global.import_done):
            os.mkdir(Global.import_done)
        if not os.path.exists(Global.import_done + clinic_dir):
            os.mkdir(Global.import_done + clinic_dir)
        os.rename(working_file, Global.import_done + clinic_dir + '/' + filename)# .replace('sql.working.sql', 'done.sql')


def scan_and_execute_loop():
    while True:
        scan_and_execute()
        print('Thread Sleep 10s')
        time.sleep(10)

def scan_and_execute():
    db = DbUtil(Global.db_conn_central)
    imf = ImportSqlFile(db)
    imf.process(Global.export_dir)

def test_import_one_sqlserver():
    db = DbUtil(Global.db_conn_central)
    imf = ImportSqlFile(db)
    imf.import_one_file_batch_commit('1001','DM_ActionGuideline.sql')

if __name__ == "__main__":
    #scan_and_execute()
    test_import_one_sqlserver()



