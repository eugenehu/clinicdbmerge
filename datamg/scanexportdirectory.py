import os, time, datetime
import paramiko
import zipfile, gzip
from common.settings import *
from multiprocessing.pool import Pool
from common.utils import *
import zipfile,os
import boto3
import psycopg2
import  gzip
from boto3.session import Session

session = Session(aws_access_key_id='AKIAJRIFKFAO2ABSNV7A',
                  aws_secret_access_key='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')
uploaded_directory = Global.base_dir + '/uploaded/'


def scan_directory(dirpath, hold=False):
    dirs = os.listdir(dirpath)
    # 开启十个进程
    pool = Pool(10)
    start_time = time.time()
    while True:
        for clinic_dir in dirs:
            sql_file_list = os.listdir(dirpath + clinic_dir)
            print('Search: ' + dirpath + clinic_dir)
            i = 0
            total = len(sql_file_list)
            for file in sql_file_list:
                if (file.endswith('.csv') or file.endswith('.txt')) and 'working' not in file:
                    i += 1
                    pool.apply_async(func=pool_temp_deal, args=(dirpath, clinic_dir, file, i, total),
                                     callback=upload_progress)  # 维持执行的进程总数为processes，当一个进程执行完毕后会添加新的进程进去
        if not hold:
            break
        pool.close()
        pool.join()  # 进程池中进程执行完毕后再关闭，如果注释，那么程序直接关闭。
        pool.terminate()
        time.sleep(5)
        print(datetime.datetime.now())


    print('耗时：{}'.format(time.time() - start_time))


def pool_temp_deal(dirpath, clinic_dir, file, i=0, total=0):
    old_file_path = dirpath + clinic_dir + '/' + file
    res = ''
    try:
        # 压缩文件存放路径
        # zip_path = dirpath + clinic_dir + '/' + file + '.zip'
        # if os.path.exists(zip_path):
        #     remove_zip_file(zip_path)
        # zip_file(zip_path, old_file_path, file)
        # 采用gzip 压缩 更高效
        # 描述文件不用压缩
        if file.endswith('.txt'):
            zip_path = old_file_path
            server_file = clinic_dir + '/' + file
        else:
            zip_path = gzip_file(old_file_path)
            server_file = clinic_dir + '/' + file + '.gz'
        # 待上传的文件名 以及 文件路径
        upload_to_server(server_file, zip_path, i, total)
        if not file.endswith('.txt'):
            remove_file(zip_path)
        # 转移到完成目录
        move_to_completed_directory(file, clinic_dir, old_file_path)
    except Exception as e:
        res = 'Error ' + old_file_path + ':' + str(e)
    return res


def upload_progress(res):
    if res:
        print(res)


def upload_to_server(filename, zip_path, i=0, total=0):
    transport = paramiko.Transport(('s-d02db4d9caeb4f72a.server.transfer.us-east-1.amazonaws.com', 22))
    pkey = r'../key/my'
    pkey = os.path.expanduser(pkey)
    key = paramiko.RSAKey.from_private_key_file(pkey, password='mdland2018')
    transport.connect(username='sqlupload', password='mdland2018', pkey=key)
    print('---------transporting {0}/{1} {2}---------------'.format(i, total, filename))
    sftp = paramiko.SFTPClient.from_transport(transport)  # 如果连接需要密钥，则要加上一个参数，hostkey="密钥"
    try:
        sftp.put(zip_path, '/dws3-eugene-sftp/test/' + filename)  # 将本地的Windows.txt文件上传至服务器/root/Windows.txt
    except Exception as e:
        print(str(e))
    sftp.close()
    transport.close()  # 关闭连接


def move_to_completed_directory(filename, clinic_dir, old_file_path):
    # 移到 uploaded 文件夹中
    if not os.path.exists(uploaded_directory):
        os.mkdir(uploaded_directory)
    if not os.path.exists(uploaded_directory + clinic_dir):
        os.mkdir(uploaded_directory + clinic_dir)
    if os.path.exists(uploaded_directory + clinic_dir + '/' + filename):
        remove_file(uploaded_directory + clinic_dir + '/' + filename)
    os.rename(old_file_path, uploaded_directory + clinic_dir + '/' + filename)


def upload_file():
    s3 = session.resource('s3')
    s3BucketName = 'dws3-eugene-sftp'
    bucket = s3.Bucket(s3BucketName)
    dirpath = Global.redshift_dir
    dirs = os.listdir(dirpath)
    for clinic_dir in dirs:
        sql_file_list = os.listdir(dirpath + clinic_dir)
        print('Search: ' + dirpath + clinic_dir)
        i = 0
        total = len(sql_file_list)
        for file in sql_file_list:
            if (file.endswith('.csv') or file.endswith('.txt')) and 'working' not in file:
                i += 1
                # Upload a new file

                old_file_path = dirpath + clinic_dir + '/' + file
                if file.endswith('.txt'):
                    zip_path = old_file_path
                    server_file = clinic_dir + '/' + file
                else:
                    zip_path = gzip_file(old_file_path)
                    server_file = clinic_dir + '/' + file + '.gz'

                data = open(zip_path, 'rb')
                res = bucket.put_object(Key='test/' + server_file, Body=data)
                print("-----------{}/{}----{}-----".format(i, total, res))
                if not file.endswith('.txt'):
                    remove_file(zip_path)
                # 转移到完成目录
                move_to_completed_directory(file, clinic_dir, old_file_path)


def remove_file(path):
    os.remove(path)


def zip_file(zip_path, old_file_path, file):
    zip = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED)
    zip.write(old_file_path, file)
    zip.close()


def gzip_file(old_file_pah):
    f_in = open(old_file_pah, "rb")  # 打开文件
    f_out = gzip.open(old_file_pah + ".gz", "wb")  # 创建压缩文件对象
    f_out.writelines(f_in)
    f_out.close()
    f_in.close()
    return old_file_pah + ".gz"


if __name__ == "__main__":
    scan_directory(Global.redshift_dir, hold=True)
