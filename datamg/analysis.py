import string, pprint
from common.utils import *
from common.outfile import OutFile

'''analysis table
and output: TableName, ClinicID, PatientID, VisitID, EmployeeID, Identity, RecordCount
'''


def analysis_table():
    db = DbUtil(Global.db_conn_central)
    table_list = db.get_tables()

    of = OutFile(Global.log_dir + 'table_info.csv')

    of.write('Seq, TableName, ClinicID, PatientID, VisitID,BillingID, InboxID, LabID, CreateDate,ModifyDate, RecordCount, PK_Column_Count, PK_COL_1,'
             'PK_Name_1, PK_COL_2,PK_Name_2, PK_COL_3,PK_Name_3')

    idx = 0
    for tbl in table_list:
        if Global.is_ignore_table(tbl.table_name):
            continue
        idx += 1
        column_list = db.get_columns(tbl.table_name)
        has_clinicid = has_column(column_list, 'clinicid')
        has_patientid = has_column(column_list, 'patientid')
        has_visitid = has_column(column_list, 'visitid')
        recoud_count = db.query_sql_to_int('select count(1) from ' + tbl.table_name)
        table_info = ('{0},{1},{2},{3},{4}'.format(idx, tbl.table_name, has_clinicid, has_patientid, has_visitid))

        table_info += ',{0}'.format(has_column(column_list, 'billingid'))
        table_info += ',{0}'.format(has_column(column_list, 'labid'))
        table_info += ',{0}'.format(has_column(column_list, 'CreateDate'))
        table_info += ',{0}'.format(has_column(column_list, 'ModifyDate'))

        has_inboxid = has_column(column_list, 'inboxid')
        table_info += ',{0}'.format(has_inboxid)

        table_info += ',{0}'.format(recoud_count)

        pk_list = db.get_primary_keys(tbl.table_name)
        table_info += (',{0}').format(len(pk_list))
        for pk in pk_list:
            col = db.get_column(tbl.table_name, pk.column_name)
            table_info += ',{0} ,{1}'.format(col.column_name, col.type_name)
        of.write(table_info)


def has_column(col_list, col_name):
    col_name = col_name.lower()
    res = 0
    for col in col_list:
        if col.column_name.lower() == col_name:
            res = 1
            break
    return res


if __name__ == "__main__":
    analysis_table()
    print('print')
