USE [ec26]
GO
/****** Object:  View [dbo].[View_mongo_Lab_Order]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Lab_Order]
AS
SELECT
	a.OrderID,
	a.ClinicID,
	Clinic.ClinicName,
	p.PatientID,
	p.MPatientID,
	CONVERT(VARCHAR(10), p.PatientID) + '-' + CONVERT(VARCHAR(4), a.ClinicID) PatientUID,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	CASE OrderType WHEN 1 THEN 'Order' WHEN 2 THEN 'Order' WHEN 3 THEN 'Imaging' WHEN 4 THEN 'Imaging' ELSE 'Order' END RecordType,
	a.OrderType,
	a.BillType,
	OrderID ReferenceTableID,
	a.DoctorID,
	RTRIM(e.EmployeeLastName) + ', ' + RTRIM(e.EmployeeFirstName) DoctorName,
	a.VisitID,
	a.VisitDate,
	a.OLID OfficeLocationID,
	o.Name OfficeLocationName,
	a.OrderDate,
	a.ServiceDate,
	a.CollectionDate,
	a.ReportDate,
	a.[Status],
	L.LabIncName LabCompany,
	L.LabIncCode,
	L.AccountNo LabIncAccountNo,
	CASE L.LabType WHEN 1 THEN 'Lab' WHEN 2 THEN 'Imaging' WHEN 3 THEN 'Lab/Imaging' ELSE '' END LabIncType,
	r.CDName Catetory,
	a.CreateDate CreateDate
FROM
	LabOrder_Order a WITH (NOLOCK)
	JOIN Patient p WITH (NOLOCK) ON a.PatientID = p.PatientID
	LEFT JOIN Employee e WITH (NOLOCK) ON a.DoctorID = e.EmployeeID
	LEFT JOIN LabOrder_LabInc L WITH (NOLOCK) ON a.LabIncID = L.LabIncID
	LEFT JOIN Ref_Customization r WITH (NOLOCK) ON a.ClinicID = r.ClinicID AND r.CDID = 3 AND a.CategoryID = r.CDNumber
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID AND ISNULL(Clinic.[Status], 0) = 0
	LEFT JOIN Ref_ClinicOfficeLocation o ON a.OLID = o.OLID
--	LEFT JOIN Pati_Daily_Tracking pdt WITH (NOLOCK) ON a.PatientID = pdt.PatientID AND a.ClinicID = pdt.ClinicID AND pdt.TrackingType='Patient' AND pdt.TableID = a.PatientID
--	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
--WHERE
--	( c.LastSyncDate IS NULL OR (c.LastSyncDate IS NOT NULL AND pdt.RowID IS NOT NULL ))
GO
/****** Object:  View [dbo].[View_mongo_Lab_OrderDetail]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Lab_OrderDetail]
AS
SELECT
	b.ClinicID,
	b.PatientID,
	a.OrderID,
	a.ID LabDetailID,
	LabName,
	LabID,
	LabCode TestNum,
	--LoincCode LOINC,
	CASE
		WHEN LoincCode IS NOT NULL AND LEN(RTRIM(LoincCode)) > 0 THEN RTRIM(LoincCode)
		WHEN RTRIM(LabName) IN ('HEMOGLOBIN A1C.', 'HEMOGLOBIN A1C (GLYCOHGB)', 'HEMOGLOBIN A1C', 'HB A1C') THEN '4548-4'
		WHEN RTRIM(LabName) LIKE '%HEMOGLOBIN%' AND RTRIM(LabName) LIKE '%A1C%' AND (LoincCode IS NULL OR LEN(RTRIM(LoincCode)) = 0) THEN '4548-4'
		WHEN RTRIM(LabName) LIKE '%HbA1c%' AND (LabName IS NULL OR LEN(RTRIM(LoincCode)) = 0) THEN '4548-4'
		ELSE RTRIM(LoincCode)
		END LOINC,  -- Added on 8/14/2018
	Result,
	Unit Units,
	Abnormal,
	[Range],
	[Description],
	Note,
	CASE WHEN LabName LIKE 'Xray%'
	       OR LabName LIKE 'CT%'
		   OR LabName LIKE '%Mammography%'
		   OR LabName LIKE '%Ultrasound%'
		   OR LabName LIKE '%Colonoscopy%'
		   OR LabName LIKE '%Nuclear Medicine%'
		   OR LabName LIKE '%Bone Density%'
		   OR LabName LIKE 'MRI%'
		   OR LabName LIKE 'MRA%'
		   OR LabName LIKE '%Endoscopy%'
	THEN 'Y' ELSE 'N' END IsRadiology
FROM
	LabOrder_OrderLab a WITH (NOLOCK)
	JOIN View_mongo_Lab_Order b WITH (NOLOCK) ON a.OrderID = b.OrderID
GO
/****** Object:  View [dbo].[View_mongo_Lab_Imaging]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Lab_Imaging]
AS
SELECT
	b.ClinicID,
	b.PatientID,
	a.OrderID,
	a.ImagingID,
	r.ImagingName,
	r.LOINCNum LOINC,
	a.ImagingL2Category,
	a.ImagingL2,
	a.Indications,
	a.ImagingOther,
	a.IndicationOther,
	CASE WHEN ImagingName LIKE 'Xray%'
	    OR ImagingName LIKE 'CT%'
		OR ImagingName LIKE '%Mammography%'
		OR ImagingName LIKE '%Ultrasound%'
		OR ImagingName LIKE '%Colonoscopy%'
		OR ImagingName LIKE '%Nuclear Medicine%'
		OR ImagingName LIKE '%Bone Density%'
		OR ImagingName LIKE 'MRI%'
		OR ImagingName LIKE 'MRA%'
		OR ImagingName LIKE '%Endoscopy%'
	THEN 'Y' ELSE 'N' END IsRadiology
FROM
	LabOrder_OrderImaging a WITH (NOLOCK)
	JOIN View_mongo_Lab_Order b WITH (NOLOCK) ON a.OrderID = b.OrderID
	JOIN LabOrder_Ref_Imaging r WITH (NOLOCK) ON a.ImagingID = r.ImagingID
GO
/****** Object:  View [dbo].[Ref_CodeSystem]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Ref_CodeSystem]
AS
   select a.CodeID CodeSystemID,a.Code,a.CodeDescription,a.CodeSysName,b.ValueSetName,c.ValueSetTypeName Type
          from Ref_MeasureValueSet_Code a,Ref_MeasureValueSet_Set b,Ref_MeasureValueSet_Type c
          where a.ValueSetID = b.ValueSetID and a.ValueSetTypeID = c.ValueSetTypeID


GO
/****** Object:  View [dbo].[v_Finance]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create VIEW [dbo].[v_Finance]
AS
select b.BillingID,
       b.BillingID BillID,
       b.ClinicID,
       b.PatientID,
       b.DoctorID,
       b.InsuComID,
       b.b34Name InsuComName,
       b.VisitID,
       b.C4form,
       b.ClaimDate as Created,
       b.ClaimDate as DOS,
       pip.PlanName as Insurance,
       b.InsuPlanID as InsuID,
       b.b2 as description,
       b.Status as status,
       b.OfficeLocation as POS,
       ltrim(rtrim(p.PatientLastName)) + ',' + ltrim(rtrim(p.PatientFirstName)) + ' ' + ltrim(rtrim(p.PatientMidName)) as Patient,
       ltrim(rtrim(e.EmployeeLastName)) + ',' + ltrim(rtrim(e.EmployeeFirstName)) + ' ' + ltrim(rtrim(e.EmployeeMidName)) as CovDoctor,
       ltrim(rtrim(e.EmployeeLastName)) + ',' + ltrim(rtrim(e.EmployeeFirstName)) + ' ' + ltrim(rtrim(e.EmployeeMidName)) as AttnDoctor,
       ltrim(rtrim(e.EmployeeLastName)) + ',' + ltrim(rtrim(e.EmployeeFirstName)) + ' ' + ltrim(rtrim(e.EmployeeMidName)) as BillingDoctor,
       a.FeeCopay as CoInsu,
       a.FeeDeduct as Deductible,
       a.FeeWriteOff as WriteOff,
       a.FeePay as InsuPaid,
       a.Feepay as PatientPaid,
       a.FeeCharge as Amount,
       a.FeeApprove as Allowed,
       a.FeeCoPay as Copay,
       a.FeeBalance as Balance,
       b.Status as UserFlag
from   dbo.Billing_1500Form as b
       left outer join dbo.Patient as p on  b.PatientID = p.PatientID
       left outer join dbo.Employee as e on  b.DoctorID = e.EmployeeID
       left outer join dbo.Pati_InsuPlan as pip on  pip.InsuPlanID = b.InsuPlanID
       left join dbo.Billing_Account a on a.ClinicID = b.ClinicID and a.BillingID = b.BillingID


GO
/****** Object:  View [dbo].[v_Forms_General_3_150]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[v_Forms_General_3_150] as
select ClinicID, PatientID, FormDetailID ID, CreateDate from [dbo].[Forms_General_3] where FormID = 150 and status = 1

GO
/****** Object:  View [dbo].[v_Forms_General_3_151]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[v_Forms_General_3_151] as
select ClinicID, PatientID, FormDetailID ID, CreateDate from [dbo].[Forms_General_3] where FormID = 151 and status = 1

GO
/****** Object:  View [dbo].[v_Forms_General_3_152]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[v_Forms_General_3_152] as
select ClinicID, PatientID, FormDetailID ID, CreateDate from [dbo].[Forms_General_3] where FormID = 152 and status = 1

GO
/****** Object:  View [dbo].[v_Medi_Payment]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- create a view on medi_payment that use the creditdebit to create a signed amount that can be worked with more easily
CREATE VIEW [dbo].[v_Medi_Payment]
AS
SELECT     a.ID, a.VisitID, a.BillingID, a.ClinicID, a.DoctorID, a.PatientID, a.PayDate, a.Amount, a.PayType, a.PayMethod, a.Ref#, a.Memo, a.Balance, a.CreateDate,
                      a.LastModifyBy, a.LastModifyDate, a.Location, a.CPTCode, a.Status, a.ApplyToBillingID, a.PaymentFlag, a.BatchNumber, a.PayMent, a.TempID,
                      a.Amount * b.CreditDebit AS SignedAmount
FROM         dbo.Medi_PayMent AS a INNER JOIN
                      dbo.Medi_Payment_PayType AS b ON a.PayType = b.PayType

GO
/****** Object:  View [dbo].[v_Pati_InsuPlan]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[v_Pati_InsuPlan]
as
select isnull(EffectiveDate,isnull(
           (
               select min(h.LastModifyDate)
               from   Pati_InsuPlan_History h
               where  h.ClinicID = p.ClinicID
                      and h.PatientID = p.PatientID
                      and h.InsuPlanID = p.InsuPlanID
                      and p.[Active] = 1
           ),
           p.CreateDate
       )) StartDate,
       case
            when p.active = 1 then isnull(PlanExpire,getdate())
            else isnull(PlanExpire,isnull(
                     (
                         select max(h.LastModifyDate)
                         from   Pati_InsuPlan_History h
                         where  h.ClinicID = p.ClinicID
                                and h.PatientID = p.PatientID
                                and h.InsuPlanID = p.InsuPlanID
                                and p.[Active] = -1
                     ),
                     p.CreateDate
                 ))
       end EndDate,
       p.*
from   Pati_InsuPlan p
GO
/****** Object:  View [dbo].[V_RPM_Conditions]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[V_RPM_Conditions]
                                                  as
                                                  select * from [192.168.168.66].[RPMDB].dbo.RPM_Conditions
GO
/****** Object:  View [dbo].[V_RPM_OtherConditions]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[V_RPM_OtherConditions]
                                                  as
                                                  select * from [192.168.168.66].[RPMDB].dbo.RPM_OtherConditions
GO
/****** Object:  View [dbo].[V_RPM_Patient]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[V_RPM_Patient]
                                                  as
                                                  select * from [192.168.168.66].[RPMDB].dbo.RPM_Patient
GO
/****** Object:  View [dbo].[View_Form_AlcoholDrugScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Form_AlcoholDrugScreening]
AS
SELECT     ID,ClinicID,PatientID ,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,CreateDate VisitDate,CreateBy,ModifyDate,ModifyBy,VisitDate CreateDate,Status,TotalScore
FROM         dbo.Form_AlcoholDrugScreening
WHERE     (Status = 1)
GO
/****** Object:  View [dbo].[View_Form_AlcoholScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[View_Form_AlcoholScreening]
AS
SELECT     ID, ClinicID, PatientID, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11_1_1, s11_1_2, s11_1_3, s11_1_4, s11_1_5, s11_2_1, s11_2_2, s11_2_3, s11_2_4,
                      s11_2_5, s11_2_6, s11_2_Scale, s11_2_dateValue, s11_2_dateType, s11_3_1, s11_3_2, s11_3_3, s11_3_4, s11_3_5, s11_3_6, s11_3_7, s11_3_8,
                      s11_3_Scale, s11_3_dateType, s11_3_dateValue, s11_3_minutes, s11_3_referred, s11_4_1, s11_4_2, s11_4_3, s11_4_4, s11_4_5, s11_4_6,
                      s11_4_7, s11_4_8, s11_4_9, s11_4_10, s11_4_11, s11_4_Scale, s11_4_dateType, s11_4_dateValue, s11_4_minutes, s11_4_referred, s12,
                      NoteComment, ModifyBy, CreateBy, VisitDate CreateDate, CreateDate CreateDate2, ModifyDate, Status
FROM         dbo.Form_AlcoholScreening
WHERE     (Status = 1)



GO
/****** Object:  View [dbo].[View_Form_DepressionScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Form_DepressionScreening]
AS
SELECT     ID, PatientID, ClinicID, S1, S2, S3, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q9, Q8, Q10, selfScore, CreateDate VisitDate, Status, VisitDate CreateDate, ModifyBy, CreateBY,
                      ModifyDate
FROM         dbo.Form_DepressionScreening
WHERE     (Status = 1)


GO
/****** Object:  View [dbo].[View_Form_DrugDast]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[View_Form_DrugDast]
AS
SELECT     ID, VisitID, ClinicID, PatientID, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q11_Note, Q11_Scale, Q11_ReturnIn, Q11_Return, Q11_Referred, Q12,
                      DastTotalScore, CreateDate CreateDate2, VisitDate CreateDate, Status, IsDefault
FROM         dbo.Form_DrugDast
WHERE     (Status = 1)



GO
/****** Object:  View [dbo].[View_Form_MiniMHExam]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[View_Form_MiniMHExam]
AS
SELECT     ID, s1, s2, s3, s4, s7, s6, s5, s8, s9, s10, s11, sLevel, CreateDate AS CreateDate2, Status, VisitDate AS CreateDate, ModifyBy, CreateBy, ModifyDate,
                      ClinicID, PatientID, VisitID
FROM         dbo.Form_MiniMHExam
WHERE     (Status = 1)


GO
/****** Object:  View [dbo].[View_Form_PHQ9TeenScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Form_PHQ9TeenScreening]
AS
SELECT     ID,ClinicID,PatientID ,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,CreateDate VisitDate,CreateBy,ModifyDate,ModifyBy,VisitDate CreateDate,Status,TotalScore
FROM         dbo.Form_PHQ9TeenScreening
WHERE     (Status = 1)
GO
/****** Object:  View [dbo].[View_Form_StandardAssessment]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_Form_StandardAssessment]
AS
SELECT     ID, ClinicID, PatientID, VisitID, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, DentalStatus1, DentalStatus2, AssessmentDate, ModifyBy, CreateBy, VisitDate,
                      VisitDate AS CreateDate, CreateDate AS CreateDate2, ModifyDate, status
FROM         dbo.Form_StandardAssessment
WHERE     (status = 1)


GO
/****** Object:  View [dbo].[view_HM_LinkInfo]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[view_HM_LinkInfo]
as
	select HMID,100 LinkType,ClinicID from dbo.Ref_HM_LinkDocument
	union
	select HMID,LinkTypeInt LinkType,ClinicID from Ref_HM_LinkOthers
	union
	select HMID,200 LinkType,ClinicID from dbo.Ref_HM_LinkLabs
	union
	select HMID,300 LinkType,ClinicID from dbo.Ref_HM_LinkCPT
	union
	select HMID,400 LinkType,ClinicID from dbo.Ref_HM_LinkVaccine

--------------------------------------------bug 1086 end---------------------------------------------

GO
/****** Object:  View [dbo].[view_Medi_Payment]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[view_Medi_Payment]
as
  select PatientID,ClinicID,sum(case when  PayType in ('S','E','P') then 1 else 0 end * Amount)Amount
         from dbo.Medi_PayMent
         where Status = 1
         group by PatientID,ClinicID


GO
/****** Object:  View [dbo].[View_mongo_ACP]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_ACP]
AS
SELECT
	p.ClinicID,
	PatientID,
	VisitID,
	VisitDate,
	ACPCode,
	EngagementStatus,
	ACPValue
FROM
	Medi_OV_ACP p
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON p.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	p.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  p.PatientID = d.PatientID
--			                                                AND p.ClinicID = d.ClinicID
--			                                                AND d.TableID = p.VisitID
--			                                                AND d.TrackingType = 'OV'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)





GO
/****** Object:  View [dbo].[View_mongo_Appointment]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Appointment]
AS
SELECT
	ID AppointmentID,
	CONVERT(CHAR(10), ID) + '-' + CONVERT(CHAR(4), A.ClinicID) AppointmentUID,
	A.ClinicID,
	DoctorID,
	CONVERT(CHAR(10), DoctorID) + '-' + CONVERT(CHAR(4), A.ClinicID) EmployeeUID,
	PatientID,
	CONVERT(CHAR(10), PatientID) + '-' + CONVERT(CHAR(4), A.ClinicID) PatientUID,
	PatientName,
	HomePhone,
	WorkPhone,
	Insurance,
	BeginDateTime,
	EndDateTime,
	A.Memo,
	CASE NewPatient WHEN 'Y' THEN 'Y' ELSE 'N' END IsNewPatient,
	LocationID,
	A.BusinessTypeID,
	B.BusinessTypeName,
	CASE IsWalkIn WHEN 1 THEN 'Y' ELSE 'N' END IsWalkIn,
	A.[Status],
	CASE A.[Status] WHEN 0 THEN 'New' WHEN 1 THEN 'Confirmed' WHEN 2 THEN 'Checked in' WHEN 3 THEN 'Cancelled' WHEN 4 THEN 'No-Show' WHEN 5 THEN 'Deleted' WHEN 6 THEN 'Patient Requested' WHEN 7 THEN 'Reschedule Appointment' ELSE '' END StatusDesc,
	CASE SMSStatus WHEN -1 THEN '' WHEN 2 THEN 'Queued' WHEN 3 THEN 'Success' ELSE NULL END SMSStatus,
	CASE TeleHealth WHEN 1 THEN 'Y' ELSE 'N' END IsTeleHealth,
	TeleHealthTranslator,
	CASE PatientWithTOC WHEN 1 THEN 'Y' ELSE 'N' END PatientWithTOC,
	CreateDate,
	ModifyDate
FROM
	Appointment A
	LEFT JOIN Business_Type B ON A.BusinessTypeID = B.BusinessTypeID AND A.ClinicID = B.ClinicID
WHERE
	A.ClinicID <> 1007
	AND DATEDIFF(dd, ModifyDate, GETDATE()) <= 3
GO
/****** Object:  View [dbo].[View_mongo_assessmentplan]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- Assessment/Plan
CREATE View [dbo].[View_mongo_assessmentplan]
AS
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		GI_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Derm_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		OB_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Int_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Cardiol_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		On_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Ophth_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		PMR_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Pulm_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Nephro_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Peds_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Neuro_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Surg_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		Ent_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)
UNION ALL
	SELECT
		a.ClinicID, v.PatientID, a.VisitID, a.ICDCode, a.ICDName, a.CodeSystem, a.PlanDetail, a.[Procedure]
	FROM
		MntH_Data_Assessment a
		JOIN Medi_OfficeVisit v ON a.VisitID=v.VisitID
		LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
		JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	--WHERE
	--	a.ClinicID <> 1007
	--	AND ( c.LastSyncDate IS NULL
	--			OR  ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--														 WHERE  v.PatientID = d.PatientID
	--																AND a.ClinicID = d.ClinicID
	--																AND d.TableID = a.VisitID
	--																AND d.TrackingType = 'OV'
	--																AND d.TrackDate >= c.LastSyncDate) )
	--	)





GO
/****** Object:  View [dbo].[View_mongo_Billing1500Form]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Billing1500Form]
AS
SELECT
	b.ClinicID,
	Clinic.ClinicName,
	b.PatientID,
	p.MPatientID MPI,
	b.BillingID,
	b.PBillingID,
	b.VisitID,
	RTRIM(ISNULL(p.PatientFirstName, '')) PatientFirstName,
	RTRIM(ISNULL(p.PatientLastName, '')) PatientLastName,
	RTRIM(ISNULL(p.PatientMidName, '')) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	b.DoctorID,
	RTRIM(ISNULL(e1.EmployeeFirstName, '')) DoctorFirstName,
	RTRIM(ISNULL(e1.EmployeeLastName, '')) DoctorLastName,
	RTRIM(ISNULL(e1.EmployeeMidName, '')) DoctorMidName,
	b.PCPID,
	RTRIM(ISNULL(e2.EmployeeFirstName, '')) PCPFirstName,
	RTRIM(ISNULL(e2.EmployeeLastName, '')) PCPLastName,
	RTRIM(ISNULL(e2.EmployeeMidName, '')) PCPMidName,
	e2.EmployeeNPI PCPNPI,
	b.BillAsPCP,
	b.BillDoctorID,
	RTRIM(ISNULL(e3.EmployeeFirstName, '')) BillDoctorFistName,
	RTRIM(ISNULL(e3.EmployeeLastName, '')) BillDoctorLastName,
	RTRIM(ISNULL(e3.EmployeeMidName, '')) BillDoctorMidName,
	e3.EmployeeNPI BillDoctorNPI,
	b.GroupBill,
	b.InsuPlanID,
	--h.InsuredRelationShip,
	b.InsuOrder,
	b.InsuComID,
	b.TempInsuComID,
	i.InsuComName,
	RTRIM(i.InsuComSubmitterID) InsuComSubmitterID,
	b.Status,
	b.LastStatus,
	b.Payment,
	CASE b1 WHEN 1 THEN 'Medicare' WHEN 2 THEN 'Medicaid' WHEN 3 THEN 'Tricare' WHEN 4 THEN 'Champva' WHEN 5 THEN 'Group' WHEN 6 THEN 'FECA' WHEN 7 THEN 'Other' ELSE NULL END b1,
	RTRIM(b1a) b1a,
	b2, RTRIM(b2FirstName) b2FirstName, RTRIM(b2LastName) b2LastName, RTRIM(b2MidName) b2MidName,
	b3, b3Sex,
	b4, RTRIM(b4FirstName) b4FirstName, RTRIM(b4LastName) b4LastName, RTRIM(b4MidName) b4MidName,
	b5Address, RTRIM(b5City) b5City, b5State, RTRIM(b5Zip) b5Zip, b5Phone,
	b6,
	b7Address, RTRIM(b7City) b7City, b7State, RTRIM(b7Zip) b7Zip, b7Phone,
	b8a, b8b, b8Reserved,
	RTRIM(b9) b9, RTRIM(b9a) b9a, b9b, RTRIM(b9bSex) b9bSex, RTRIM(b9c) b9c, b9bReserved, b9cReserved, RTRIM(b9d) b9d,
	b10a, b10b, RTRIM(b10bPlace) b10bPlace, b10c, b10d,
	RTRIM(b11) b11, b11a, b11aSex, b11bReserved,
	RTRIM(b11b) b11b, RTRIM(b11c) b11c, b11d,
	b12, b12Date,
	b13,
	b14, b14Qual,
	b15, b15Qual,
	b16From, b16To,
	b17, RTRIM(b17FirstName) b17FirstName, RTRIM(b17LastName) b17LastName, RTRIM(b17MidName) b17MidName, RTRIM(b17a) b17a, b17b, b17Indicator,
	b18From, b18To,
	b19, b19Template, b19CPT,
	RTRIM(b20) b20, b20Charges,
	b21Indicator,
	RTRIM(b22Code) b22Code, RTRIM(b22No) b22No, b22FrequencyTypeCode, b22DelayReasonCode,
	RTRIM(b23) b23,
	RTRIM(b25) b25, b25Type,
	b26, b27, b28, b29,
	b30, b30Reserved,
	b31, b31Date,
	b32Name, b32Addr, b32Addr2, RTRIM(b32City) b32City, RTRIM(b32State) b32State, RTRIM(b32Zip) b32Zip, b32Phone, b32NPI, b32ID,
	b33Name, b33FirstName, b33LastName, b33MidName, b33Addr, b33Addr2, b33City, b33State, RTRIM(b33Zip) b33Zip, b33Phone, RTRIM(b33PIN) b33PIN, RTRIM(b33GRP) b33GRP, RTRIM(b33UPIN) b33UPIN, RTRIM(b33ProviderID) b33ProviderID, b33NPI, b33ID,
	b32ZipExt, b33ZipExt, b34ZipExt, b5ZipExt, b7ZipExt,
	b34Name, b34Addr, b34Addr2, RTRIM(b34Apt) b34Apt, b34City, b34State, RTRIM(b34Zip) b34Zip, b34Phone,
	ClaimDate, ClaimPrintDate, SendDate, PrintBy,
	UserClaimComments, EMC, OfficeLocation, EMCBatchNumber, BillingAddressID, C4form,
	b.LastModifier, b.LastModifyDate,
	DoctorNPI,
	RefDoctorID,
	COStatus,
	[Message],
	MSP,
	BillingValidation, RequestStatus, RequestStatusDate, RequestStatusBy,
	BillCreateDate, BillCreateBy,
	C4Type, FacilityID, FormVersion, ICDCodeSystem
FROM
	Billing_1500Form b WITH(NOLOCK)
	--LEFT JOIN Pati_InsuPlan h ON h.InsuPlanID = b.InsuPlanID
	LEFT JOIN Patient p WITH(NOLOCK) ON p.PatientID = b.PatientID
	LEFT JOIN Ref_Insucom i WITH(NOLOCK) ON i.InsuComID = b.TempInsuComID
	LEFT JOIN Employee e1 WITH(NOLOCK) ON e1.EmployeeID = b.DoctorID
	LEFT JOIN Employee e2 WITH(NOLOCK) ON e2.EmployeeID = b.PCPID
	LEFT JOIN Employee e3 WITH(NOLOCK) ON e3.EmployeeID = b.BillDoctorID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON b.ClinicID = Clinic.ClinicID and ISNULL(Clinic.[Status], 0) = 0
	JOIN Pati_Daily_Tracking pdt WITH(NOLOCK)
		ON b.PatientID = pdt.PatientID
		   AND b.ClinicID = pdt.ClinicID
		   AND pdt.TrackingType='Billing'
		   AND b.BillingID = pdt.TableID
WHERE
	b.ClinicID <> 1007
	AND ( c.LastSyncDate IS NULL OR (c.LastSyncDate IS NOT NULL AND pdt.RowID is not null) )
GO
/****** Object:  View [dbo].[View_mongo_Billing1500Form_Init]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Billing1500Form_Init]
AS
SELECT
	b.ClinicID,
	Clinic.ClinicName,
	b.PatientID,
	p.MPatientID MPI,
	b.BillingID,
	b.PBillingID,
	b.VisitID,
	RTRIM(ISNULL(p.PatientFirstName, '')) PatientFirstName,
	RTRIM(ISNULL(p.PatientLastName, '')) PatientLastName,
	RTRIM(ISNULL(p.PatientMidName, '')) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	b.DoctorID,
	RTRIM(ISNULL(e1.EmployeeFirstName, '')) DoctorFirstName,
	RTRIM(ISNULL(e1.EmployeeLastName, '')) DoctorLastName,
	RTRIM(ISNULL(e1.EmployeeMidName, '')) DoctorMidName,
	b.PCPID,
	RTRIM(ISNULL(e2.EmployeeFirstName, '')) PCPFirstName,
	RTRIM(ISNULL(e2.EmployeeLastName, '')) PCPLastName,
	RTRIM(ISNULL(e2.EmployeeMidName, '')) PCPMidName,
	e2.EmployeeNPI PCPNPI,
	b.BillAsPCP,
	b.BillDoctorID,
	RTRIM(ISNULL(e3.EmployeeFirstName, '')) BillDoctorFistName,
	RTRIM(ISNULL(e3.EmployeeLastName, '')) BillDoctorLastName,
	RTRIM(ISNULL(e3.EmployeeMidName, '')) BillDoctorMidName,
	e3.EmployeeNPI BillDoctorNPI,
	b.GroupBill,
	b.InsuPlanID,
	--h.InsuredRelationShip,
	b.InsuOrder,
	b.InsuComID,
	b.TempInsuComID,
	i.InsuComName,
	RTRIM(i.InsuComSubmitterID) InsuComSubmitterID,
	b.Status,
	b.LastStatus,
	b.Payment,
	CASE b1 WHEN 1 THEN 'Medicare' WHEN 2 THEN 'Medicaid' WHEN 3 THEN 'Tricare' WHEN 4 THEN 'Champva' WHEN 5 THEN 'Group' WHEN 6 THEN 'FECA' WHEN 7 THEN 'Other' ELSE NULL END b1,
	RTRIM(b1a) b1a,
	b2, RTRIM(b2FirstName) b2FirstName, RTRIM(b2LastName) b2LastName, RTRIM(b2MidName) b2MidName,
	b3, b3Sex,
	b4, RTRIM(b4FirstName) b4FirstName, RTRIM(b4LastName) b4LastName, RTRIM(b4MidName) b4MidName,
	b5Address, RTRIM(b5City) b5City, b5State, RTRIM(b5Zip) b5Zip, b5Phone,
	b6,
	b7Address, RTRIM(b7City) b7City, b7State, RTRIM(b7Zip) b7Zip, b7Phone,
	b8a, b8b, b8Reserved,
	RTRIM(b9) b9, RTRIM(b9a) b9a, b9b, RTRIM(b9bSex) b9bSex, RTRIM(b9c) b9c, b9bReserved, b9cReserved, RTRIM(b9d) b9d,
	b10a, b10b, RTRIM(b10bPlace) b10bPlace, b10c, b10d,
	RTRIM(b11) b11, b11a, b11aSex, b11bReserved,
	RTRIM(b11b) b11b, RTRIM(b11c) b11c, b11d,
	b12, b12Date,
	b13,
	b14, b14Qual,
	b15, b15Qual,
	b16From, b16To,
	b17, RTRIM(b17FirstName) b17FirstName, RTRIM(b17LastName) b17LastName, RTRIM(b17MidName) b17MidName, RTRIM(b17a) b17a, b17b, b17Indicator,
	b18From, b18To,
	b19, b19Template, b19CPT,
	RTRIM(b20) b20, b20Charges,
	b21Indicator,
	RTRIM(b22Code) b22Code, RTRIM(b22No) b22No, b22FrequencyTypeCode, b22DelayReasonCode,
	RTRIM(b23) b23,
	RTRIM(b25) b25, b25Type,
	b26, b27, b28, b29,
	b30, b30Reserved,
	b31, b31Date,
	b32Name, b32Addr, b32Addr2, RTRIM(b32City) b32City, RTRIM(b32State) b32State, RTRIM(b32Zip) b32Zip, b32Phone, b32NPI, b32ID,
	b33Name, b33FirstName, b33LastName, b33MidName, b33Addr, b33Addr2, b33City, b33State, RTRIM(b33Zip) b33Zip, b33Phone, RTRIM(b33PIN) b33PIN, RTRIM(b33GRP) b33GRP, RTRIM(b33UPIN) b33UPIN, RTRIM(b33ProviderID) b33ProviderID, b33NPI, b33ID,
	b32ZipExt, b33ZipExt, b34ZipExt, b5ZipExt, b7ZipExt,
	b34Name, b34Addr, b34Addr2, RTRIM(b34Apt) b34Apt, b34City, b34State, RTRIM(b34Zip) b34Zip, b34Phone,
	ClaimDate, ClaimPrintDate, SendDate, PrintBy,
	UserClaimComments, EMC, OfficeLocation, EMCBatchNumber, BillingAddressID, C4form,
	b.LastModifier, b.LastModifyDate,
	DoctorNPI,
	RefDoctorID,
	COStatus,
	[Message],
	MSP,
	BillingValidation, RequestStatus, RequestStatusDate, RequestStatusBy,
	BillCreateDate, BillCreateBy,
	C4Type, FacilityID, FormVersion, ICDCodeSystem
FROM
	Billing_1500Form b WITH(NOLOCK)
	--LEFT JOIN Pati_InsuPlan h ON h.InsuPlanID = b.InsuPlanID
	LEFT JOIN Patient p WITH(NOLOCK) ON p.PatientID = b.PatientID
	LEFT JOIN Ref_Insucom i WITH(NOLOCK) ON i.InsuComID = b.TempInsuComID
	LEFT JOIN Employee e1 WITH(NOLOCK) ON e1.EmployeeID = b.DoctorID
	LEFT JOIN Employee e2 WITH(NOLOCK) ON e2.EmployeeID = b.PCPID
	LEFT JOIN Employee e3 WITH(NOLOCK) ON e3.EmployeeID = b.BillDoctorID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON b.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
	b.ClinicID <> 1007
GO
/****** Object:  View [dbo].[View_mongo_Billing1500FormCPT]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










CREATE VIEW [dbo].[View_mongo_Billing1500FormCPT]
AS
SELECT
	a.ClinicID,
	a.BillingID,
	CPTID,
	DisplayOrder,
	b24From, b24To,
	b24POS, RTRIM(b24TOS) b24TOS,
	RTRIM(b24CPT) b24CPT, RTRIM(b24CPTShow) b24CPTShow,
	RTRIM(b24Ma) b24Ma, RTRIM(b24Mb) b24Mb, RTRIM(b24Mc) b24Mc, RTRIM(b24Md) b24Md,
	RTRIM(b24ICD) b24ICD, b24Charge, b24PCharge, RTRIM(b24Units) b24Units,
	RTRIM(b24EPDST) b24EPDST, RTRIM(b24EMG) b24EMG, RTRIM(b24COB) b24COB, b24Reserved,
	b24fm, b24fh, b24tm, b24th,
	b24IDQual, b24ReservedS, b24ShadedArea,
	PCPTID,
	n14pos, n14desc
FROM
	Billing_1500Form_CPT a WITH(NOLOCK)
	JOIN Billing_1500Form b WITH(NOLOCK) ON a.BillingID = b.BillingID AND a.ClinicID = b.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  b.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = b.BillingID
--			                                                AND d.TrackingType = 'Billing'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)









GO
/****** Object:  View [dbo].[View_mongo_BillingAccount]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[View_mongo_BillingAccount]
AS
SELECT
	a.ClinicID,
	BillingID,
	PatientID,
	PatientName,
	PatientDOB,
	ClaimDate,
	ServiceDate,
	OVID VisitID,
	CDID, CDName, CDAbbr,
	PCPID, PCPName, PCPAbbr,
	BillBy, BillTo,
	InsuredID, InsuredName, InsuredRelationship,
	InsuComID, InsuComName, InsuComPhone, InsuType, InsuTypeName, InsuOrderName,
	FeeCharge, FeeApprove, FeePay, FeeCopay, FeeDeduct, FeeBalance,
	CPTNum,
	RTRIM(CheckNum) CheckNum,
	a.Status,
	PayDate, PayBy, WaiveReason, Memo,
	Modifier LastModifier, ModifyDate LastModifyDate,
	InputDate, Lock, EFTNum, SubStatus,
	FeeWithheld,
	FeeWriteOff,
	CopayID,
	CopayAmount,
	BillPatientDate,
	FeeInsuranceCopay,
	FeeEOBBalance,
	FeeClinicWriteOff,
	BillPatient,
	BillNextInsurance,
	FeePatientBalance,
	FeeBillNext,
	AcctSum,
	FeeInsuWriteOff,
	FeeOther,
	AcctCreateDate,
	CrossoverAccount,
	CopayAmount2,
	HasNextAcct,
	HasHandleBalance,
	FeeConversion,
	V12Account,
	AcctCreateBy,
	FeeAdjustment,
	FeeV12PaymentApplied,
	ApplyPayment,
	ICN
FROM
	Billing_Account a WITH(NOLOCK)
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.BillingID
--			                                                AND d.TrackingType = 'Billing'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)






GO
/****** Object:  View [dbo].[View_mongo_BillingAccountCPT]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[View_mongo_BillingAccountCPT]
AS
SELECT
	a.ClinicID,
	a.BillingID,
	CPTID,
	DisplayOrder, RTRIM(CPTCode) CPTCode, CPTName,
	CPTMod, CPTCharge, CPTApprove, CPTPay, CPTCopay, CPTDeduct, CPTInsuranceCopay,
	Withheld, WriteOff, CPTInsuWriteOff,
	CPTEOBBalance, CPTEOBNote,
	CPTClinicWriteOff, CPTPatientBalance, CPTBalance,
	CPTOther, CPTConversion, RCode, a.EFTNum, CPTNote, CPTBillNext
FROM
	Billing_Account_CPT a WITH(NOLOCK)
	JOIN Billing_Account b WITH(NOLOCK) ON a.BillingID = b.BillingID and a.ClinicID = b.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  b.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.BillingID
--			                                                AND d.TrackingType = 'Billing'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)







GO
/****** Object:  View [dbo].[View_mongo_BillingICD]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[View_mongo_BillingICD]
AS
SELECT
	a.ClinicID,
	a.BillingID,
	ICDNum, RTRIM(ICDCode) ICDCode, ICDName,
	Conditions, CodeSystem
FROM
	Billing_ICD a WITH(NOLOCK)
	JOIN Billing_1500Form b WITH(NOLOCK) ON a.BillingID = b.BillingID AND a.ClinicID = b.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  b.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.BillingID
--			                                                AND d.TrackingType = 'Billing'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)







GO
/****** Object:  View [dbo].[View_mongo_Chemotherapy_Medication]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[View_mongo_Chemotherapy_Medication]
AS
SELECT
	a.ClinicID, a.ChemID, PatientID, ICD, ICDName, ICDCodeSystem, a.[Type], Protocol, a.[Status],
	t.Drug, t.Dosage, t.Units, b.DrugAlias, PRN, BeginTime, EndTime, b.RxNorm
FROM
	On_Chemotherapy a WITH (NOLOCK)
	JOIN On_TreatmentPlan b WITH (NOLOCK) ON b.ChemID=a.ChemID AND b.RxNorm IS NOT NULL AND LEN(RxNorm)>0
	LEFT JOIN On_TreatmentPlanDetail t WITH (NOLOCK) ON t.PlanID=b.PlanID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.VisitID
--			                                                AND d.TrackingType = 'OV'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)





GO
/****** Object:  View [dbo].[View_mongo_clinic]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_clinic]
AS
SELECT
	ClinicID,
	[web_id] WebID,
	RTRIM(ClinicName) ClinicName,
	RTRIM(ClinicAddrStreet) AddressLine1,
	RTRIM(ClinicAddrApt) Apt,
	RTRIM(ClinicAddrCity) City,
	RTRIM(ClinicAddrState) [State],
	RTRIM(ClinicAddrZip) Zip,
	RTRIM(ClinicPhone) Phone,
	RTRIM(ClinicFax) Fax,
	RTRIM(ClinicEmail) Email,
	RTRIM(ClinicWebsite) Website,
	RTRIM(TaxID) TaxID,
	RTRIM(NPI) NPI,
	CASE ABCSEnable WHEN 1 THEN 'Y' ELSE 'N' END ABCSEnable,
	CASE ACPEnable WHEN 1 THEN 'Y' ELSE 'N' END ACPEnable,
	CASE HealthixEnable WHEN 1 THEN 'Y' ELSE 'N' END HealthixEnable,
	ClinicCreateDate,
	ContractDate,
	CASE ReadOnlyVersion WHEN 1 THEN 'Y' ELSE 'N' END [ReadOnly],
	CASE [Status] WHEN 1 THEN 'I' ELSE 'A' END [Status]
FROM
	Clinic c
	LEFT JOIN db_identity d ON 1 = 1
WHERE
	c.ClinicID <> 1007
	AND
	c.ClinicID > 0
	--AND ISNULL(c.[Status], 0) = 0
GO
/****** Object:  View [dbo].[View_mongo_ClinicEMR]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_ClinicEMR]
AS
SELECT
	a.ClinicID,
	a.EMRMode ID,
	[description] Name
FROM
	Clinic_EMRMode a
	JOIN Ref_EMRMode b ON a.EMRMode = b.Mode
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
	a.ClinicID <> 1007
	AND a.ClinicID > 0




GO
/****** Object:  View [dbo].[View_mongo_complaint]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- Chief Complaint
CREATE View [dbo].[View_mongo_complaint]
AS
SELECT
	a.ClinicID, a.VisitID, ChiefComplaint, PresentIllness
FROM
	Medi_OV_Complaint a
	JOIN Medi_OfficeVisit v ON a.VisitID = v.VisitID AND a.ClinicID = v.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  v.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.VisitID
--			                                                AND d.TrackingType = 'OV'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)




GO
/****** Object:  View [dbo].[View_mongo_ConsultationLetter]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_ConsultationLetter]
AS
SELECT
	CID,
	ClinicID,
	PatientID,
	VisitID,
	CONVERT(CHAR(10),PatientID)+'-'+CONVERT(CHAR(4),ClinicID) PatientUID,
	CONVERT(CHAR(10),VisitID)+'-'+CONVERT(CHAR(4),ClinicID) VisitUID,
	FromDoctorID,
	ToDoctorID,
	OfficeLocation,
	SendDate,
	FollowupDate,
	AssignTo,
	[Status],
	CASE [Status] WHEN 0 THEN 'New' WHEN 1 THEN 'Completed' WHEN 2 THEN 'Canceled' WHEN 3 THEN 'Sent' ELSE NULL END StatusName,
	CASE AttachPatientTOC WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END IsAttachPatientTOC,
	CreateBy,
	CreateDate,
	LastModifyBy ModifyBy,
	LastModifyDate ModifyDate,
	CASE WHEN b.PECID IS NOT NULL THEN 'Y' ELSE 'N' END Exchanged
FROM
	Medi_OV_Consultation a WITH (NOLOCK)
	OUTER APPLY (SELECT TOP 1 PECID FROM PhoneEncounter e WHERE a.ClinicID = e.ClinicID AND a.CID = e.LinkID AND e.EncounterType IN (1024, 1060)) b
GO
/****** Object:  View [dbo].[View_mongo_cptlist]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- CPT List
CREATE VIEW [dbo].[View_mongo_cptlist]
AS
SELECT
  a.ClinicID,
  a.VisitID,
  CPTID PKID,
  CASE WHEN CPT IS NOT NULL AND LEN(CPT) > 5 THEN SUBSTRING(CPT, 1, 5) ELSE CPT END Code,
  RTRIM(CPTName) Name,
  RTRIM(Units) Units,
  RTRIM(CPTMa) CPTMa,
  RTRIM(CPTMb) CPTMb,
  RTRIM(CPTMc) CPTMc,
  RTRIM(CPTMd) CPTMd,
  RTRIM(ICDLink) ICDLink--,
  --RTRIM(ManualLink) ManualLink,
  --CreateDate,
  --CreateBy,
  --ModifyDate,
  --ModifyBy
FROM
      Medi_OV_CPTList a WITH(NOLOCK)
	  JOIN Medi_OfficeVisit v WITH(NOLOCK) ON v.VisitID = a.VisitID AND v.ClinicID = a.ClinicID
	  LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	  JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  v.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = v.VisitID
--			                                                AND d.TrackingType = 'OV'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)







GO
/****** Object:  View [dbo].[View_mongo_DiabetesManagement]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_DiabetesManagement]
AS
SELECT
  a.ClinicID,
  a.VisitID,
  CASE EyeExam WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END EyeExam,
  EyeExamDate,
  CASE MBTest WHEN 1 THEN 'Positive' WHEN 0 THEN 'Negative' ELSE NULL END MBTest,
  MBTestDate,
  CASE WHEN FootExam=1 THEN 'Y' ELSE 'N' END FootExam,
  FootExamDate,
  CASE DLA WHEN 1 THEN 'Y' WHEN '0' THEN 'N' ELSE NULL END DLA,
  DLADate,
  Reference,
  CASE Retinopathy WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END Retinopathy,
  CASE GestationalDiabetes WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END GestationalDiabetes,
  GestationalDiabetesCode,
  DateofLastEyeExam,
  DateofLastEyeExamCode,
  DateofLastSensoryExamofFoot,
  DateofLastSensoryExamofFootCode,
  DateofLastVisualExamofFoot,
  DateofLastVisualExamofFootCode,
  DateofLastPulseExamofFoot,
  DateofLastPulseExamofFootCode,
  CASE BilateralAmputee WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END BilateralAmputee,
  BilateralAmputeeCode,
  CASE LeftFootAmputee WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END LeftFootAmputee,
  LeftFootAmputeeCode,
  CASE RightFootAmputee WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END RightFootAmputee,
  RightFootAmputeeCode,
  CASE DateofLastEyeExamResult WHEN 1 THEN 'Positive' WHEN 0 THEN 'Negative' ELSE NULL END DateofLastEyeExamResult
FROM
      Medi_OV_DiabetesManagement a WITH(NOLOCK)
	  JOIN Medi_OfficeVisit v WITH(NOLOCK) ON v.VisitID = a.VisitID AND v.ClinicID = a.ClinicID
	  LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	  JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  v.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = v.VisitID
--			                                                AND d.TrackingType = 'OV'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)




GO
/****** Object:  View [dbo].[View_mongo_DrugMedication]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_DrugMedication]
AS

SELECT
	a.ClinicID,
	a.PatientID,
	a.MedicationID,
	a.NDC,
	--GPI,
	CASE WHEN LEN(dl.GPI) > 0 THEN dl.GPI ELSE a.GPI END GPI,
	RxNorm,
	Print_Name,
	Generic_Name,
	Manufacture,
	BeginDate,
	EndDate,
	a.Active,
	a.Strength,
	Units,
	a.DosageForm,
	[Route],
	[Take],
	Quantity,
	Qualifier,
	--RTRIM(scheduleLevel) scheduleLevel,
	CASE
		WHEN LEN(dl.scheduleLevel) > 0 THEN dl.scheduleLevel
		WHEN dl.scheduleLevel = '' THEN '0'
		ELSE CASE WHEN RTRIM(a.scheduleLevel) = '' THEN '0' WHEN a.scheduleLevel IS NULL THEN '0' ELSE RTRIM(a.scheduleLevel) END
	END scheduleLevel,
	Duration,
	Refill,
	CASE DAW WHEN 1 THEN 'Y' ELSE 'N' END DAW,
	SIG,
	CASE sente WHEN 1 THEN 'Y' ELSE 'N' END sente,
	CASE SendeFaxed WHEN 1 THEN 'Y' ELSE 'N' END SendeFaxed,
	CASE Printed WHEN 1 THEN 'Y' ELSE 'N' END Printed,
	CASE Dispensed WHEN 1 THEN 'Y' ELSE 'N' END Dispensed,
	MyDrugID,
	Note NoteToPharmacist,
	InternalNote,
	DoctorID,
	CreateBy,
	CreateDate,
	ModifyBy,
	ModifyDate,
	ReconcileDate,
	ReconcileBy,
	PrescribeDate,
	PrescriberID,
	PrescriberName,
	RefillDate,
	SideEffects,
	CASE Adherence WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' WHEN 2 THEN 'New Medication' ELSE NULL END Adherence,
	DiscontinueReason,
	AsReq,
	Purpose,
	IntoleranceCode
FROM
	Drug_Medication a WITH (NOLOCK)
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0 and a.[Status] = 0
	LEFT JOIN [192.168.168.47].Medispan.dbo.DrugList dl ON a.NDC = dl.NDC
WHERE
	a.[Status] = 0
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_DrugMedication_Refill]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_DrugMedication_Refill]
AS
SELECT
	a.PKID,
	a.MedicationID,
	a.RefillDate,
	a.RefillBy,
	--a.Dispensed,
	--a.Duration,
	--a.Refill,
	d.ClinicID,
	d.PatientID
FROM
	Drug_Medication_Refill a
	JOIN Drug_Medication d ON a.MedicationID = d.MedicationID
GO
/****** Object:  View [dbo].[View_mongo_EducationRecord]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_EducationRecord]
AS
SELECT
	a.ClinicID, PatientID,
	a.EduID, EduName, EduURL, ICD, CPT, CreateTime
FROM
	Medi_Education_Record a
	JOIN Ref_Education b ON a.EduID = b.EduID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_AlcoholDrugIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_AlcoholDrugIntervention]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedBack ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	FollowUp,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	ReferredFollowUp,
	FollowUpPlan,
	ScreenToolLinkID,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate
FROM
	Form_AlcoholDrugIntervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_AlcoholDrugScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_AlcoholDrugScreening]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	C1, C2, C3, C4, C5, C6, C7, C8, C9, C10,
	TotalScore,
	a.[Status],
	VisitDate
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_AlcoholDrugScreening a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_AlcoholIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_AlcoholIntervention]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedback ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	FollowUp,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	ReferredFollowUp,
	SpecialtyFollowUp,
	FollowUpPlan,
	ScreenToolLinkID,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate,
	f.CreateBy,
	f.ModifyDate,
	f.ModifyBy
FROM
	Form_AlcoholIntervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_AlcoholScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_AlcoholScreening]
AS
SELECT
	a.ClinicID,
	a.PatientID,
	s1, s2, s3, s4, s5, s6, s7, s8, s9, s10,
	s11_1_1, s11_1_2, s11_1_3, s11_1_4, s11_1_5,
	s11_2_1, s11_2_2, s11_2_3, s11_2_4, s11_2_5, s11_2_6,
	s11_2_Scale, s11_2_dateValue, s11_2_dateType,
	s11_3_1, s11_3_2, s11_3_3, s11_3_4, s11_3_5, s11_3_6, s11_3_7, s11_3_8
	s11_3_Scale, s11_3_dateType, s11_3_dateValue, s11_3_minutes, s11_3_referred
	s11_4_1, s11_4_2, s11_4_3, s11_4_4, s11_4_5, s11_4_6, s11_4_7, s11_4_8, s11_4_9, s11_4_10, s11_4_11
	s11_4_Scale, s11_4_dateType, s11_4_dateValue, s11_4_minutes, s11_4_referred
	s12,
	NoteComment,
	VisitDate,
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy,
	a.[Status]
FROM
	Form_AlcoholScreening a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_BMI_Intervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_BMI_Intervention]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	ReferredFollowUP,
	SpecialtyFollowUp,
	ScreenToolLinkID,
	FollowUp,
	FollowUpPlan,
	FollowupPlanCode FollowUpPlanCode,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate
FROM
	Form_BMI_Intervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_ColonoscopyScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_ColonoscopyScreening]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	ScreeningDate,
	OrderingDoctor,
	Phone,
	Note,
	a.[Status],
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_ColonoscopyScreening a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_DepressionIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_DepressionIntervention]
AS

SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	FollowUp,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	ReferredFollowUp,
	SpecialtyFollowUp,
	FollowUpPlan,
	FollowupPlanCode FollowUpPlanCode,
	ScreenToolLinkID,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate
FROM
	Form_DepressionIntervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND f.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  f.PatientID = d.PatientID
--			                                                AND f.ClinicID = d.ClinicID
--			                                                AND d.TableID = f.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_DepressionScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_DepressionScreening]
AS

SELECT
  f.ClinicID,
  f.PatientID,
  f.VisitID,
  CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
  CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
  f.VisitDate,
  f.ReportCode,
  f.ScoreStatus,
  f.TotalScore,
  f.SelfScore,
  f.LoincCode,
  ov.CDID CoverByID,
  f.CreateDate,
  f.CreateBy,
  f.ModifyDate,
  f.ModifyBy
FROM   Form_DepressionScreening f
       LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	   LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
       JOIN Clinic ON f.ClinicID = Clinic.ClinicID AND Isnull(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_DrugDast]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_DrugDast]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10,
	Q11, Q11_Note, Q11_Scale, Q11_ReturnIn,	Q11_Referred,
	Q12,
	DastTotalScore,
	IsDefault,
	a.[Status],
	VisitDate,
	CreateDate,
	CreateBy
FROM
	Form_DrugDast a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_DrugIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_DrugIntervention]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	FollowUp,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	ReferredFollowUP,
	SpecialtyFollowUp,
	FollowUpPlan,
	ScreenToolLinkID,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate
FROM
	Form_DrugIntervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_FallRiskIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_FallRiskIntervention]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	PatientRreadiness PatientReadiness,
	AssistiveDevice,
	ReferredFollowUp,
	SpecialtyFollowUp,
	ScreenToolLinkID,
	a.[Status],
	VisitDate,
	CreateDate
FROM
	Form_FallRiskIntervention a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_FaxToQuit]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_FaxToQuit]
AS
SELECT
	ID,
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	f.VisitDate,
	PatientFirstName,
	PatientLastName,
	DOB,
	PatientGender,
	PatientPrimaryPhone,
	PatientSecondPhone,
	PatientAddr,
	PatientCity,
	PatientState,
	PatientZip,
	PatientEmail,
	ShouldWeCall,
	LeaveMessage,
	PatientLanguage,
	PatientOtherLanguage,
	RefDocName,
	RefDocPhone,
	RefDocFacility,
	RefDocFax,
	RefDocAddr,
	RefDocCity,
	RefDocState,
	RefDocZip,
	RefDocEmail,
	RefDocProcess,
	RefDocSame,
	ReportToDocName,
	ReportToDocPhone,
	ReportToDocFacility,
	ReportToDocFax,
	ReportToDocAddr,
	ReportToDocCity,
	ReportToDocState,
	ReportToEmail,
	RelToChild,
	RelToChildOther,
	ChildName,
	ov.CDID CoverByID,
	f.CreateDate,
	f.CreateBy,
	f.ModifyDate,
	f.ModifyBy
FROM
	Form_FaxToQuit f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
	f.[Status] = 1
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_MammogramScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_MammogramScreening]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	ScreeningDate,
	OrderingDoctor,
	Result,
	Note,
	a.[Status],
	Phone,
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_MammogramScreening a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_MiniMHExam]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_MiniMHExam]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11,
	sLevel,
	sEnter,
	a.[Status],
	VisitDate,
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_MiniMHExam a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_PAPSmearScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_PAPSmearScreening]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	ScreeningDate,
	OrderingDoctor,
	Result,
	Note,
	Phone,
	a.[Status],
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_PAPSmearScreening a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_PHQ9TeenIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_PHQ9TeenIntervention]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	FollowUp,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	ReferredFollowUp,
	SpecialtyFollowUp,
	FollowUpPlan,
	FollowUpPlanCode FollowUpPlanCode,
	ScreenToolLinkID,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate
FROM
	Form_PHQ9TeenIntervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_PHQ9TeenScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_PHQ9TeenScreening]
AS

SELECT
  f.ClinicID,
  f.PatientID,
  f.VisitID,
  CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
  CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
  f.VisitDate,
  f.ScoreStatus,
  f.TotalScore,
  ov.CDID CoverByID,
  f.CreateDate,
  f.CreateBy,
  f.ModifyDate,
  f.ModifyBy
FROM   Form_PHQ9TeenScreening f
       LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	   LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
       JOIN Clinic ON f.ClinicID = Clinic.ClinicID AND Isnull(Clinic.Status, 0) = 0
WHERE
    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_PostpartumTool]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_PostpartumTool]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	CareSnomedCode,
	ScreeningSnomedCode,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate,
	f.CreateBy,
	f.ModifyDate,
	f.ModifyBy
FROM
	OB_Forms_PostpartumTool f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--    f.Status = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_PSAScreening]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_PSAScreening]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	ScreeningDate,
	OrderingDoctor,
	Result,
	Note,
	Phone,
	a.[Status],
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_PSAScreening a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_Screen]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_Screen]
AS

SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	QA,
	FormID,
	TotalScore,
	ScreeningFormCode,
	FileID,
	SignGUID,
	NeedConfirm,
	FormStatus,
	CASE
		WHEN FormID=311 AND QA LIKE '%<IsSmoke>%</IsSmoke>%' THEN
			CASE WHEN QA LIKE '%<IsSmoke>Yes</IsSmoke>%' THEN '77176002' ELSE '8392000' END
		ELSE NULL END
	SubCode,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate,
	f.CreateBy,
	f.LastModifyTime ModifyDate,
	f.LastModifyBy ModifyBy
FROM
	Form_Screen f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
    f.IsDeleted = 0
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_SmokingTobaccoIntervention]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_SmokingTobaccoIntervention]
AS
SELECT
	ID,
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	ResultFeedback,
	EducationOnRisk,
	Recommendations,
	Referral,
	PatientRreadiness PatientReadiness,
	ReturnTimes,
	ReturnType,
	SpecialtyFollowUp,
	CessationCounselingCode,
	ScreenToolLinkID,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate
FROM
	Form_SmokingTobaccoIntervention f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
	f.[Status] = 1
--	AND a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_Form_StandardAssessment]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_Form_StandardAssessment]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	a.PatientID,
	S1, S2, S3, S4, S5, S6, S7, S8, S9, S10,
	DentalStatus1,
	DentalStatus2,
	AssessmentDate,
	a.[Status],
	VisitDate,
	CreateDate,
	CreateBy,
	ModifyDate,
	ModifyBy
FROM
	Form_StandardAssessment a
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)



GO
/****** Object:  View [dbo].[View_mongo_Form_SuicideRiskAssessment]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Form_SuicideRiskAssessment]
AS
SELECT
	f.ClinicID,
	f.PatientID,
	f.VisitID,
	CONVERT(VARCHAR(10), f.PatientID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) PatientUID,
    CONVERT(VARCHAR(10), f.VisitID)+'-'+CONVERT(VARCHAR(4), f.ClinicID) VisitUID,
	f.ReportCode,
	ov.CDID CoverByID,
	f.VisitDate,
	f.CreateDate,
	f.CreateBy,
	f.ModifyDate,
	f.ModifyBy
FROM
	Form_SuicideRiskAssessment f
	LEFT JOIN Medi_OfficeVisit ov ON f.VisitID = ov.VisitID AND f.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)
GO
/****** Object:  View [dbo].[View_mongo_HospitalAdmission]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[View_mongo_HospitalAdmission]
AS
SELECT
	a.ID,
    a.ClinicID,
	a.PatientID,
	DateIn,
	DateOut,
	HospitalName,
	InpatientEncounter,
	Illness,
	DIllness,
	SurgicalProcedure,
	DoctorName,
	SurgicalDoctor
FROM
	Pati_HospitalAdmission a WITH (NOLOCK)
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)






GO
/****** Object:  View [dbo].[View_mongo_HospitalAdmissionICD]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE View [dbo].[View_mongo_HospitalAdmissionICD]
AS
SELECT
	a.ClinicID,
	a.PatientID,
	b.HAID,
	b.[Type],
	RTRIM(b.ICDCode) ICDCode,
	b.ICDName,
	b.CodeSystem
FROM
	Pati_HospitalAdmission a WITH (NOLOCK)
	LEFT JOIN Pati_HospitalAdmission_ICD b WITH (NOLOCK) ON b.HAID = a.ID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  a.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = a.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)





GO
/****** Object:  View [dbo].[View_mongo_icdlist]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- ICD List
CREATE VIEW [dbo].[View_mongo_icdlist]
AS
SELECT
	a.ClinicID,
	a.VisitID,
	ICDID PKID,
	ICD Code,
	ICDName Name,
	CodeSystem
FROM
	Medi_OV_ICDList a WITH(NOLOCK)
	JOIN Medi_OfficeVisit v WITH(NOLOCK) ON v.VisitID = a.VisitID AND v.ClinicID = a.ClinicID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic WITH(NOLOCK) ON a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	a.ClinicID <> 1007 AND
--	 ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  v.PatientID = d.PatientID
--			                                                AND a.ClinicID = d.ClinicID
--			                                                AND d.TableID = v.VisitID
--			                                                AND d.TrackingType = 'OV'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)







GO
/****** Object:  View [dbo].[View_mongo_Lab]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Lab]
AS
SELECT
	inbox.ReceiverClinicID ClinicID,
	Clinic.ClinicName,
	inbox.PatientID,
	p.MPatientID MPID,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	'HL7' RecordType,
	inbox.InboxID ReferenceTableID,
	NULL VisitID,
	NULL VisitDate,
	--obr.ObsvDateTime CollectionDate,
	--obr.SpecimenRecvTime ReceiveDate,
	--obr.ReportDateTime ReportDate,
	NULL OrderDate,
	inbox.ServiceDate,
	inbox.[Subject] LabCompany,
	inbox.CheckInDate,
	RTRIM(e.EmployeeLastname) + ', ' + RTRIM(e.EmployeeFirstName) CheckInBy,
	NULL OrderBy,
	inbox.[Status],
	LinkID,
	AttachPatientTOC,
	SentOutLetter
	--,doc.PicType CategoryID
FROM
	LabInbox inbox WITH(NOLOCK)
	LEFT JOIN Patient p WITH(NOLOCK) ON inbox.PatientID = p.PatientID AND inbox.ReceiverClinicID = p.ClinicID
	LEFT JOIN Employee e WITH(NOLOCK) ON inbox.CheckinEmployeeID = e.EmployeeID
	--CROSS APPLY (SELECT TOP 1 * FROM LabOBR obr1 WHERE obr1.ClinicID = inbox.ReceiverClinicID AND CONVERT(VARCHAR(50), obr1.MsgID) = CONVERT(VARCHAR(50), inbox.LinkID)) obr
	--CROSS APPLY (SELECT TOP 1 * FROM Pati_DiagPic doc1 WHERE  CONVERT(VARCHAR(50), doc1.PicSize) = CONVERT(VARCHAR(50), inbox.LinkID)) doc
	JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	LEFT JOIN Pati_Daily_Tracking pdt WITH(NOLOCK) ON  inbox.PatientID = pdt.PatientID and inbox.ReceiverClinicID = pdt.ClinicID and pdt.TrackingType='Inbox' and pdt.TableID = inbox.InboxID
	JOIN Clinic WITH(NOLOCK) ON inbox.ReceiverClinicID = Clinic.ClinicID AND ISNULL(Clinic.Status, 0) = 0
WHERE
	--p.ClinicID = 1007 AND
	(CONVERT(INT, inbox.ServiceType) BETWEEN 51 AND 100 OR CONVERT(INT, inbox.ServiceType) BETWEEN 10000 AND 20000)
	AND inbox.[Status] <> 'CA'
	AND (  c.LastSyncDate IS NULL OR (c.LastSyncDate IS NOT NULL AND pdt.RowID is not null ))
UNION
SELECT
	inbox.ReceiverClinicID ClinicID,
	Clinic.ClinicName,
	inbox.PatientID,
	p.MPatientID MPID,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	'ALT' RecordType,
	inbox.InboxID ReferenceTableID,
	NULL VisitID,
	NULL VisitDate,
	NULL OrderDate,
	inbox.ServiceDate,
	inbox.[Subject] LabCompany,
	inbox.CheckInDate,
	RTRIM(e.EmployeeLastname) + ', ' + RTRIM(e.EmployeeFirstName) CheckInBy,
	NULL OrderBy,
	inbox.[Status],
	LinkID,
	AttachPatientTOC,
	SentOutLetter
FROM
	LabInbox inbox WITH(NOLOCK)
	LEFT JOIN Patient p WITH(NOLOCK) ON inbox.PatientID = p.PatientID AND inbox.ReceiverClinicID = p.ClinicID
	LEFT JOIN Employee e WITH(NOLOCK) ON inbox.CheckinEmployeeID = e.EmployeeID
	JOIN Clinic WITH(NOLOCK) ON inbox.ReceiverClinicID = Clinic.ClinicID AND ISNULL(Clinic.Status, 0) = 0
WHERE
	inbox.ServiceType = '10047'
	AND inbox.[Status] <> 'CA'
	AND DATEDIFF(dd, inbox.CreateDate, GETDATE()) BETWEEN 0 AND 3
GO
/****** Object:  View [dbo].[View_mongo_LabInbox]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[View_mongo_LabInbox]
AS
SELECT
	1 LabType,
	inbox.InboxID OrderID,
	inbox.ReceiverClinicID ClinicID,
	inbox.PatientID,
	p.MPatientID MPI,
	inbox.[Status],
	inbox.CheckInDate,
	RTRIM(e.EmployeeLastname) + ', ' + RTRIM(e.EmployeeFirstName) CheckInBy,
	inbox.InboxMemo Memo,
	inbox.ServiceDate,
	inbox.ServiceType LabIncID,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	inbox.[Subject] LabIncName,
	LinkID
FROM
	LabInbox inbox
	JOIN Patient p ON inbox.PatientID = p.PatientID AND inbox.ReceiverClinicID = p.ClinicID
	JOIN Employee e ON inbox.CheckinEmployeeID = e.EmployeeID
	left join (select dateadd(dd,-2,LastSyncDate)LastSyncDate from db_identity) c on 1 = 1
	JOIN Clinic ON inbox.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	left join Pati_Daily_Tracking pdt on inbox.PatientID = pdt.PatientID and inbox.ReceiverClinicID = pdt.ClinicID and inbox.InboxID = pdt.TableID and pdt.TrackingType='Inbox'
WHERE
	p.ClinicID <> 1007
	AND ( CONVERT(INT, inbox.ServiceType) BETWEEN 51 AND 100
	      OR CONVERT(INT, inbox.ServiceType) BETWEEN 10000 AND 20000 )
	AND inbox.[Status] <> 'CA' AND (  c.LastSyncDate is null or (c.LastSyncDate is not null and pdt.RowID is not null ))






GO
/****** Object:  View [dbo].[View_mongo_LabInbox_Single]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_mongo_LabInbox_Single]
AS
SELECT
	a.InboxID,
	a.ReceiverID,
	a.ReceiverClinicID,
	a.PatientID,
	a.PatientName,
	a.SenderID,
	a.SenderClinicID,
	a.SenderName,
	a.LinkID,
	a.[Subject],
	a.ServiceDate,
	a.ServiceType,
	a.[Status],
	a.CreateDate,
	a.CreateBy,
	a.LastModifyDate,
	a.LastModifyBy,
	a.ProcessStatus,
	a.HandleStatus,
	a.LabStatus,
	a.[Private],
	a.ClinicID,
	a.CheckInEmployeeID,
	a.CheckInDate,
	a.UrgentLevel,
	a.InboxMemo
FROM
	LabInbox a with(nolock)
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
	Left join Pati_Daily_Tracking pdt on a.PatientID = pdt.PatientID and a.ReceiverClinicID = pdt.ClinicID and pdt.TrackingType='Inbox' and pdt.TableID = a.InboxID
WHERE
	 ( c.LastSyncDate IS NULL OR ( c.LastSyncDate IS NOT NULL AND pdt.RowID is not null ))

GO
/****** Object:  View [dbo].[View_mongo_LabNTE_Single]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_LabNTE_Single]
AS
SELECT
	MsgID,
	OBRSetID,
	OBXSetID,
	NTESetID,
	Source,
	Comment,
	b.ClinicID
FROM
	LabNTE b
	JOIN LabInbox a ON a.LinkID = b.MsgID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0




GO
/****** Object:  View [dbo].[View_mongo_LabOBR_Single]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_mongo_LabOBR_Single]
AS
SELECT
	MsgID,
	OBRSetID,
	PlacerOrderNum,
	FillerOrderNum,
	ResultsGroup,
	GroupName,
	SubgroupName,
	[Priority],
	ReqDateTime,
	ObsvDateTime,
	ObsvEndDateTime,
	CollectionVol,
	CollectorID,
	SpecimenActionCode,
	RelevantClinicInfo,
	SpecimenRecvTime,
	SpecimenSource,
	PrvdClientID,
	PrvdName,
	PrvdAddr1,
	PrvdAddr2,
	PrvdCityState,
	OrderCallbackPhoneNum,
	PlacerField1,
	PlacerField2,
	FillerField1,
	FillerField2,
	ReportDateTime,
	ChargeToPractice,
	DiagServSectID,
	ResultStatus,
	RecvDateTime,
	ObBeginDateTime,
	ObEndDateTime,
	b.ClinicID
FROM
	LabOBR b
	JOIN LabInbox a ON a.LinkID = b.MsgID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0





GO
/****** Object:  View [dbo].[View_mongo_LabOBX]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_LabOBX]
AS
SELECT
	1 LabType,
	Inbox.InboxID,
	Inbox.ReceiverClinicID ClinicID,
	obx.OBRSetID,
	obx.OBXSetID,
	obr.FillerOrderNum,
	obr.ResultsGroup,
	obr.GroupName,
	obr.SubgroupName,
	obr.[Priority],
	obr.ReqDateTime,
	obr.ObsvDateTime,
	obr.CollectionVol,
	obr.CollectorId,
	obr.SpecimenActionCode,
	obr.DangerCode,
	obr.RelevantClinicInfo,
	obr.SpecimenRecvTime,
	obr.SpecimenSource,
	obr.PrvdClientId,
	obr.PrvdName,
	obr.ReportDateTime,
	obr.ResultStatus ORBStatus,
	CASE orc.OrderStatus WHEN 'CM' THEN 'Final' WHEN 'IP' THEN 'Partial' WHEN 'A' THEN 'Addendum' ELSE OrderStatus END ORCStatus,
	obx.ValueType,
	obx.TestNum,
	obx.TestName,
	CASE WHEN ISNUMERIC(obx.[Value]) = 1 THEN obx.[Value] WHEN CHARINDEX('|',obx.[Value]) > 0 THEN SUBSTRING(obx.[Value],0,CHARINDEX('|',obx.[Value])) ELSE '' END [Value],
	obx.Units,
	obx.ReferenceRange,
	obx.AbnormalFlag,
	obx.Probability,
	obx.ResultStatus OBXStatus,
	CASE obx.LOINCNum WHEN NULL THEN lrc.LonicCode WHEN '' THEN lrc.LonicCode ELSE LTRIM(obx.LOINCNum) END AS LOINCNum,
	Inbox.PatientID,
	Inbox.ServiceDate ServiceDate
FROM
	LabInbox Inbox
	JOIN LabOBR   obr      ON Inbox.LinkID = CONVERT(NVARCHAR(50), obr.MsgId)
	JOIN LabOBX   obx      ON obr.MsgId = obx.MsgId AND obr.OBRSetId = obx.OBRSetId
	JOIN Patient  p        ON Inbox.ReceiverClinicID = p.ClinicID AND Inbox.PatientID = p.PatientID
	LEFT JOIN LabORC orc   ON [orc].MsgID = CASE WHEN ISNUMERIC(Inbox.LinkID)=1 THEN Inbox.LinkID ELSE NULL END
	LEFT JOIN Lab_LoincResultCode lrc ON lrc.ServiceType = Inbox.ServiceType AND obx.TestNum = lrc.ResultCode
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON Inbox.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
	--p.ClinicID <> 1007 AND
	 ( Inbox.ServiceType BETWEEN 51 AND 100 OR Inbox.ServiceType BETWEEN 10000 AND 20000 )
	AND Inbox.[Status] <> 'CA'
	--AND ( c.LastSyncDate IS NULL
	--      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
	--			                                     WHERE  Inbox.PatientID = d.PatientID
	--		                                                AND Inbox.ClinicID = d.ClinicID
	--		                                                AND d.TableID = Inbox.PatientID
	--		                                                AND d.TrackingType = 'Patient'
	--		                                                AND d.TrackDate >= c.LastSyncDate) )
	--)




GO
/****** Object:  View [dbo].[View_mongo_LabOBX_Single]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_LabOBX_Single]
AS
SELECT
 b.MsgID,
 b.OBRSetID,
 OBXSetID,
 ValueType,
 TestNum,
 RTRIM(TestName) TestName,
 UPPER(RTRIM(TestName)) UpperTestName,
 SubID,
 CASE WHEN CHARINDEX('||', Value) > 0 THEN SUBSTRING(Value, 1, CHARINDEX('||', Value)-1) ELSE Value END Value,
 Units,
 ReferenceRange,
 AbnormalFlag,
 Probability,
 NatureOfAbnormalTest,
 b.ResultStatus,
 CASE
	WHEN LOINCNum IS NOT NULL AND LEN(RTRIM(LOINCNum)) > 0 THEN RTRIM(LOINCNum)
	WHEN RTRIM(TestName) IN ('HEMOGLOBIN A1C.', 'HEMOGLOBIN A1C (GLYCOHGB)', 'HEMOGLOBIN A1C', 'HB A1C') THEN '4548-4'
	WHEN RTRIM(TestName) LIKE '%HEMOGLOBIN%' AND RTRIM(TestName) LIKE '%A1C%' AND (LOINCNum IS NULL OR LEN(RTRIM(LOINCNum)) = 0) THEN '4548-4'
	WHEN RTRIM(TestName) LIKE '%HbA1c%' AND (LOINCNum IS NULL OR LEN(RTRIM(LOINCNum)) = 0) THEN '4548-4'
	ELSE RTRIM(LOINCNum)
	END LOINCNum,  -- Added on 8/14/2018
 b.ClinicID,
 LabSite,
 obr.ObsvDateTime CollectionDate,
 obr.SpecimenRecvTime ReceiveDate,
 obr.ReportDateTime ReportDate
FROM
 LABOBX b
 JOIN LabInbox a WITH(NOLOCK) ON CONVERT(VARCHAR(50),a.LinkID) = CONVERT(VARCHAR(50), b.MsgID)
 JOIN LABOBR obr WITH(NOLOCK) ON b.MsgID = obr.MsgID AND b.OBRSetId = obr.OBRSetId
 LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
 JOIN Clinic WITH(NOLOCK) ON a.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
GO
/****** Object:  View [dbo].[View_mongo_LabORC_Single]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_LabORC_Single]
AS
SELECT
	MsgID,
	SetID,
	OrderControl,
	PlacerOrderNum,
	FillerOrderNum,
	OrderStatus,
	TrnDateTime,
	EnteredBy,
	OrderingProvCode,
	OrderingProvLastName,
	OrderingProvFirstName,
	OrderingClinicCode,
	OrderingClinicName,
	b.ClinicID
FROM
	LabORC b
	JOIN LabInbox a ON a.LinkID = b.MsgID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0




GO
/****** Object:  View [dbo].[View_mongo_LabOrder]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE view [dbo].[View_mongo_LabOrder]
as
select
	2 LabType,
	o.OrderID,
	o.ClinicID,
	o.DoctorID,
	o.PatientID,
	o.VisitID,
	o.OrderDate,
	o.ReportDate,
	o.Status,
	o.CheckinDate,
	o.CheckinBy,
	o.Memo,
	o.VisitDate,
	o.ScheduleDate,
	o.ServiceDate,
	o.LabIncID,
	o.OrderStatus,
	o.CollectionDate,
	o.MemoClinic,
	o.OLID,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	labinc.LabIncName,
	RTRIM(e.EmployeeLastName)+' '+RTRIM(e.EmployeeFirstName) OrderDoctorName,
	e.EmployeeNPI OrderDoctorNPI,
	o.ReportDate DOS
from
	LabOrder_Order o
	join Patient        p on o.PatientID = p.PatientID and o.ClinicID = p.ClinicID
	left join Employee  e on o.DoctorID = e.EmployeeID and o.ClinicID = e.ClinicID
	left join LabOrder_LabInc labinc on o.LabIncID = labinc.LabIncID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON o.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--where
--	o.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  o.PatientID = d.PatientID
--			                                                AND o.ClinicID = d.ClinicID
--			                                                AND d.TableID = o.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)






GO
/****** Object:  View [dbo].[View_mongo_LabOrder_OrderLab]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE view [dbo].[View_mongo_LabOrder_OrderLab]
as
select
	2 OrderType,a.OrderID,a.LabID,a.LabName,Result,Abnormal,[Range],Unit,Description,LoincCode,LabCode,Note,
	b.PatientID,b.ClinicID,b.VisitDate DOS
from
	LabOrder_OrderLab a
	join LabOrder_Order    b on a.OrderID = b.OrderID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON b.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--where
--	b.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  b.PatientID = d.PatientID
--			                                                AND b.ClinicID = d.ClinicID
--			                                                AND d.TableID = b.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)





GO
/****** Object:  View [dbo].[View_mongo_LabPID_Single]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_mongo_LabPID_Single]
AS
SELECT
	MsgID,
	SetID,
	MatchedPID,
	ExternPID,
	InternalPID,
	AltPID,
	LastName,
	FirstName,
	MidName,
	MomMaidenName,
	BirthDate,
	Gender,
	PatientAlias,
	Race,
	Addr1,
	Addr2,
	City,
	b.[State],
	ZipCode,
	ZipCodeExt,
	CountryCode,
	HomePhoneNum,
	WorkPhoneNum,
	PrimLanguage,
	MaritalStatus,
	Religion,
	PatientAccountNum,
	SSN,
	b.ClinicID
FROM
	LabPID b
	JOIN LabInbox a ON a.LinkID = b.MsgID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON a.ReceiverClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0




GO
/****** Object:  View [dbo].[View_mongo_LabResult]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[View_mongo_LabResult]
AS
SELECT
	o.ClinicID,
	o.PatientID,
	p.MPatientID MPID,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	'Result' RecordType,
	o.VisitID,
	o.VisitDate,
	null CollectionDate,
	null ReceiveDate,
	o.ReportDate,
	null OrderDate,
	null ServiceDate,
	labinc.LabIncName LabCompany,
	o.LabIncID,
	o.CheckInDate,
	o.CheckInBy,
	RTRIM(e.EmployeeLastName) + ', ' + RTRIM(e.EmployeeFirstName) OrderBy,
	null OrderStatus,
	o.OrderID,
	o.DoctorID,
	o.Status,
	o.Memo,
	null ScheduleDate,
	null MemoClinic,
	null OLID,
	e.EmployeeNPI OrderDoctorNPI,
	o.ReportDate DOS
FROM
	LabResult_Result o
	JOIN Patient        p ON o.PatientID = p.PatientID AND o.ClinicID = p.ClinicID
	LEFT JOIN Employee  e ON o.DoctorID = e.EmployeeID AND o.ClinicID = e.ClinicID
	LEFT JOIN LabOrder_LabInc labinc ON o.LabIncID = labinc.LabIncID
	LEFT JOIN (SELECT DATEADD(dd, -2, LastSyncDate) LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON o.ClinicID = Clinic.ClinicID AND ISNULL(Clinic.Status, 0) = 0
--where
--	o.ClinicID <> 1007
--	AND ( c.LastSyncDate IS NULL
--	      OR ( c.LastSyncDate IS NOT NULL AND EXISTS(SELECT 1 FROM Pati_Daily_Tracking d
--				                                     WHERE  o.PatientID = d.PatientID
--			                                                AND o.ClinicID = d.ClinicID
--			                                                AND d.TableID = o.PatientID
--			                                                AND d.TrackingType = 'Patient'
--			                                                AND d.TrackDate >= c.LastSyncDate) )
--	)






GO
/****** Object:  View [dbo].[View_mongo_MuCheckItemList]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_MuCheckItemList]
AS
	SELECT  m.ClinicID,
			m.VisitID,
			m.CMSID,
			m.ItemKey,
			m.ItemCode,
			m.ItemCodeSysName,
			m.ItemSubCode,
			m.ItemOther,
			m.ItemStatus,
			m.ItemOther1
    FROM Medi_OV_MUCheckItemList m WITH(NOLOCK)
			JOIN Clinic WITH(NOLOCK) on m.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
			--WHERE m.ClinicID <> 1007
			--	  AND (   c.LastSyncDate IS NULL OR
			--			  (c.LastSyncDate IS NOT NULL AND exists(select 1
			--															from Pati_Daily_Tracking d
			--															where     m.PatientID    =  d.PatientID
			--																  AND m.ClinicID     =  d.ClinicID
			--																  AND m.VisitID      =  d.TableID
			--																  AND d.TrackingType =  'OV'
			--																  AND d.TrackDate    >= c.LastSyncDate
			--													   )
			--			  )
			--		 )





GO
/****** Object:  View [dbo].[View_mongo_OB_Forms_PostpartumTool]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_OB_Forms_PostpartumTool]
AS
SELECT
	p.ClinicID,
	p.VisitID,
	p.PatientID,
	p.b1,
	p.b2,
	p.b3,
	p.b4,
	p.b5,
	p.b6,
	p.b7,
	p.b8,
	p.b9,
	p.b10,
	p.b11,
	p.b12,
	p.Q1,
	p.Q2,
	p.Q3,
	p.Q4,
	p.Q5,
	p.Q6,
	p.Q7,
	p.Q8,
	p.Q9,
	p.Q10,
	p.CareSnomedCode,
	p.ScreeningSnomedCode,
	p.OnePointOption,
	p.CreateDate,
	p.CreateBy,
	p.ModifyDate,
	p.ModifyBy
FROM
	OB_Forms_PostpartumTool p
	JOIN Clinic on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
--WHERE
--	p.ClinicID <> 1007
--	AND (   c.LastSyncDate IS NULL OR
--			(c.LastSyncDate IS NOT NULL AND exists(select 1
--														from Pati_Daily_Tracking d
--														where       p.PatientID    =  d.PatientID
--																AND p.ClinicID     =  d.ClinicID
--																AND p.PatientID    =  d.TableID
--																AND d.TrackingType =  'Patient'
--																AND d.TrackDate    >= c.LastSyncDate
--													)
--			)
--		)




GO
/****** Object:  View [dbo].[View_mongo_OB_GeneticHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_OB_GeneticHistory]
as
SELECT a.PatientID,
       a.ClinicID,
	   b.GSName
       FROM OB_Data_Genetic a
	   JOIN (select '~'+ltrim(rtrim(str(GSID)))+'~' GSID,GSName from OB_Ref_Genetic) b on CHARINDEX(b.GSID,'~'+a.SelectedCode+'~') > 0
	   JOIN Clinic on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	  -- where a.CliniciD <> 1007
			--AND (   c.LastSyncDate IS NULL OR
			--		(c.LastSyncDate IS NOT NULL AND exists(select 1
			--													from Pati_Daily_Tracking d
			--													where       a.PatientID    =  d.PatientID
			--															AND a.ClinicID     =  d.ClinicID
			--															AND a.PatientID    =  d.TableID
			--															AND d.TrackingType =  'Patient'
			--															AND d.TrackDate    >= c.LastSyncDate
			--												)
			--		)
			--	)




GO
/****** Object:  View [dbo].[View_mongo_OB_HighRiskFactors]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_OB_HighRiskFactors]
as
SELECT a.PatientID,
       a.ClinicID,
	   b.HRName
       FROM OB_Data_HighRiskFactors a
	   JOIN (select '~0~' HRID,'None' HRName union all select '~'+ltrim(rtrim(str(HRID)))+'~' HRID,HRName from OB_Ref_HighRiskFactors) b ON CHARINDEX(b.HRID,'~'+a.SelectedCode+'~') > 0
	   JOIN Clinic on A.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	  -- where a.CliniciD <> 1007
			--AND (   c.LastSyncDate IS NULL OR
			--		(c.LastSyncDate IS NOT NULL AND exists(select 1
			--													from Pati_Daily_Tracking d
			--													where       A.PatientID    =  d.PatientID
			--															AND A.ClinicID     =  d.ClinicID
			--															AND A.PatientID    =  d.TableID
			--															AND d.TrackingType =  'Patient'
			--															AND d.TrackDate    >= c.LastSyncDate
			--												)
			--		)
			--	)




GO
/****** Object:  View [dbo].[View_mongo_OBHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_OBHistory]
as
   select
            p.PatientID,
		    p.ClinicID,
		    rtrim(s.AgeFi)AgeFi,
		    rtrim(s.NumP)NumP,
		    s.SexO,
		    case s.PID when 'Y' then 'Yes' when 'N' then 'No' else null end PID,
		    case s.VD when 'Y' then 'Yes' when 'N' then 'No' else null end VD,
		    s.Commons SexualHistoryOthers,
			rtrim(m.LMP_Definite)LMP_Definite,
			rtrim(m.LMP_Approximate)LMP_Approximate,
			rtrim(m.LMP_Unknown)LMP_Unknown,
			rtrim(m.LMP_NAmount)LMP_NAmount,
			rtrim(m.LMP_Final)LMP_Final,
			rtrim(m.MensesMonthly)MensesMonthly,
			m.PriorMenses,
			rtrim(m.Frequency)Frequency,
			rtrim(m.Menarche)Menarche,
			case m.OnBCP when 'Y' then 'Yes' when 'N' then 'No' else null end OnBCP,
			m.hCG,
			rtrim(m.Final)Final,
			rtrim(m.FinalStr)FinalStr,
			m.Duration,
			rtrim(m.FT)FT,
			rtrim(m.PT)PT,
			rtrim(m.SPAB)SPAB,
			rtrim(m.[TOP])[TOP],
			rtrim(m.ECTOPIC)ECTOPIC,
			rtrim(m.MULTIBIRTHS)MULTIBIRTHS,
			rtrim(m.LIVING)LIVING,
			m.Memo MenstrualHistoryOthers,
			IH.Commons InfectionHistoryOthers,
			HF.Commons HighRiskFactorOthers,
			og.Comments GeneticOthers
		  from Patient p
		  LEFT JOIN OB_Data_SexualHistory s     on p.PatientID = s.PatientID and p.ClinicID  = s.ClinicID
		  LEFT JOIN OB_Data_MenstrualHistory m  on p.PatientID = m.PatientID and p.ClinicID  = m.ClinicID
		  LEFT JOIN OB_Data_InfectionHistory IH on p.PatientID = IH.PatientID and p.ClinicID = IH.ClinicID
		  LEFT JOIN OB_Data_HighRiskFactors  HF on p.PatientID = HF.PatientID and p.ClinicID = HF.ClinicID
		  LEFT JOIN OB_Data_Genetic          OG on p.PatientID = OG.PatientID and p.ClinicID = OG.ClinicID
		  JOIN Clinic on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		  --where p.ClinicID <> 1007
				--  AND (   c.LastSyncDate IS NULL OR
				--		  (c.LastSyncDate IS NOT NULL AND exists(select 1
				--														from Pati_Daily_Tracking d
				--														where     p.PatientID    =  d.PatientID
				--															  AND p.ClinicID     =  d.ClinicID
				--															  AND p.PatientID    =  d.TableID
				--															  AND d.TrackingType =  'Patient'
				--															  AND d.TrackDate    >= c.LastSyncDate
				--												   )
				--		  )
				--	 )




GO
/****** Object:  View [dbo].[View_mongo_OBInfectionHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





create view [dbo].[View_mongo_OBInfectionHistory]
as
	select a.PatientID,
		   a.ClinicID,
		   b.IHName
			from OB_Data_InfectionHistory a
			JOIN (select '~0~' IHID,'None' IHName union all select '~'+ltrim(rtrim(str(IHID)))+'~' IHID,IHName from OB_Ref_InfectionHistory) b ON CHARINDEX(b.IHID,'~'+a.SelectedCode+'~')>0
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
			JOIN Clinic on A.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
			--where a.ClinicID <> 1007
			--	  AND (   c.LastSyncDate IS NULL OR
			--			  (c.LastSyncDate IS NOT NULL AND exists(select 1
			--															from Pati_Daily_Tracking d
			--															where     A.PatientID    =  d.PatientID
			--																  AND A.ClinicID     =  d.ClinicID
			--																  AND A.PatientID    =  d.TableID
			--																  AND d.TrackingType =  'Patient'
			--																  AND d.TrackDate    >= c.LastSyncDate
			--													   )
			--			  )
			--		 )




GO
/****** Object:  View [dbo].[View_mongo_OccupationHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_OccupationHistory]
as
   select A.PatientID,
          A.ClinicID,
		  A.Occupation,
		  A.FromDate,
          A.EndDate,
		  A.Notes,
		  A.[Status]
		  from MH_Data_OccupationHistory A
		  LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		  JOIN Clinic on A.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  --where A.ClinicID <> 1007
				--  AND (   c.LastSyncDate IS NULL OR
				--		  (c.LastSyncDate IS NOT NULL AND exists(select 1
				--														from Pati_Daily_Tracking d
				--														where     A.PatientID    =  d.PatientID
				--															  AND A.ClinicID     =  d.ClinicID
				--															  AND A.PatientID    =  d.TableID
				--															  AND d.TrackingType =  'Patient'
				--															  AND d.TrackDate    >= c.LastSyncDate
				--												   )
				--		  )
				--	  )





GO
/****** Object:  View [dbo].[View_mongo_OfficeLocation]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[View_mongo_OfficeLocation]
AS
SELECT
	OLID,
	ClinicID,
	Name,
	[Address],
	City,
	[State],
	RTRIM(Zip) Zip,
	ZipExt,
	Phone,
	Fax,
	WorkBeginHour,
	WorkEndHour,
	CIRIdentity,
	CIRUsername,
	CIRPassword,
	CIRFacility,
	CASE CIRDefault WHEN 1 THEN 'Y' ELSE 'N' END CIRDefault,
	FacilityCode,
	CLIA,
	CIRRXAFacility,
	ISNULL([Status], 0) [StatusCode],
	CASE [Status] WHEN '1' THEN 'Active' ELSE 'Inactive' END [Status]
FROM
	Ref_ClinicOfficeLocation
GO
/****** Object:  View [dbo].[View_mongo_OncologyICDExt]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_OncologyICDExt]
AS
	SELECT
	  a.ClinicID,
	  a.VisitID,
	  a.ICDCode Code,
	  a.CodeSystem,
	  a.[Key],
	  a.Value
	FROM  On_Data_ICDExt a WITH(NOLOCK)
	JOIN Medi_OfficeVisit ov WITH(NOLOCK) on a.VisitID = ov.VisitID and a.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic WITH(NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--WHERE a.ClinicID <> 1007
	--		AND (    c.LastSyncDate IS NULL OR
	--				(c.LastSyncDate IS NOT NULL AND exists(select 1
	--															from Pati_Daily_Tracking d
	--															where       ov.PatientID    =  d.PatientID
	--																	AND ov.ClinicID     =  d.ClinicID
	--																	AND ov.VisitID      =  d.TableID
	--																	AND d.TrackingType  =  'OV'
	--																	AND d.TrackDate     >= c.LastSyncDate
	--														)
	--				)
	--			)






GO
/****** Object:  View [dbo].[View_mongo_OncologyICDStage]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_OncologyICDStage]
AS
	SELECT
		  a.ClinicID,
		  a.VisitID,
		  a.ICDCode Code,
		  a.CodeSystem,
		  a.S,
		  a.T,
		  a.N,
		  a.M,
		  a.DiagnosisDate
		FROM  On_Data_ICDStage a WITH(NOLOCK)
		JOIN Medi_OfficeVisit ov WITH(NOLOCK) on a.VisitID = ov.VisitID and a.ClinicID = ov.ClinicID
		JOIN Clinic WITH(NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		--WHERE a.ClinicID <> 1007
		--		AND (   c.LastSyncDate IS NULL OR
		--				(c.LastSyncDate IS NOT NULL AND exists(select 1
		--															from Pati_Daily_Tracking d
		--															where       ov.PatientID    =  d.PatientID
		--																	AND ov.ClinicID     =  d.ClinicID
		--																	AND ov.VisitID      =  d.TableID
		--																	AND d.TrackingType  =  'OV'
		--																	AND d.TrackDate     >= c.LastSyncDate
		--														)
		--				)
		--			)






GO
/****** Object:  View [dbo].[View_mongo_Ophth_PE_FundusDetail]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Ophth_PE_FundusDetail]
AS

SELECT
	p.ClinicID,
	ov.PatientID,
	p.VisitID,
	CONVERT(VARCHAR(10), ov.PatientID)+'-'+CONVERT(VARCHAR(4), p.ClinicID) PatientUID,
	CONVERT(VARCHAR(10), p.VisitID)+'-'+CONVERT(VARCHAR(4), p.ClinicID) VisitUID,
	p.FItemID,
	p.PENormal,
	p.FItemDescription,
	p.OS_PENormal,
	p.OS_FItemDescription,
	p.ODSnomedCode,
	p.OSSnomedCode,
	p.ODSnomedCode2,
	p.OSSnomedCode2,
	p.MuCheckModifySnomedBy,
	p.MuCheckModifySnomedDate,
	c.FItemName,
	c.FItemDesc,
	f.Dilatingdrops_Date
FROM
	Ophth_Data_PE_FundusDetail p
	JOIN Ophth_Data_PE_Fundus f ON p.VisitID = f.VisitID AND p.ClinicID = f.ClinicID
	JOIN Ophth_Ref_FundusItem c ON p.FItemID = c.FItemID
	LEFT JOIN Medi_OfficeVisit ov ON p.VisitID = ov.VisitID AND p.ClinicID = ov.ClinicID
	JOIN Clinic ON f.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
GO
/****** Object:  View [dbo].[View_mongo_Ophth_PE_VA]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Ophth_PE_VA]
AS

SELECT
	p.ClinicID,
	ov.PatientID,
	p.VisitID,
	CONVERT(VARCHAR(10), ov.PatientID)+'-'+CONVERT(VARCHAR(4), p.ClinicID) PatientUID,
	CONVERT(VARCHAR(10), p.VisitID)+'-'+CONVERT(VARCHAR(4), p.ClinicID) VisitUID,
	VAsc_OD,
	VAsc_OS,
	VAsc_OU,
	VAsc_Near,
	VAcc_OD,
	VAcc_OS,
	VAcc_OU,
	VAcc_Near,
	Vaph_OD,
	Vaph_OS,
	Vaph_OU,
	Vaph_Near,
	VABatGlare_OD,
	VABatGlare_OS,
	PAM_OD,
	PAM_OS,
	Pachymetry_OD,
	Pachymetry_OS,
	p.CreateDate,
	p.CreateBy,
	p.ModifyDate,
	p.ModifyBy
FROM
	Ophth_Data_PE_VA p
	LEFT JOIN Medi_OfficeVisit ov ON p.VisitID = ov.VisitID AND p.ClinicID = ov.ClinicID
	JOIN Clinic ON p.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
GO
/****** Object:  View [dbo].[View_mongo_OtherAssessmentPlan]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- Other Assessment/Plan
CREATE View [dbo].[View_mongo_OtherAssessmentPlan]
AS
SELECT a.ClinicID,
       a.VisitID,
	   a.OtherPlan,
	   a.ProcedureNotes
       FROM Medi_OV_CPT a
	   JOIN Medi_OfficeVisit          ov on a.VisitID = ov.VisitID and a.ClinicID = ov.ClinicID
	   JOIN Clinic on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	  -- WHERE ov.ClinicID <> 1007
			--AND (   c.LastSyncDate IS NULL OR
			--		(c.LastSyncDate IS NOT NULL AND exists(select 1
			--													from Pati_Daily_Tracking d
			--													where       ov.PatientID    =  d.PatientID
			--															AND ov.ClinicID     =  d.ClinicID
			--															AND ov.VisitID      =  d.TableID
			--															AND d.TrackingType  =  'OV'
			--															AND d.TrackDate    >= c.LastSyncDate
			--												)
			--		)
			--	)




GO
/****** Object:  View [dbo].[View_mongo_ov]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_ov]
AS
SELECT
	[web_id] WebID,
	ov.ClinicID,
	Clinic.ClinicName,
	ov.PatientID,
	p.MPatientID MPID,
	RTRIM(p.PatientFirstName) PatientFirstName,
	RTRIM(p.PatientLastName) PatientLastName,
	RTRIM(p.PatientMidName) PatientMidName,
	p.PatientDOB,
	p.PatientGender,
	ov.VisitID,
	ov.VisitStatus,
	ov.EMRMode,
	ov.DoctorID PcpID,
	RTRIM(e1.EmployeeLastName) + ', ' + RTRIM(e1.EmployeeFirstName) PcpName,
	e1.EmployeeNPI PcpNPI,
	ov.CDID CoverByID,
	RTRIM(e2.EmployeeLastName) + ', ' + RTRIM(e2.EmployeeFirstName) CoverByName,
	e2.EmployeeNPI CoverByNPI,
	ov.RegDate,
	ov.VisitDate,
	CASE ov.PatientIsReady WHEN '1' THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END PatientIsReady,
	nn.NurseNotes,
	CASE ov.ReconciliationComplete WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END Reconciliation,
	CASE IsNewPatient WHEN 0 THEN 'N' WHEN 1 THEN 'Y' ELSE NULL END IsNewPatient,
	CASE PatientWithTOC WHEN 0 THEN 'N' WHEN 1 THEN 'Y' ELSE NULL END PatientWithTOC,
	UseMedicationForAsthma,
	CounselingForNutrition,
	CounselingForNutritionCode,
	CounselingForPhysicalActivity,
	CounselingForPhysicalActivityCode,
	CASE RxNotDone WHEN 0 THEN 'N' WHEN 1 THEN 'Y' ELSE NULL END RxNotDone,
	RxNotDoneReason,
	CASE ClinicalSummariesIsReady WHEN 0 THEN 'N' WHEN 1 THEN 'Y' ELSE NULL END ClinicalSummariesIsReady,
	CASE MedicationDocumented WHEN 0 THEN 'N' WHEN 1 THEN 'Y' ELSE NULL END MedicationDocumented,
	CMS90v3Code,
	CMS90v3Result,
	CMS56v2Code,
	CMS56v2Result,
	CMS66v2Code,
	CMS66v2Result,
	HeartFailureSNOMED,
	VisitFinishTime,
	ICDCodeSystem,
	ACPCheckingStatus,
	CMS130Code,
	CMS130Date,
	CMS130Selection,
	InsuPlanID,
	ov.OfficeLocation OfficeLocationID,
	loc.Name OfficeLocationName
FROM  Medi_OfficeVisit ov with(nolock)
      LEFT JOIN Patient p with(nolock) ON p.PatientID = ov.PatientID AND p.ClinicID = ov.ClinicID
      LEFT JOIN Employee e1 with(nolock) ON e1.EmployeeID = ov.DoctorID AND e1.ClinicID = ov.ClinicID
	  LEFT JOIN Employee e2 with(nolock) ON e2.EmployeeID = ov.CDID AND e2.ClinicID = ov.ClinicID
	  JOIN Clinic with(nolock) on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	  LEFT JOIN Medi_OV_NurseNotes nn with(nolock) ON nn.VisitID = ov.VisitID
	  LEFT JOIN Ref_ClinicOfficeLocation loc WITH(NOLOCK) ON loc.OLID = ov.OfficeLocation
	  LEFT JOIN (SELECT web_id, dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	  LEFT JOIN PATI_DAILY_TRACKING pdt with(nolock) on ov.PatientID = pdt.PatientID and ov.ClinicID = pdt.ClinicID and ov.VisitID = pdt.TableID and pdt.TrackingType = 'OV'
WHERE
	  ov.VisitStatus <> 'BI' AND ov.VisitStatus <> 'CA'
		AND (   c.LastSyncDate IS NULL OR (c.LastSyncDate IS NOT NULL AND pdt.RowID is not null ) )
GO
/****** Object:  View [dbo].[View_mongo_patient]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[View_mongo_patient]
AS
SELECT
	[web_id] WebID,
	a.PatientID,
	a.MPatientID,
	a.ClinicID,
	Clinic.ClinicName,
	RTRIM(a.PatientFirstName) PatientFirstName,
	RTRIM(a.PatientLastName) PatientLastName,
	RTRIM(a.PatientMidName) PatientMidName,
	a.PatientDOB,
	CONVERT(INT, FLOOR(DATEDIFF(DY, a.PatientDOB, GETDATE()) / 365.25)) Age,
	DATEDIFF(mm, a.PatientDOB, GETDATE()) - CASE WHEN DATEPART(dd, a.PatientDOB) - DATEPART(dd, GETDATE()) > 0 THEN 1 ELSE 0 END AgeMonth,
	DATEDIFF(dd, a.PatientDOB, GETDATE()) AgeDay,
	a.PatientGender,
	pcp.EmployeeID PcpID,
	RTRIM(pcp.EmployeeLastName) + ', ' + RTRIM(pcp.EmployeeFirstName) PcpName,
	RTRIM(pcp.EmployeeNPI) PcpNPI,
	RTRIM(a.ChartNum) ChartNum,
	a.PatientSSNumber,
	a.ActiveStatus,
	a.DeceaseDate,
	a.PatientMarrage,
	dbo.fn_mongo_GetPatientRaceName(a.ClinicID, a.PatientID) Race, --a.Race,
	dbo.fn_mongo_GetPatientRaceCode(a.ClinicID, a.PatientID) RaceCode,
	dbo.fn_mongo_GetPatientEthnicityName(a.ClinicID, a.PatientID) Ethnicity, --a.Ethnicity,
	dbo.fn_mongo_GetPatientEthnicityCode(a.ClinicID, a.PatientID) EthnicityCode,
	dbo.fn_mongo_GetPatientTopLevelRaceName(a.ClinicID, a.PatientID) Race_TopLevel,
	dbo.fn_mongo_GetPatientTopLevelRaceCode(a.ClinicID, a.PatientID) RaceCode_TopLevel,
	dbo.fn_mongo_GetPatientTopLevelEthnicityName(a.ClinicID, a.PatientID) Ethnicity_TopLevel,
	dbo.fn_mongo_GetPatientTopLevelEthnicityCode(a.ClinicID, a.PatientID) EthnicityCode_TopLevel,
	a.[Language],
	a.[Notification],
	a.Occupation,
	CASE a.AdvanceDirective WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END AdvanceDirective,
	a.PatientAddrStreet,
	a.PatientAddrStreet2,
	RTRIM(a.PatientAddrApt) PatientAddrApt,
	RTRIM(a.PatientAddrCity) PatientAddrCity,
	a.PatientAddrState,
	RTRIM(a.PatientAddrZip) PatientAddrZip,
	RTRIM(a.PatientAddrZipExt) PatientAddrZipExt,
	a.PatientHomePhone,
	a.PatientWorkPhone,
	RTRIM(a.PatientWorkPhoneExt) PatientWorkPhoneExt,
	a.PatientMobilePhone,
	a.PatientEMail,
	CASE a.MedicationPerformed WHEN 1 THEN 'Y' ELSE 'N' END MedicationPerformed,
	pic.PicSize PicID,
	pic.UploadDate PicDate
FROM
	Patient a with(nolock)
	LEFT JOIN (SELECT web_id, dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID AND isnull(Clinic.Status,0) = 0
	LEFT JOIN Employee pcp WITH (NOLOCK) ON a.ClinicID = pcp.ClinicID AND a.DoctorID = pcp.EmployeeID
	LEFT JOIN Pati_Picture pic ON a.PatientID = pic.PatientID
	LEFT JOIN Pati_Daily_Tracking pdt WITH (NOLOCK) ON a.PatientID = pdt.PatientID AND a.ClinicID = pdt.ClinicID AND pdt.TrackingType='Patient' AND pdt.TableID = a.PatientID
WHERE
	--a.ClinicID = 1007
	( c.LastSyncDate IS NULL OR (c.LastSyncDate IS NOT NULL AND pdt.RowID IS NOT NULL ))
GO
/****** Object:  View [dbo].[View_mongo_PatientAllergyEnvironmentalHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientAllergyEnvironmentalHistory]
AS
	SELECT  a.ClinicID,
	        a.PatientID,
			a.EnvironmentalID,
			CASE a.EnvironmentalID WHEN 0 THEN 'No known Environmental Allergy' ELSE EnvName END EnvName,
			Reaction,
			c.[Status],
			ResolvedDate
	FROM MH_Data_Allergy_Environmental a WITH (NOLOCK)
			LEFT JOIN MH_Ref_Environmental b WITH (NOLOCK) ON b.EnvID=a.EnvironmentalID
			LEFT JOIN Pati_Allergy_Reaction c WITH (NOLOCK) ON c.AllergyID=a.EnvironmentalID AND c.AllergyType='Environmental' AND c.PatientID=a.PatientID
			--LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) ic on 1 = 1
			JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
			--WHERE a.ClinicID <> 1007
			--	  AND (   ic.LastSyncDate IS NULL OR
			--			  (ic.LastSyncDate IS NOT NULL AND exists(select 1
			--															from Pati_Daily_Tracking d
			--															where     a.PatientID    =  d.PatientID
			--																  AND a.ClinicID     =  d.ClinicID
			--																  AND a.PatientID      =  d.TableID
			--																  AND d.TrackingType =  'Patient'
			--																  AND d.TrackDate    >= ic.LastSyncDate
			--													   )
			--			  )
			--		 )
GO
/****** Object:  View [dbo].[View_mongo_PatientAllergyFoodHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientAllergyFoodHistory]
AS
SELECT
	a.ClinicID, a.PatientID, a.FoodID, CASE a.FoodID WHEN 0 THEN 'No known Food Allergy' ELSE FoodName END FoodName, Reaction, c.[Status], ResolvedDate, b.SnomedCode
FROM MH_Data_Allergy_Food a WITH (NOLOCK)
	LEFT JOIN MH_Ref_Food b WITH (NOLOCK) ON b.FoodID=a.FoodID
	LEFT JOIN Pati_Allergy_Reaction c WITH (NOLOCK) ON c.AllergyID=a.FoodID AND c.AllergyType='Food' AND c.PatientID=a.PatientID
	--LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) ic on 1 = 1
	JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--WHERE a.ClinicID <> 1007
	--		AND (   ic.LastSyncDate IS NULL OR
	--				(ic.LastSyncDate IS NOT NULL AND exists(select 1
	--															from Pati_Daily_Tracking d
	--															where     a.PatientID    =  d.PatientID
	--																	AND a.ClinicID     =  d.ClinicID
	--																	AND a.PatientID      =  d.TableID
	--																	AND d.TrackingType =  'Patient'
	--																	AND d.TrackDate    >= ic.LastSyncDate
	--														)
	--				)
	--			)
GO
/****** Object:  View [dbo].[View_mongo_PatientAllergyMedicalHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientAllergyMedicalHistory]
AS
SELECT
   a.ClinicID,
   a.PatientID,
   a.MedicationID,
   CASE a.MedicationID WHEN 0 THEN 'No known Medication Allergy' ELSE a.DrugName END DrugName,
   a.NDC,
   a.GPI,
   a.RxNorm,
   a.AllergyCode,
   a.Reaction,
   a.[Status],
   ISNULL(a.ResolvedDate, ISNULL(a.CreateDate, a.ModifyDate)) ResolvedDate,
   a.[Source],
   b.[SNOMEDCode]
FROM
   MH_Data_Allergy_Medication a WITH (NOLOCK)
   LEFT JOIN MH_Ref_Allergy b WITH (NOLOCK) on a.MedicationID = b.AllergyID
   --LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
   JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--   a.ClinicID <> 1007
--	AND (   c.LastSyncDate IS NULL OR
--			(c.LastSyncDate IS NOT NULL AND exists(select 1
--														from Pati_Daily_Tracking d
--														where       a.PatientID    =  d.PatientID
--																AND a.ClinicID     =  d.ClinicID
--																AND a.PatientID    =  d.TableID
--																AND d.TrackingType =  'Patient'
--																AND d.TrackDate    >= c.LastSyncDate
--													)
--			)
--		)
GO
/****** Object:  View [dbo].[View_mongo_PatientConsent]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[View_mongo_PatientConsent]
as
select p.PatientID,
       p.ClinicID,
	   cs.ConsentID,
	   cs.ConsentStatus,
	   cs.ConsentDate,
	   cl.ConsentName
from Consent_Status cs WITH (NOLOCK)
	   join Consent_ConsentList cl WITH (NOLOCK) on cs.ConsentID = cl.ConsentID
	   join Patient p WITH (NOLOCK) on cs.PatientID = p.PatientID
	   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	   JOIN Clinic WITH (NOLOCK) on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	  -- where p.ClinicID <> 1007
			--AND (   c.LastSyncDate IS NULL OR
			--		(c.LastSyncDate IS NOT NULL AND exists(select 1
			--													from Pati_Daily_Tracking d
			--													where       p.PatientID    =  d.PatientID
			--															AND p.ClinicID     =  d.ClinicID
			--															AND p.PatientID    =  d.TableID
			--															AND d.TrackingType =  'Patient'
			--															AND d.TrackDate    >= c.LastSyncDate
			--												)
			--		)
			--	)





GO
/****** Object:  View [dbo].[View_mongo_PatientDocument]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientDocument]
AS
SELECT
  a.ClinicID,
  a.PatientID,
  CONVERT(VARCHAR(10), a.PatientID) + '-' + CONVERT(CHAR(4), a.ClinicID) PatientUID,
  RTRIM(a.PicExt) PicExt,
  a.PicSize FileID,
  Replace(Replace(Replace(Replace(Rtrim(Isnull(CDName, '')), '. ', ' '), '/ ', ' '), '.', ' '), '/', ' ') Category,
  a.DiagDate,
  a.UploadDate,
  RTRIM(a.[Description]) [Description],
  a.ID DocID
FROM   Pati_DiagPic a
       JOIN Ref_Customization c
              ON c.ClinicID = a.ClinicID
                 AND c.CDID = 3
                 AND CDEnable = 1
                 AND c.CDNumber = a.PicType
GO
/****** Object:  View [dbo].[View_mongo_PatientFamilyHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientFamilyHistory]
AS
SELECT
	  a.ClinicID,
	  a.PatientID,
	  a.FHID,
	  CASE a.FHID WHEN 0 THEN 'No Significant Family History' ELSE ISNULL(a.FamilyHistoryName, b.FHName) END FamilyHistoryName,
	  a.Reaction,
	  a.ICD9Code ICDCode,
	  a.CodeSystem,
	  b.SNOMED SnomedCode,
	  a.CreateDate,
	  b.FHName
FROM  MH_Data_FamilyHistory_Reaction a WITH (NOLOCK)
	  LEFT JOIN MH_Ref_FamilyHistory b WITH (NOLOCK) ON a.FHID=b.FHID AND (a.ClinicID = b.ClinicID OR b.ClinicID = 0)
	  LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	  JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--      a.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       a.PatientID    =  d.PatientID
--																	AND a.ClinicID     =  d.ClinicID
--																	AND a.PatientID    =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)
GO
/****** Object:  View [dbo].[View_mongo_PatientHistoryOthers]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE View [dbo].[View_mongo_PatientHistoryOthers]
AS
SELECT
	p.ClinicID,
	p.PatientID,
	su.InputMemo SurgicalHistoryOthers,
	me.HistoryDetail MedicalHistoryOthers,
	fa.InputMemo FamilyHistoryOthers,
	so.Comments SocialHistoryOthers,
	al.Food AllergyFoodOthers,
	al.Environmental AllergyEnvironmentalOthers,
	al.Medication AllergyMedicationOthers,
	fa.Ocular,
	fa.OcularOther,
	mn.MedicationNotes,
	rtrim(me.TOBACCO_PREPREG)TOBACCO_PREPREG,
	rtrim(me.TOBACCO_PREG)TOBACCO_PREG,
	rtrim(me.TOBACCO_YEARS)TOBACCO_YEARS,
	rtrim(me.ALCOHOL_PREPREG)ALCOHOL_PREPREG,
	rtrim(me.ALCOHOL_PREG)ALCOHOL_PREG,
	rtrim(me.ALCOHOL_YEARS)ALCOHOL_YEARS,
	rtrim(me.ILLICIT_PREPREG)ILLICIT_PREPREG,
	rtrim(me.ILLICIT_PREG)ILLICIT_PREG,
	rtrim(me.ILLICIT_YEARS)ILLICIT_YEARS,
	rtrim(me.Prenatal)Prenatal,
	rtrim(me.ld)ld,
	rtrim(me.Neonatal)Neonatal,
	dp.NoProblem
FROM
    Patient p WITH (NOLOCK)
	LEFT JOIN MH_Data_SurgicalHistory su WITH (NOLOCK) ON su.PatientID = p.patientID
	LEFT JOIN MH_Data_MedicalHistory  me WITH (NOLOCK) ON me.PatientID = p.patientID
	LEFT JOIN MH_Data_FamilyHistory   fa WITH (NOLOCK) ON fa.PatientID = p.patientID
	LEFT JOIN MH_Data_SocialHistory   so WITH (NOLOCK) ON so.PatientID = p.patientID
	LEFT JOIN MH_Data_Allergy         al WITH (NOLOCK)ON al.PatientID = p.patientID
	LEFT JOIN MH_Data_MedicationNotes mn WITH (NOLOCK) on p.PatientID  = mn.PatientID and p.ClinicID = mn.ClinicID
	LEFT JOIN MH_Data_ProblemList     dp WITH (NOLOCK) on p.PatientID  = dp.PatientID and p.ClinicID = dp.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic WITH (NOLOCK)on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--	p.ClinicID <> 1007
--	AND (   c.LastSyncDate IS NULL OR
--			(c.LastSyncDate IS NOT NULL AND exists(select 1
--														from Pati_Daily_Tracking d
--														where       p.PatientID    =  d.PatientID
--																AND p.ClinicID     =  d.ClinicID
--																AND p.PatientID    =  d.TableID
--																AND d.TrackingType =  'Patient'
--																AND d.TrackDate    >= c.LastSyncDate
--													)
--			)
--		)





GO
/****** Object:  View [dbo].[View_mongo_PatientInsuPlanHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- For mongo CDR database
-- PlanExpire date need to be MAX Datetime if insurance plan is in active status and PlanExpire field is null,
-- because this database is not up to date
CREATE VIEW [dbo].[View_mongo_PatientInsuPlanHistory]
AS
SELECT ISNULL(EffectiveDate,
			  ISNULL((SELECT MIN(h.LastModifyDate) FROM Pati_InsuPlan_History h WHERE h.ClinicID=p.ClinicID AND h.PatientID=p.PatientID AND h.InsuPlanID=p.InsuPlanID AND p.Active=1), p.CreateDate)
	   ) StartDate,
	   CASE
	       WHEN p.Active = 1 THEN ISNULL(PlanExpire, CONVERT(DATETIME,'2099-12-31'))
		   ELSE ISNULL(PlanExpire, ISNULL((SELECT MAX(h.LastModifyDate) FROM Pati_InsuPlan_History h WHERE h.ClinicID=p.ClinicID AND h.PatientID=p.PatientID AND h.InsuPlanID=p.InsuPlanID AND p.Active=-1), p.CreateDate))
	   END EndDate,
	   p.ClinicID,
	   p.PatientID,
	   p.InsuPlanID,
	   p.InsuOrder,
	   RTRIM(r.InsuComName) InsuComName,
	   RTRIM(r.InsucomSubmitterID) InsucomSubmitterID,
	   r.InsuComType,
	   RTRIM(p.PlanName) PlanName,
	   RTRIM(p.InsuredID) InsuredID,
	   RTRIM(p.PolicyNumber) PolicyNumber,
	   CASE p.Active WHEN 1 THEN 'A' WHEN 0 THEN 'I' ELSE NULL END ActiveStatus,
	   RTRIM(p.PlanType) PlanType,
	   p.InsuredRelationShip,
	   RTRIM(p.InsuredFirstName) InsuredFirstName,
	   RTRIM(p.InsuredLastName) InsuredLastName,
	   RTRIM(p.InsuredMidName) InsuredMidName,
	   p.InsuredDOB,
	   p.InsuredGender,
	   p.InsuredSSNumber,
	   p.InsuredMarrage,
	   p.InsuredAddrStreet,
	   RTRIM(p.InsuredAddrApt) InsuredAddrApt,
	   RTRIM(p.InsuredAddrCity) InsuredAddrCity,
	   RTRIM(p.InsuredAddrState) InsuredAddrState,
	   RTRIM(p.InsuredAddrZip) InsuredAddrZip,
	   p.InsuredHomePhone,
	   p.InsuredWorkPhone
FROM
      Pati_InsuPlan p WITH (NOLOCK)
	  LEFT JOIN Ref_InsuCom r WITH (NOLOCK) ON r.InsuComID = p.TempInsuComID
	  JOIN Clinic WITH (NOLOCK) on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	  LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
--WHERE
--	  p.ClinicID <> 1007
--		AND (    c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where     p.PatientID      =  d.PatientID
--																	AND p.ClinicID     =  d.ClinicID
--																	AND p.PatientID    =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)








GO
/****** Object:  View [dbo].[View_mongo_PatientMedicalHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientMedicalHistory]
AS
SELECT a.ClinicID, a.PatientID,
	   a.MHID,
	   CASE a.MHID WHEN 0 THEN 'No Significant Past Medical History' ELSE b.MHName END MHName,
	   b.ICDCode ICD9, b.ICD10Code ICD10, b.SnomedCode, c.MHDate Note,
	   CASE WHEN c.CreateDate IS NOT NULL THEN c.CreateDate ELSE c.ModifyDate END MHDate
FROM MH_Data_MedicalHistory_List a WITH (NOLOCK)
	LEFT JOIN MH_Ref_MedicalHistory b WITH (NOLOCK) ON a.MHID=b.MHID AND (a.ClinicID = b.ClinicID OR b.ClinicID = 0)
	LEFT JOIN MH_Data_MedicalHistory_Date c WITH (NOLOCK) ON a.MHID=c.MHID AND a.PatientID=c.PatientID AND a.ClinicID = c.ClinicID
	--LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--WHERE
	--	  a.ClinicID <> 1007
	--		AND (   di.LastSyncDate IS NULL OR
	--				(di.LastSyncDate IS NOT NULL AND exists(select 1
	--															from Pati_Daily_Tracking d
	--															where       a.PatientID    =  d.PatientID
	--																	AND a.ClinicID     =  d.ClinicID
	--																	AND a.PatientID    =  d.TableID
	--																	AND d.TrackingType =  'Patient'
	--																	AND d.TrackDate    >= di.LastSyncDate
	--														)
	--				)
	--			)
GO
/****** Object:  View [dbo].[View_mongo_PatientMedicalHistory_ICD]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientMedicalHistory_ICD]
AS
SELECT
	a.ClinicID,
	a.PatientID,
	a.ICDName,
	CASE a.CodeSystem WHEN 'ICD9' THEN a.ICDCode ELSE NULL END ICD9,
	CASE a.CodeSystem WHEN 'ICD10' THEN a.ICDCode ELSE NULL END ICD10,
	a.Notes Note,
	CASE WHEN a.CreateDate IS NOT NULL THEN a.CreateDate ELSE a.ModifyDate END MHDate
FROM MH_Data_MedicalHistory_ICD a WITH (NOLOCK)
	JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and ISNULL(Clinic.[Status],0) = 0
GO
/****** Object:  View [dbo].[View_mongo_PatientMyTags]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[View_mongo_PatientMyTags]
as
select doc.ClinicID,
       pDoc.PatientID,
	   doc.LinkID,
	   doc.LinkID2,
	   doc.TagType,
	   taglist.MyTagName,
	   --tagCat.TagCategoryName,
	   tagCode.TagCode,
	   tagCode.CodeSystem,
	   doc.CreateDate DOS
from   MyTag_Document          doc WITH (NOLOCK)
	   join MyTag_TagList      tagList WITH (NOLOCK) on doc.MyTagID = tagList.MyTagID
	   --join MyTag_TagCategory tagCat  on tagList.TagCategoryID = tagCat.TagCategoryID
	   join Pati_DiagPic       pDoc    WITH (NOLOCK) on doc.ClinicID = pDoc.ClinicID and doc.LinkID = pDoc.ID
	   LEFT JOIN MyTag_TagCode tagCode WITH (NOLOCK) on tagCode.MyTagID = tagList.MyTagID
	   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	   JOIN Clinic WITH (NOLOCK) on doc.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	  -- where doc.ClinicID <> 1007
			--AND (   c.LastSyncDate IS NULL OR
			--		(c.LastSyncDate IS NOT NULL AND exists(select 1
			--													from Pati_Daily_Tracking d
			--													where       pDoc.PatientID    =  d.PatientID
			--															AND pDoc.ClinicID     =  d.ClinicID
			--															AND pDoc.PatientID    =  d.TableID
			--															AND d.TrackingType    =  'Patient'
			--															AND d.TrackDate       >= c.LastSyncDate
			--												)
			--		)
			--	)





GO
/****** Object:  View [dbo].[View_mongo_PatientProgram]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientProgram]
AS
SELECT
    ltrim(rtrim(str(PatientID)))+'-'+ltrim(rtrim(str(a.ClinicID))) PatientUID,
	a.ClinicID,
	a.PatientID,
	a.ProgramCode,
	a.CreateBy,
	a.CreateDate
FROM
	Pati_ProgramsPanel a,Clinic c
	where a.ClinicID=c.ClinicID and isnull(c.Status,0)=0
GO
/****** Object:  View [dbo].[View_mongo_PatientSmokingHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientSmokingHistory]
AS
SELECT
  a.ClinicID,
  a.PatientID,
  CONVERT(VARCHAR(10), a.PatientID)+'-'+CONVERT(VARCHAR(4), a.ClinicID) PatientUID,
  a.CurrentSmoker,
  CASE a.CurrentSmoker
    WHEN 0 THEN 'Former smoker'
    WHEN 1 THEN 'Current every day smoker'
    WHEN 2 THEN 'Never smoker'
    WHEN 3 THEN 'Current some day smoker'
    WHEN 4 THEN 'Smoker, current status unknown'
    WHEN 5 THEN 'Unknown if ever smoked'
    WHEN 6 THEN 'Heavy tobacco smoker'
    WHEN 7 THEN 'Light tobacco smoker'
  END             SmokingStatus,
  CASE a.CurrentSmoker
    WHEN 0 THEN '8517006'
    WHEN 1 THEN '449868002'
    WHEN 2 THEN '266919005'
    WHEN 3 THEN '428041000124106'
    WHEN 4 THEN '77176002'
    WHEN 5 THEN '266927001'
    WHEN 6 THEN '428071000124103'
    WHEN 7 THEN '428061000124105'
  END             SmokingStatusSnomed,
  CASE a.CCS
    WHEN '1' THEN 'Y'
    WHEN '0' THEN 'N'
    ELSE NULL
  END             CessationCounseling,
  CessationCounselingCode,
  ISNULL(CCS_Date, ISNULL(a.CreateDate, a.ModifyDate))  SmokingStartDate,
  FSmokerStopDate SmokingEndDate,
  CSmokerYears,
  CASE CSType
    WHEN 1 THEN 'Cigarette'
    WHEN 2 THEN 'Cigar'
    WHEN 3 THEN 'Chewing Tobacco'
    WHEN 4 THEN 'Pipe'
    ELSE NULL
  END             SmokeType,
  CSmokerPacks,
  FSmokerPacks,
  CASE CurrentSmokerUnit
    WHEN 1 THEN 'Cigarette(s)'
    WHEN 2 THEN 'Pack(s)'
    WHEN 3 THEN 'Cigar(s)'
    WHEN 4 THEN 'Pinch(es)'
    WHEN 5 THEN 'Poach(es)'
    ELSE NULL
  END             CurrentSmokerUnit,
  CASE FormerSmokerUnit
    WHEN 1 THEN 'Cigarette(s)'
    WHEN 2 THEN 'Pack(s)'
    WHEN 3 THEN 'Cigar(s)'
    WHEN 4 THEN 'Pinch(es)'
    WHEN 5 THEN 'Poach(es)'
    ELSE NULL
  END             FormerSmokerUnit,
  SmokingNotes
FROM   MH_Data_SocialHistory a WITH (NOLOCK)
       LEFT JOIN (SELECT Dateadd(dd, -2, LastSyncDate)LastSyncDate FROM db_identity) c ON 1 = 1
       JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID AND Isnull(Clinic.Status, 0) = 0
--WHERE a.ClinicID <> 1007
--	AND (   c.LastSyncDate IS NULL OR
--			(c.LastSyncDate IS NOT NULL AND exists(select 1
--														from Pati_Daily_Tracking d
--														where       a.PatientID    =  d.PatientID
--																AND a.ClinicID     =  d.ClinicID
--																AND a.PatientID    =  d.TableID
--																AND d.TrackingType =  'Patient'
--																AND d.TrackDate    >= c.LastSyncDate
--													)
--			)
--	     )
GO
/****** Object:  View [dbo].[View_mongo_PatientSocialHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- Social History
CREATE VIEW [dbo].[View_mongo_PatientSocialHistory]
AS
SELECT
	a.ClinicID, a.PatientID,
	a.SHID,
	CASE a.SHID WHEN 0 THEN 'No Significant Social History' ELSE SHName END SHName,
	SHNotes
FROM MH_Data_SocialHistory_List a WITH (NOLOCK)
	LEFT JOIN MH_Ref_SocialHistory b WITH (NOLOCK) ON b.SHID = a.SHID AND (a.ClinicID = b.ClinicID OR b.ClinicID = 0)
	LEFT JOIN MH_Data_SocialHistory_Notes c WITH (NOLOCK) ON c.PatientID = a.PatientID AND c.ClinicID = a.ClinicID AND c.SHID = a.SHID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--      a.ClinicID <> 1007
--		AND (    di.LastSyncDate IS NULL OR
--				(di.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       a.PatientID    =  d.PatientID
--																	AND a.ClinicID     =  d.ClinicID
--																	AND a.PatientID      =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= di.LastSyncDate
--														)
--				)
--			)






GO
/****** Object:  View [dbo].[View_mongo_PatientSurgicalHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- Surgical List
CREATE VIEW [dbo].[View_mongo_PatientSurgicalHistory]
AS
SELECT
	  a.ClinicID, a.PatientID,
	  a.SHID,
	  CASE a.SHID WHEN 0 THEN 'No Significant Surgical History' ELSE b.SHName END SHName,
	  b.SHName_cn, b.SnomedCode, b.SHCPTCode, c.SHNotes,
	  case when c.SHDate is not null then c.SHDate else case when c.CreateDate is not null then c.CreateDate else c.ModifyDate end end SHDate
		FROM      MH_Data_SurgicalHistory_List a WITH (NOLOCK)
		LEFT JOIN MH_Ref_SurgicalHistory b WITH (NOLOCK) ON a.SHID=b.SHID AND (a.ClinicID = b.ClinicID OR b.ClinicID = 0)
		LEFT JOIN MH_Data_SurgicalHistory_Notes c WITH (NOLOCK) ON a.SHID=c.SHID AND a.PatientID=c.PatientID AND a.ClinicID = c.ClinicID
		LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
		JOIN Clinic WITH (NOLOCK) on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--      a.ClinicID <> 1007
--		AND (    di.LastSyncDate IS NULL OR
--				(di.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       a.PatientID    =  d.PatientID
--																	AND a.ClinicID     =  d.ClinicID
--																	AND a.PatientID      =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= di.LastSyncDate
--														)
--				)
--			)






GO
/****** Object:  View [dbo].[View_mongo_PatientVaccine]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_PatientVaccine]
AS
SELECT
	a.ClinicID,
	a.PatientID,
	CONVERT(VARCHAR(10), a.PatientID) + '-' + CONVERT(CHAR(4), a.ClinicID) PatientUID,
	a.VaccineID,
	CONVERT(INT, FLOOR(DATEDIFF(DY, p.PatientDOB, a.AdministrationDate) / 365.25)) Age,
	DATEDIFF(mm, p.PatientDOB, GETDATE()) - CASE WHEN DATEPART(dd, p.PatientDOB) - DATEPART(dd, a.AdministrationDate) > 0 THEN 1 ELSE 0 END AgeMonth,
	DATEDIFF(dd, p.PatientDOB, GETDATE()) AgeDay,
	p.PatientGender,
	RTRIM(a.CVXCode) CVXCode,
	cvx.ShortName CVXName,
	cvx.FullName,
	--g.GroupCode,
	--g.GroupName,
	--g.GroupNameCIR,
	--gr.[State] GroupState,
	a.InvID InventoryID,
	a.FundingSource,
	RTRIM(a.MFTCode) MFTCode,
	a.MFTName,
	a.LotNumber,
	a.VISDate,
	a.ExpiredDate,
	a.AdministrationDate,
	a.OrderDate,
	a.VaccineDose,
	a.Unit,
	RTRIM(a.[Site]) [Site],
	a.ProviderID,
	RTRIM(e1.EmployeeFirstName) ProviderFirstName,
	RTRIM(e1.EmployeeLastName) ProviderLastName,
	RTRIM(e1.EmployeeMidName) ProviderMidName,
	RTRIM(e1.EmployeeType) ProviderType,
	a.AdministerID AdministerID,
	RTRIM(e2.EmployeeFirstName) AdministerFirstName,
	RTRIM(e2.EmployeeLastName) AdministerLastName,
	RTRIM(e2.EmployeeMidName) AdministerMidName,
	RTRIM(e2.EmployeeType) AdministerType,
	a.ProviderInitials,
	a.OtherProvider,
	a.OfficeLocation,
	RTRIM(location.Name) OfficeLocationName,
	RTRIM(location.CIRFacility) CIRFacility,
	a.OtherLocation,
	RTRIM(a.VFC) Eligibility,
	RTRIM(a.InformationSource) InformationSource,
	a.NextDueDate,
	a.PublishDate,
	a.Notes,
	a.WaitTime,
	CASE a.IsUploaded WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END IsUploaded,
	a.UploadDate,
	CASE a.NewVaccine WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END NewVaccine,
	a.CreateBy,
	a.CreateDate,
	a.ModifyBy,
	a.ModifyDate
FROM
	Vaccine_Data_PatientVaccine a
	LEFT JOIN Vaccine_Ref_CVX cvx on a.CVXCode = cvx.CVXCode
	--LEFT JOIN Vaccine_Ref_Group_CVX gr ON gr.CVXCode = a.CVXCode
	--LEFT JOIN Vaccine_Ref_Group g ON g.GroupCode = gr.GroupCode
	--LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Patient p ON p.PatientID = a.PatientID
	LEFT JOIN Employee e1 ON e1.EmployeeID = a.ProviderID
	LEFT JOIN Employee e2 ON e2.EmployeeID = a.AdministerID
	LEFT JOIN Ref_ClinicOfficeLocation location ON location.OLID = a.OfficeLocation
	JOIN Clinic on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--	a.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       a.PatientID    =  d.PatientID
--																	AND a.ClinicID     =  d.ClinicID
--																	AND a.PatientID      =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)
GO
/****** Object:  View [dbo].[View_mongo_PEDetail]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_PEDetail]
as
	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from GI_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN GI_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 2 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Derm_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Derm_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 3 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from OB_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN OB_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 4 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Int_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Int_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 7 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Cardiol_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Cardiol_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 8 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from On_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN On_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 9 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Ophth_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Ophth_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 10 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from PMR_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN PMR_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 11 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Pulm_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Pulm_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 12 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Nephro_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Nephro_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 13 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Peds_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Peds_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 14 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Neuro_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Neuro_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 15 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from Surg_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN Surg_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 16 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from ENT_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN ENT_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 17 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)

	union all

	select a.VisitID,ov.ClinicID,a.PEID,case when a.PENormal = 1 then 'Y' else 'N' end PENormal,a.PEDesc,case when c.CDNumber is not null then c.CDName else b.Name end PEName,case when c.CDNumber is not null then c.CDOrder else b.DispPosition end PEDispOrder
	from MntH_Data_PEDetail a
	join Medi_OfficeVisit   ov on a.VisitID = ov.VisitID
	LEFT JOIN MntH_Ref_PE   b on a.PEID = b.ID
	LEFT JOIN Ref_Customization c on a.PEID = c.CDNumber and c.CDID = 5 and c.EMRMODE = 18 and c.ClinicID = ov.ClinicID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	--where ov.ClinicID <> 1007
	--	AND (    di.LastSyncDate IS NULL OR
	--			(di.LastSyncDate IS NOT NULL AND exists(select 1
	--														from Pati_Daily_Tracking d
	--														where       ov.PatientID   =  d.PatientID
	--																AND ov.ClinicID    =  d.ClinicID
	--																AND ov.VisitID     =  d.TableID
	--																AND d.TrackingType =  'OV'
	--																AND d.TrackDate    >= di.LastSyncDate
	--													)
	--			)
	--		)




GO
/****** Object:  View [dbo].[View_mongo_PEOther]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





create view [dbo].[View_mongo_PEOther]
as
	select ov.VisitID,ov.ClinicID,PEOther2
		   from GI_Data_PE a
		   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
    union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Derm_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
    union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from OB_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
    union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Int_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Cardiol_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Ophth_Data_PE a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from ON_Data_PE a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from PMR_Data_PE a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
		   from Pulm_Data_PE a
		   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Nephro_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Peds_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
		   from Neuro_Data_PE a
		   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Surg_Data_PE  a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from Ent_Data_PE a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)
	union all
	select ov.VisitID,ov.ClinicID,PEOther2
			from MntH_Data_PE a
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		   Join Medi_OfficeVisit ov on a.VisitID = ov.VisitID
		   JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		  -- where ov.ClinicID <> 1007
				--AND (   c.LastSyncDate IS NULL OR
				--		(c.LastSyncDate IS NOT NULL AND exists(select 1
				--													from Pati_Daily_Tracking d
				--													where       ov.PatientID    =  d.PatientID
				--															AND ov.ClinicID     =  d.ClinicID
				--															AND ov.VisitID      =  d.TableID
				--															AND d.TrackingType =  'OV'
				--															AND d.TrackDate    >= c.LastSyncDate
				--												)
				--		)
				--	)






GO
/****** Object:  View [dbo].[View_mongo_PhoneEncounter]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_PhoneEncounter]
AS
SELECT
	p.ClinicID,
	p.PatientID,
	p.[Caller],
	p.CallDate,
	p.CallPhone,
	p.CallMessage,
	p.ActionTaken,
	p.[Status],
	p.Reason,
	p.[Private],
	p.EncounterType,
	p.LinkID
FROM
	PhoneEncounter p
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--	p.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       p.PatientID    =  d.PatientID
--																	AND p.ClinicID     =  d.ClinicID
--																	AND p.PatientID    =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)




GO
/****** Object:  View [dbo].[View_mongo_PregHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_PregHistory]
as
  select p.PatientID,
         p.ClinicID,
		 p.PregDate,
		 rtrim(p.GAWeeks)GAWeeks,
		 rtrim(p.LaborLength)LaborLength,
		 rtrim(p.BirthWeight)BirthWeight,
		 rtrim(p.BabySEX)BabySEX,
		 rtrim(p.DeliverType)DeliverType,
		 rtrim(p.ANES)ANES,
		 p.DeliverPlace,
		 case p.PretermLabor when 'Y' then 'Yes' when 'N' then 'No' when 'O' then 'N/A' else null end PretermLabor,
		 p.Comments,
		 p.BabyName
         from OB_Data_PregHistory p
		 LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
		 JOIN Clinic on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
		-- where p.ClinicID <> 1007
		--AND (   c.LastSyncDate IS NULL OR
		--		(c.LastSyncDate IS NOT NULL AND exists(select 1
		--													from Pati_Daily_Tracking d
		--													where     p.PatientID    =  d.PatientID
		--															AND p.ClinicID     =  d.ClinicID
		--															AND p.PatientID      =  d.TableID
		--															AND d.TrackingType =  'Patient'
		--															AND d.TrackDate    >= c.LastSyncDate
		--												)
		--		)
		--	)





GO
/****** Object:  View [dbo].[View_mongo_prescriber]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[View_mongo_prescriber]
AS
SELECT
	s.ClinicID,
	Clinic.ClinicName,
	s.EmployeeID,
	s.SPI,
	s.DoctorName,
	s.NPI,
	s.ServiceLevels,
	CASE EPCS WHEN '1' THEN 'Y' ELSE 'N' END EPCS,
	s.EPCSSignupDate EPCSSignupTime,
	s.DirectAddress,
	s.Active_Start_Time ActiveStartTime,
	s.Active_End_Time ActiveEndTime,
	s.LocationID,
	s.[Address]
FROM
	ss_prescriber s
	JOIN Clinic on s.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	left join (select dateadd(dd,-2,LastSyncDate)LastSyncDate from db_identity) c on 1 = 1
WHERE
	s.ClinicID <> 1007 AND s.ClinicID > 0




GO
/****** Object:  View [dbo].[View_mongo_prescription]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_prescription]
AS
SELECT
  p.ClinicID,
  p.VisitID,
  p.NDC,
  --p.GPI,
  CASE WHEN LEN(dl.GPI) > 0 THEN dl.GPI ELSE p.GPI END GPI,
  p.RxNorm,
  p.Print_Name PrintName,
  p.Generic_Name GenericName,
  p.Manufacture,
  p.Strength,
  p.Units,
  p.DosageForm,
  p.[Route],
  p.[Take],
  p.Qualifier,
  p.Quantity,
  --RTRIM(p.scheduleLevel) ScheduleLevel,
  CASE
	  WHEN LEN(dl.scheduleLevel) > 0 THEN dl.scheduleLevel
	  WHEN dl.scheduleLevel = '' THEN '0'
	  ELSE CASE WHEN RTRIM(p.scheduleLevel) = '' THEN '0' WHEN p.scheduleLevel IS NULL THEN '0' ELSE RTRIM(p.scheduleLevel) END
  END scheduleLevel,
  p.Duration,
  Refill,
  CASE p.DAW WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END DAW,
  p.SIG,
  CASE p.sente WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END SentRx,
  CASE p.SendeFaxed WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END SentFax,
  CASE Printed WHEN 1 THEN 'Y' ELSE 'N' END Printed,
  p.MyDrugID,
  p.PresID PON,
  p.Note NoteToPharmacist,
  p.InternalNote,
  p.CreateBy,
  p.CreateDate,
  p.ModifyBy,
  p.ModifyDate
FROM
      Drug_Prescription p WITH(NOLOCK)
	  LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	  JOIN Clinic WITH(NOLOCK) on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
	  JOIN Medi_OfficeVisit ov WITH(NOLOCK) on p.VisitID = ov.VisitID and p.CliniciD = ov.ClinicID and p.[Status] = 0
	  LEFT JOIN [192.168.168.47].Medispan.dbo.DrugList dl ON p.NDC = dl.NDC
WHERE
	  p.[Status] = 0
--      AND p.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       p.PatientID    =  d.PatientID
--																	AND p.ClinicID     =  d.ClinicID
--																	AND p.VisitID      =  d.TableID
--																	AND d.TrackingType =  'OV'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)
GO
/****** Object:  View [dbo].[View_mongo_ProblemList]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_ProblemList]
AS
SELECT
	p.ClinicID,
	p.PatientID,
	p.ID ProblemID,
	p.Diagnosis,
	p.[Date],
	RTRIM(p.ICD) ICD,
	p.CodeSystem,
	p.Active,
	p.Chronic,
	p.ResolveDate,
	p.Notes,
	P.SNOMEDCode
FROM
	Pati_ProblemList p WITH (NOLOCK)
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic WITH (NOLOCK) on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0 and p.Active <> 'D'
--WHERE
--	p.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where     p.PatientID      =  d.PatientID
--																	AND p.ClinicID     =  d.ClinicID
--																	AND p.PatientID    =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)
GO
/****** Object:  View [dbo].[View_mongo_provider]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_provider]
AS
SELECT
	[web_id] WebID,
	e.ClinicID,
	ClinicName,
	EmployeeID,
	CONVERT(CHAR(10), e.EmployeeID) + '-' + CONVERT(CHAR(4), e.ClinicID) EmployeeUID,
	RTRIM(EmployeeFirstName) FirstName,
	RTRIM(EmployeeLastName) LastName,
	RTRIM(EmployeeMidName) MidName,
	EmployeeDOB DOB,
	RTRIM(Abbreviation) Abbr,
	RTRIM(EmployeeType) Title,
	RTRIM(EmployeeTaxonomy) Taxonomy,
	RTRIM(EmployeeLicNumber) License,
	RTRIM(EmployeeDEANumber) DEA,
	RTRIM(EmployeeNPI) NPI,
	RTRIM(EmployeeUPINNumber) UPIN,
	RTRIM(EmployeeWCBNumber) WCBAuthorization,
	RTRIM(WCBRatingCode) WCBRatingCode,
	EmployeeAddrStreet AddressLine1,
	EmployeeAddrApt APT,
	RTRIM(EmployeeAddrCity) City,
	EmployeeAddrState [State],
	RTRIM(EmployeeAddrZip) Zip,
	EmployeeHomePhone HP,
	EmployeeWorkPhone WK,
	EmployeeCellPhone Mobile,
	EmployeeFax Fax,
	EmployeeEmail Email,
	CASE EmployeeStatus WHEN 1 THEN 'Y' ELSE 'N' END [Disabled],
	CASE EmployeeLock WHEN 1 THEN 'Y' ELSE 'N' END ClinicLock,
	CASE SystemLock WHEN 1 THEN 'Y' ELSE 'N' END MdlandLock
FROM
	Employee e
	JOIN Clinic c ON c.ClinicID = e.ClinicID and isnull(c.Status,0) = 0
	LEFT JOIN db_identity d ON 1 = 1
WHERE
    --e.ClinicID <> 1007 AND e.ClinicID > 0 AND
	 EmployeeCode <> 9999999999
	AND LoginID <> 'RefDoctor'
GO
/****** Object:  View [dbo].[View_mongo_PTNote]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[View_mongo_PTNote]
AS
SELECT
	p.ClinicID,
	p.PatientID,
	p.VisitID,
	p.S,
	p.O,
	p.A,
	p.P,
	p.Memo
FROM
	Medi_OV_PTNote p
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--	p.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where     p.PatientID    =  d.PatientID
--																	AND p.ClinicID     =  d.ClinicID
--																	AND p.VisitID      =  d.TableID
--																	AND d.TrackingType =  'OV'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)




GO
/****** Object:  View [dbo].[View_mongo_QMCheckItemList]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_QMCheckItemList]
AS
	SELECT  m.ClinicID,
			m.PatientID,
			CONVERT(VARCHAR(10), m.PatientID)+'-'+CONVERT(VARCHAR(4), m.ClinicID) PatientUID,
			m.ItemCode,
			m.ItemName,
			m.ItemCodeSystem,
			m.SubCode,
			m.SubCodeSystem,
			m.SubCodeName,
			m.ItemStatus,
			m.ItemDate,
			m.ItemOther,
			m.CalcYear,
			m.CalcMonth,
			m.ModifyDate,
			m.ModifyBy
    FROM Medi_QMCheckItemList m WITH(NOLOCK)
			JOIN Clinic WITH(NOLOCK) on m.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
			LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
			--WHERE m.ClinicID <> 1007
			--	  AND (   c.LastSyncDate IS NULL OR
			--			  (c.LastSyncDate IS NOT NULL AND exists(select 1
			--															from Pati_Daily_Tracking d
			--															where     m.PatientID    =  d.PatientID
			--																  AND m.ClinicID     =  d.ClinicID
			--																  AND m.VisitID      =  d.TableID
			--																  AND d.TrackingType =  'OV'
			--																  AND d.TrackDate    >= c.LastSyncDate
			--													   )
			--			  )
			--		 )
GO
/****** Object:  View [dbo].[View_mongo_Ref_VaccineGroup]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_Ref_VaccineGroup]
AS
SELECT
	a.GroupCode,
	a.GroupName,
	a.GroupIndex,
	a.GroupNameCIR,
	RTRIM(b.CVXCode) CVXCode,
	RTRIM(b.[State]) GroupState
FROM
	Vaccine_Ref_Group a
	LEFT JOIN Vaccine_Ref_Group_CVX b ON a.GroupCode = b.GroupCode
GO
/****** Object:  View [dbo].[View_mongo_ReferralLetter]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_ReferralLetter]
AS
SELECT
	RefLetterID,
	a.ClinicID,
	PatientID,
	CONVERT(CHAR(10), a.PatientID) + '-' + CONVERT(CHAR(4), a.ClinicID) PatientUID,
	VisitID,
	DOS,
	ReferralDate,
	a.DoctorID ReferralBy,
	OfficeLocation,
	--Specialty,
	a.RefDoctorID ReferralTo,
	Taxonomy,
	SpecialtyName,
	RefReason,
	Unit Duration,
	CASE UnitType WHEN 1 THEN 'Time(s)' WHEN 2 THEN 'Month(s)' ELSE NULL END DurationType,
	FollowupDate,
	[Status],
	CASE [Status] WHEN 0 THEN 'New' WHEN 1 THEN 'Consultation Completed' WHEN 2 THEN 'Canceled' WHEN 4 THEN 'Printed' WHEN 5 THEN 'Faxed' WHEN 6 THEN 'Sent' ELSE NULL END StatusName,
	CASE PrintStatus WHEN '1' THEN 'Y' ELSE 'N' END Printed,
	CASE Urgent WHEN '1' THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END IsUrgent,
	AssignTo,
	ReturnDate,
	--Notes,
	AuthCode,
	CASE PrintOnRxPager WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END IsPrintOnRxPager,
	LetterType,
	CASE LetterType WHEN 1 THEN 'Referral Letter (Short Form)' WHEN 2 THEN 'Transition of Care (MU)' ELSE NULL END LetterTypeName,
	--Letter,
	CASE AttachPatientTOC WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END IsAttachPatientTOC,
	CASE AttachPatientSummary WHEN 1 THEN 'Y' WHEN 0 THEN 'N' ELSE NULL END IsAttachPatientSummary,
	ReferralLetterCode,
	ConsultationLetterReturnCode,
	AppointmentDate,
	a.CreateBy,
	a.CreateDate,
	a.LastModifyBy ModifyBy,
	a.LastModifyDate ModifyDate,
	CASE WHEN b.PECID IS NOT NULL THEN 'Y' ELSE 'N' END Exchanged,
	-- Referral Doctor Detail
	RTRIM(e.EmployeeFirstName) ReferralFirstName,
	RTRIM(e.EmployeeLastName) ReferralLastName,
	RTRIM(e.EmployeeMidName) ReferralMidName,
	RTRIM(e.EmployeeAddrAPT) ReferralAddrAPT,
	RTRIM(e.EmployeeAddrStreet) ReferralAddrStreet,
	RTRIM(e.EmployeeAddrCity) ReferralAddrCity,
	RTRIM(e.EmployeeAddrState) ReferralAddrState,
	RTRIM(e.EmployeeAddrZip) ReferralAddrZip,
	RTRIM(e.EmployeeHomePhone) ReferralHP,
	RTRIM(e.EmployeeWorkPhone) ReferralWP,
	RTRIM(e.EmployeeCellPhone) ReferralMobile,
	RTRIM(e.EmployeeFax) ReferralFax,
	RTRIM(e.EmployeeEMail) ReferralEmail,
	RTRIM(e.SecureMesgEmail) ReferralSecureMesgEmail,
	RTRIM(e.EmployeeType) ReferralTitle,
	RTRIM(e.EmployeeTaxonomy) ReferralTaxonomy,
	CASE WHEN r.ContactClinicID IS NOT NULL THEN 'Y' ELSE 'N' END IsMDLand,
	CASE WHEN r.ContactClinicID IS NOT NULL THEN r.ContactClinicID ELSE NULL END MDLandClinicID,
	CASE WHEN r.ContactClinicName IS NOT NULL THEN RTRIM(r.ContactClinicName) ELSE NULL END MDLandClinicName
FROM
	Medi_OV_Referral a WITH (NOLOCK)
	OUTER APPLY (SELECT TOP 1 PECID FROM PhoneEncounter e WHERE a.ClinicID = e.ClinicID AND a.RefLetterID = e.LinkID AND e.EncounterType IN (1054, 1061)) b
	LEFT JOIN Employee_ReferralDoctor e ON a.RefDoctorID = e.DoctorID AND a.ClinicID = e.ClinicID
	LEFT JOIN Ref_MDList r ON e.DoctorID = r.RefDoctorID AND e.ClinicID = r.ClinicID
GO
/****** Object:  View [dbo].[View_mongo_RiskAE]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_RiskAE]
AS

SELECT
  ov.ClinicID,
  ov.PatientID,
  a.VisitID,
  CONVERT(VARCHAR(10), ov.PatientID)+'-'+CONVERT(VARCHAR(4), ov.ClinicID) PatientUID,
  CONVERT(VARCHAR(10), a.VisitID)+'-'+CONVERT(VARCHAR(4), ov.ClinicID) VisitUID,
  a.RiskAEID,
  r.RiskAEName,
  r.SnomedCode,
  r.LoincCode,
  a.ModifyDate
FROM   Medi_OV_RiskAE a
	LEFT JOIN Ref_RiskAE r ON r.RiskAEID = a.RiskAEID
	LEFT JOIN Medi_OfficeVisit ov ON a.VisitID = ov.VisitID
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic on ov.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--	ov.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where       ov.PatientID    =  d.PatientID
--																	AND ov.ClinicID     =  d.ClinicID
--																	AND ov.VisitID      =  d.TableID
--																	AND d.TrackingType =  'OV'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)
GO
/****** Object:  View [dbo].[View_mongo_ScreeningSmoking]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_mongo_ScreeningSmoking]
AS
SELECT
	ID, a.ClinicID, PatientID, VisitID, VisitDate,
	CASE CONVERT(XML, QA).value('(/root/IsSmoke)[1]', 'VARCHAR(10)') WHEN 'Yes' THEN 'Y' WHEN 'No' THEN 'N' ELSE '' END AS IsSmoker,
	CONVERT(XML, QA).value('(/root/DaySmokeCount)[1]', 'INT') AS DaySmokeCount,
	CONVERT(XML, QA).value('(/root/HaveFirstCigarette)[1]', 'INT') AS HaveFirstCigarette,
	CASE CONVERT(XML, QA).value('(/root/WantAssistance)[1]', 'VARCHAR(10)') WHEN 'Yes' THEN 'Y' WHEN 'No' THEN 'N' ELSE '' END AS WantAssistance,
	ScreeningFormCode TobaccoSmokingLOINC,
	CASE CONVERT(XML, QA).value('(/root/IsSmoke)[1]', 'VARCHAR(10)') WHEN 'Yes' THEN '77176002' WHEN 'No' THEN '8392000' ELSE '' END AS SmokingSNOMED,
	a.CreateBy, a.CreateDate,
	a.LastModifyBy, a.LastModifyTime,
	FormID,
	TotalScore,
	IsDeleted,
	SignGUID,
	NeedConfirm,
	FormStatus
FROM
	Form_Screen a
	JOIN Clinic WITH (NOLOCK) ON a.ClinicID = Clinic.ClinicID AND ISNULL(Clinic.[Status], 0) = 0
WHERE
	FormID = 311
	AND IsDeleted = 0



GO
/****** Object:  View [dbo].[View_mongo_TagInbox]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_TagInbox]
AS

SELECT
	t.ClinicID,
	t.PatientID,
	CONVERT(VARCHAR(10), t.PatientID)+'-'+CONVERT(VARCHAR(4), t.ClinicID) PatientUID,
	t.TagID,
	t.FollowUpType,
	t.TagType,
	t.LinkID,
	t.TagName,
	t.TagMemo,
	t.TagStatus,
	t.RemindDate,
	t.RemindStatus,
	t.PatientName,
	t.PatientPhone,
	t.DoctorName,
	t.LinkID2,
	t.Private,
	t.AssignTo,
	t.NeedAlert,
	t.ForPatientPortal,
	t.TagNameID,
	t.Escorting,
	t.PatientConfirm,
	t.CreateDate,
	t.CreateBy,
	t.LastModifyDate ModifyDate,
	t.LastModifyBy ModifyBy
FROM
	Tag_Inbox t
	LEFT JOIN (SELECT LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON t.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
WHERE
	--t.LastModifyDate > c.LastSyncDate
	DATEDIFF(dd, t.LastModifyDate, GETDATE()) <= 1
GO
/****** Object:  View [dbo].[View_mongo_TagInbox_Log]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_TagInbox_Log]
AS

SELECT
	t2.ClinicID,
	t2.PatientID,
	CONVERT(VARCHAR(10), t2.PatientID)+'-'+CONVERT(VARCHAR(4), t2.ClinicID) PatientUID,
	t.TagID,
	t.ActionID,
	t.FollowDate,
	t.Description,
	t.Remark,
	t.ActionText,
	t.CreateDate,
	t.CreateBy
FROM
	TagInbox_Log t
	JOIN Tag_Inbox t2 ON t.TagID = t2.TagID
	LEFT JOIN (SELECT LastSyncDate FROM db_identity) c ON 1 = 1
	JOIN Clinic ON t2.ClinicID = Clinic.ClinicID and ISNULL(Clinic.Status, 0) = 0
--WHERE
--	--t.LastModifyDate > c.LastSyncDate
--	DATEDIFF(dd, t.CreateDate, GETDATE()) <= 1
GO
/****** Object:  View [dbo].[View_mongo_TreatmentPlan]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[View_mongo_TreatmentPlan]
as
    SELECT a.ClinicID,
		   a.PatientLastName,
		   a.PatientFirstName,
		   a.PatientMidName,
           a.PatientGender,
           a.PatientDOB,
		   b.Drug,
		   b.Dosage,
		   b.Units,
		   b.RecDosage,
		   b.Adjusted,
		   b.Route,
		   b.DrugAlias,
           CONVERT(VARCHAR(20), CASE
                                  WHEN c.PlanID IS NOT NULL THEN Isnull(c.BeginTime, c.PlanDate)
                                  ELSE
                                    CASE
                                      WHEN b.[Route] = 'PO' THEN Dateadd(dd, CASE
                                                                               WHEN Len(Frequency) > 0
                                                                                    AND Frequency <> '' THEN Cast(Substring(Frequency, 1, Charindex(',', Frequency) - 1) AS INT)
                                                                               ELSE 0
                                                                             END
                                                                             + Isnull(b.[Delay], 0) - 1, d.StartDate)
                                      ELSE NULL
                                    END
                                END, 101)       DOS,
           b.RxNorm
    FROM   Patient a
           JOIN On_Chemotherapy d ON a.PatientID = d.PatientID AND a.ClinicID = d.ClinicID
           JOIN On_TreatmentPlan b ON d.ChemID = b.ChemID AND d.ClinicID = b.ClinicID AND b.RxNorm IS NOT NULL AND Len(b.RxNorm) > 0
           LEFT JOIN On_TreatmentPlanDetail c ON b.ChemID = c.ChemID AND b.ClinicID = c.ClinicID AND b.PlanID = c.PlanID
		   LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) di on 1 = 1
		   JOIN Clinic on a.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
   -- WHERE  a.ClinicID <> 1007
			--AND (    di.LastSyncDate IS NULL OR
			--		(di.LastSyncDate IS NOT NULL AND exists(select 1
			--													from Pati_Daily_Tracking d
			--													where       a.PatientID    =  d.PatientID
			--															AND a.ClinicID     =  d.ClinicID
			--															AND a.PatientID    =  d.TableID
			--															AND d.TrackingType =  'Patient'
			--															AND d.TrackDate    >= di.LastSyncDate
			--												)
			--		)
			--	)





GO
/****** Object:  View [dbo].[View_mongo_VitalSign]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_mongo_VitalSign]
AS
SElECT
	p.ClinicID,
	p.PatientID,
	p.ID VitalSignID,
	p.VisitDateTime,
	CASE WHEN ISNUMERIC(p.VisitVitalSignHT) = 1 AND LTRIM(RTRIM(p.VisitVitalSignHT)) <> '-' AND LTRIM(RTRIM(p.VisitVitalSignHT)) <> '.' THEN CONVERT(FLOAT, RTRIM(p.VisitVitalSignHT)) ELSE NULL END VisitVitalSignHT,
	CASE WHEN ISNUMERIC(p.VitalSignHT) = 1 AND LTRIM(RTRIM(p.VitalSignHT)) <> '-' AND LTRIM(RTRIM(p.VitalSignHT)) <> '.' THEN CONVERT(FLOAT, RTRIM(p.VitalSignHT)) ELSE NULL END VitalSignHT,
	CASE WHEN ISNUMERIC(p.VisitVitalSignWT) = 1 AND LTRIM(RTRIM(p.VisitVitalSignWT)) <> '-' AND LTRIM(RTRIM(p.VisitVitalSignWT)) <> '.' THEN CONVERT(FLOAT, RTRIM(p.VisitVitalSignWT)) ELSE NULL END VisitVitalSignWT,
	CASE WHEN ISNUMERIC(p.VitalSignWT) = 1 AND LTRIM(RTRIM(p.VitalSignWT)) <> '-' AND LTRIM(RTRIM(p.VitalSignWT)) <> '.' THEN CONVERT(FLOAT, RTRIM(p.VitalSignWT)) ELSE NULL END VitalSignWT,
	CASE WHEN ISNUMERIC(p.VisitVitalSignBMI) = 1 AND LTRIM(RTRIM(p.VisitVitalSignBMI)) <> '-' AND LTRIM(RTRIM(p.VisitVitalSignBMI)) <> '.' THEN CONVERT(FLOAT, RTRIM(p.VisitVitalSignBMI)) ELSE NULL END VisitVitalSignBMI,
	VisitVitalSignBMIPercentile,
	RTRIM(p.VisitVitalSignTemp) VisitVitalSignTemp,
	RTRIM(p.VitalSignTemp) VitalSignTemp,
	p.VisitVitalSignBPMax,
	p.VisitVitalSignBPMin,
	RTRIM(p.VisitVitalSignHR) VisitVitalSignHR,
	p.P,
	p.VisitVitalSignRR,
	RTRIM(p.VisitVitalSignSPO2) VisitVitalSignSPO2,
	RTRIM(p.VisitVitalSignFS) VisitVitalSignFS,
	p.VisitVitalSignHC,
	p.VitalSignHC,
	p.VisitVitalSignWC,
	p.VitalSignWC,
	p.VisitVitalSignBHT,
	p.VitalSignBHT,
	p.VisitVitalSignBWT,
	p.VitalSignBWT,
	p.VisitVitalSignBSA,
	p.VisitVitalASB,
	p.VisitVitalSignHS,
	p.VisitVitalSignPainLevel,
	p.PerformentStatus,
	p.Others,
	BMILOINC LOINC_BMI,
	BMIPercentileLOINC LOINC_BMIPercentile,
	WTLOINC LOINC_WT,
	HTLOINC LOINC_HT,
	CASE WHEN BPLOINC1 IS NOT NULL THEN '8480-6' ELSE NULL END LOINC_BPMax,
	CASE WHEN BPLOINC2 IS NOT NULL THEN '8462-4' ELSE NULL END LOINC_BPMin
FROM
	Medi_VitalSign p WITH (NOLOCK)
	LEFT JOIN (SELECT dateadd(dd,-2,LastSyncDate)LastSyncDate FROM db_identity) c on 1 = 1
	JOIN Clinic WITH (NOLOCK) on p.ClinicID = Clinic.ClinicID and isnull(Clinic.Status,0) = 0
--WHERE
--	p.ClinicID <> 1007
--		AND (   c.LastSyncDate IS NULL OR
--				(c.LastSyncDate IS NOT NULL AND exists(select 1
--															from Pati_Daily_Tracking d
--															where     p.PatientID      =  d.PatientID
--																	AND p.ClinicID     =  d.ClinicID
--																	AND p.PatientID    =  d.TableID
--																	AND d.TrackingType =  'Patient'
--																	AND d.TrackDate    >= c.LastSyncDate
--														)
--				)
--			)
GO
/****** Object:  View [dbo].[View_Patient_Allegy]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_Allegy]
AS
SELECT  a.MedicationID,   b.Reaction, b.Status, '' AS RxNorm, c.AllergyName, b.ResolvedDate, a.PatientID, a.ClinicID
FROM         dbo.MH_Data_Allergy_Medication AS a LEFT OUTER JOIN
                      dbo.Pati_Allergy_Reaction AS b ON a.ClinicID = b.ClinicID AND a.PatientID = b.PatientID AND a.MedicationID = b.AllergyID AND
                      b.AllergyType = 'Medication' LEFT OUTER JOIN
                      dbo.MH_Ref_Allergy AS c ON a.MedicationID = c.AllergyID
GO
/****** Object:  View [dbo].[View_Patient_InsuPlan]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_InsuPlan]
AS
SELECT     c.InsuComType, c.InsuComName, c.InsuComFeeCateID, c.InsuComAddrApt, c.InsuComAddrStreet1, c.InsuComAddrStreet2, c.InsuComAddrCity, c.InsuComAddrState, c.InsuComAddrZip, c.InsuComAddrZipExt, c.InsuComMembershipPhone AS InsuComPhone,
                       c.InsuComProviderPhone, c.InsuComEligibilityPhone, c.InsuComFax,c.InsuComEMail,c.InsuComWebSite, c.InsuComSubmitterID AS SubmitterID,c.InsuComEMC, c.InsuComSubmitType,c.InsuComNSFID, c.InsuComNeedMoreInfo, c.InsuComGroupID,
                       c.Status, c.InsuComNPI, c.DefaultGroup, c.InsuMemo, c.AgingDays, c.AppealDays, '' InsuComAddrStateExt,
                       ip.InsuredID, ip.PlanName, ip.PlanType, ip.EffectiveDate, ip.PLanExpire,
                       ip.PlanCatNo AS Category, ip.PlanDeduct AS AnnualDeductible, ip.PlanCopay, ip.PlanCoverageExam AS Exam,
                      ip.PlanCoverageHosp AS Hospital, ip.PlanCoverageSurg AS Surgery, ip.RxID, ip.InsuredRelationship, ip.InsuredFirstName, ip.InsuredLastName, ip.InsuredMidName,
                      ip.InsuredSSNumber, ip.InsuredDOB, ip.InsuredGender, ip.InsuredMarrage, ip.InsuredAddrApt, ip.InsuredAddrStreet, ip.InsuredAddrCity, ip.InsuredAddrState,
                      ip.InsuredAddrZip, ip.InsuredHomePhone, ip.InsuredWorkPhone, ip.InsuredWorkPhoneExt, ip.InsuredEmail, ip.InsuredEmploymentCode, ip.InsuredEmployerName,
                      ip.PolicyNumber AS GroupNumber, ip.InsuPlanID, ip.InsuOrder, ip.PatientID, ip.ClinicID, ip.DoctorID, ip.MedicarePart, ip.MedicarePartID, ip.TempInsuComID,
                      ip.InsuredAddrZipExt,f.FeeCateName AS InsuranceType
FROM         dbo.Patient AS p INNER JOIN
                      dbo.Pati_InsuPlan AS ip ON p.PatientID = ip.PatientID AND p.ClinicID = ip.ClinicID INNER JOIN
                      dbo.Ref_InsuCom AS c ON ip.TempInsuComID = c.InsuComID AND ip.ClinicID = c.ClinicID inner JOIN
                      dbo.Ref_FeeCategory AS f ON f.FeeCateID = c.InsuComFeeCateID AND (f.ClinicID = p.ClinicID or f.ClinicID = 0)

WHERE     (ip.Active = 1)
GO
/****** Object:  View [dbo].[View_Patient_Medication]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_Medication]
AS
select  --[MedicationID]
      [ClinicID]
      ,[PatientID]
      --,[MyDrugID]
      ,[NDC]
      ,[DoctorID]
      ,[DrugID]
      ,[DrugFullDesc]
      ,[Generic_Name]
      ,[Print_Name]
      ,[Manufacture]
      ,[Inactive_Date]
      ,[Strength]
      ,[Units]
      ,[Pkg_Size]
      ,[Pkg_Type]
      ,[Route]
      ,[Take]
      ,[Frequency]
      ,[Duration]
      ,[Qualifier]
      ,[Quantity]
      ,[Refill]
      ,[Note]
      ,[DAW]
      ,[CreateDate]
      ,[CreateBy]
      ,[ModifyDate]
      ,[ModifyBy]
      ,[IsControl]
      ,[UserDefine]
      ,[scheduleLevel]
      ,[BeginDate]
      ,[EndDate]
      ,[Active]
      ,[sente]
      ,[OnCologyCheckStatus]
      ,[OnCologyCheckStatus1]
      ,[OnCologyCheckStatus2]
      ,[SIG]
      ,[Printed]
      ,[ICDCode]
      ,[SendeFaxed]
      ,[DosageForm]
      ,[GPI]
      ,[IsBrand]
      ,[IsGeneric]
      ,[GenericAndBrand]
      ,[SigUnit]
      ,[IsDrug]
      ,[Status]
      ,[FrequencyTimes]
      ,[FrequencyEvery]
      ,[FrequencySelection]
      ,[SIG_OLD]
      ,'' SideEffects
      ,'' Adherence
      ,'' PrescriberID
      ,'' PrescriberName
      ,'' DiscontinueReason
      ,'' ReconcileDate
      ,'' ReconcileBy
      ,'' AsReq
      ,'' Purpose
      ,'' AdditionalDirection
      ,'' Qualifier_Old
      ,'' PrescribeDate
      ,'' OrderDate
      ,'' RefillDate
      ,[InternalNote]
      --,[RxNorm]
      --,[CodeSystemID]
from Drug_Medication where  [Status]=0 and ISNULL(Active,'') <> 'N'
GO
/****** Object:  View [dbo].[View_Patient_ProblemList]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_ProblemList]
AS
SELECT     ID, PatientID, ClinicID, DoctorID, Date, Diagnosis, Medication, Active, ICD, MLog, Chronic, ResolveDate, Notes, LastModifyDate, LastModifyBy

FROM         dbo.Pati_ProblemList
GO
/****** Object:  View [dbo].[View_Patient_SocialHistory]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_SocialHistory]
AS
SELECT     FSmokerStopDate, CCS_Date, CurrentSmoker, SmokingNotes, PatientID, ClinicID
FROM         dbo.MH_Data_SocialHistory
GO
/****** Object:  View [dbo].[View_Patient_Vaccine]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_Vaccine]
AS
SELECT    -- [VaccineID]
      a.[ClinicID]
      ,a.[PatientID]
      ,a.[CVXCode]
      ,a.[AdministrationDate]
      ,a.[OrderDate]
      ,a.[OfficeLocation]
      ,a.[MFTCode]
      ,a.[MFTName]
      ,a.[VaccineAmount]
      ,a.[VaccineUnit]
      ,a.[InvID]
      ,a.[LotNumber]
      ,a.[ProviderInitials]
      ,a.[ProviderID]
      ,a.[ParentInitials]
      ,a.[Notes]
      ,a.[CreateDate]
      ,a.[CreateBy]
      ,a.[ModifyDate]
      ,a.[ModifyBy]
      ,a.[OfficeLocationBak]
      ,a.[DoseOfSeries]
      ,a.[ExpiredDate]
      ,a.[VISDate]
      ,a.[VaccineDose]
      ,a.[Site]
      ,a.[VFC]
      ,a.[InsuPlanID1]
      ,a.[InsuPlanID2]
      ,a.[InsuPlanID3]
      ,a.[InformationSource]
      ,a.[FundingSource]
      ,a.[OtherProvider]
      ,a.[OtherLocation]
      ,a.[NewVaccine]
      ,a.[NextDueDate]
      ,a.[DownFrom]
      ,a.[OBXMsg]
      ,a.[LogID]
      ,a.[IsRecom]
      ,a.[WaitTime]
      ,a.[UploadDate]
      ,a.[VisitID]
      ,a.[IsUploaded]
      ,a.[CPTCode]
      ,a.[PPDResult]
      ,a.[Unit]
      ,a.[DateRead]
      ,a.[Induration]

      ,b.ShortName, b.FullName
FROM         dbo.Vaccine_Data_PatientVaccine AS a LEFT OUTER JOIN
                      dbo.Vaccine_Ref_CVX AS b ON a.CVXCode = b.CVXCode
GO
/****** Object:  View [dbo].[View_Patient_VitalSign]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Patient_VitalSign]
AS
SELECT     [VisitDateTime]
      ,[PatientID]
      ,[ClinicID]
      ,[VisitVitalSignHT]
      ,[VisitVitalSignWT]
      ,[VisitVitalSignWTOZ]
      ,[VisitVitalSignBP]
      ,[VisitVitalSignHR]
      ,[VisitVitalSignRR]
      ,[VisitVitalSignHC]
      ,[VisitVitalSignFS]
      ,[VisitVitalSignTemp]
      ,[VisitVitalSignSPO2]
      ,[VisitVitalSignLMP]
      ,[VisitVitalSignBMI]
      ,[VisitVitalSignPainLevel]
      ,[VisitVitalSignPulse]
      ,[VisitVitalSignPainLocation]
      ,[CreateBy]
      ,[CreateDate]
      ,[ModifyBy]
      ,[ModifyDate]
      ,[VisitVitalSignBPMin]
      ,[VisitVitalSignBPMax]
      ,[VitalSignHT]
      ,[VitalSignWT]
      ,[VitalSignHC]
      ,[VisitVitalSignBMIPercentile]
      ,[VisitVitalSignBSA]
      ,[VisitVitalSignBHT]
      ,[VisitVitalSignBWT]
      ,[VisitVitalSignBWTOZ]
      ,[VitalSignBHT]
      ,[VitalSignBWT]
      ,[VitalSignTemp]
      ,[VisitVitalSignHS]
      ,[VisitVitalASB]
      ,[P]
      ,[PerformentStatus]
      ,[Others]
      ,[ID]
      --,[BPLOINC1]
      --,[BPLOINC1CodeSystemID]
      --,[BPLOINC2]
      --,[BPLOINC2CodeSystemID]
      --,[HTLOINC]
      --,[HTLOINCCodeSystemID]
      --,[BMILOINC]
      --,[BMILOINCCodeSystemID]
      --,[WTLOINC]
      --,[WTLOINCCodeSystemID]
FROM         dbo.Medi_VitalSign
GO
/****** Object:  View [dbo].[view_Ref_CPT_And_Modifier]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[view_Ref_CPT_And_Modifier]
as
select CPTCode,CPTName,CPTDesc,ISNULL(ClinicID,0)ClinicID,-1*(Status-1) Status
       from dbo.Ref_CPT
union all
       select a.CPTCode+ISNULL(Ma,'')+ISNULL(Mb,'')+ISNULL(Mc,'')+ISNULL(Md,'')CPTCode,b.CPTName,b.CPTDesc,a.ClinicID,a.Status
       from dbo.Ref_CPT_Modifier a,dbo.Ref_CPT b
       where a.CPTCode = b.CPTCode


GO
/****** Object:  View [dbo].[view_Ref_CPT_Modifier]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[view_Ref_CPT_Modifier]
as
       select a.CPTCode+ISNULL(Ma,'')+ISNULL(Mb,'')+ISNULL(Mc,'')+ISNULL(Md,'')CPTCode,b.CPTName,b.CPTDesc,Ma,Mb,Mc,Md,a.ClinicID,a.Status
       from dbo.Ref_CPT_Modifier a,dbo.Ref_CPT b
       where a.CPTCode = b.CPTCode



GO
/****** Object:  View [dbo].[View_Ref_ICD]    Script Date: 3/21/2019 3:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[View_Ref_ICD]
as
   select ICDCode,ICDName,ICDDesc,'ICD9'CodeSystem,'Diagnosis'ICDType from ref_ICD
   union all
   select ICDCode,ICDName,ICDDesc,'ICD10'CodeSystem,ICDType from ref_ICD10 where ICDType = 'Diagnosis'
GO
